

module.exports = function(app){
    return {
        createLog : function(req, obj){
            var item = new app.db.models.AuditLog(obj);
            item.employeeId = req.user.employee.employeeId;
            //item.entity
            item.save();
        },
        prepareLog : function(req, entity, action){
           var log = new app.db.models.AuditLog({
                employeeId: req.user.employee.employeeId,
                entityType: entity,
                endpoint: req.ip || req.scgnIP
            });
            action && (log.action = action);
            return log;
        },
        updateAndSave: function(log, desc, obj, action){
            desc && (log.description = desc);
            obj && (log.newValue = JSON.stringify(obj), log.entityId = obj.id);
            action && (log.action = action);
            log.save();
        }
    };
};


