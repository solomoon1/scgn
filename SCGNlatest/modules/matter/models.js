﻿//'use strict';
module.exports = function (app, mongoose) {
    var ObjectId = mongoose.Schema.Types.ObjectId;
    var matterSchema = new mongoose.Schema({
        brNo: {type: String, required: true}, //Serial No

        title: {type: String, required: true},
        client: {type: String, required: true},

        dateOpened: {type: Date, required: true},
        nextHearing: {type: Date, required: true},
        verdict: {type: String},
        status: {type: String, enum: ['', 'Active', 'Inactive']},

        members: [{type: ObjectId, ref: 'Employee'}],
        lead: {type: ObjectId, ref: 'Employee'},

        practiceArea: {type: String, required: true},
        date: {type: Date, required: true, default: new Date()},

        by: {type: ObjectId, ref: 'Employee'},
        // description: { type: String },
        // location: { type: String, enum: ['Lagos', 'Abuja'], required: true },
        // batch: { type: Number, required: true },
        // stat: { sub: Number, sup: Number },
        // full: { type: Boolean, default: false },
        // closed: { type: Boolean, default: false },
        // closeDate : {type: Date}

        history: [{
            date: Date,
            by: String,
            type: {type: String, enum: ['verdict', 'nextHearing', 'Details']},
            value: Object,
            verdict: String
        }]
    });
    matterSchema.index({brNo: 1}, {unique: true});

    matterSchema.statics.getPrefix = function () {
        return PREFIX;
    };

    const PREFIX = 'Ref-M-';

    matterSchema.statics.getNextSerial = function (cb) {
        app.db.models.Matter.find()
            .sort({brNo: -1})
            .limit(1)
            .exec(function (err, pd) {
                if (err) cb(err);
                else if (pd.length) {
                    //var last = pd.pop();
                    cb(null, PREFIX + app.scgn.utility.pad(pd.pop().getSerial() + 1));
                } else cb(null, PREFIX + req.app.scgn.utility.pad(1));
            })
    };
    matterSchema.methods.getNextSerial = function (cb) {
        app.db.models.Matter.getNextSerial(cb);
    };

    matterSchema.methods.getSerial = function () {
        return parseInt(this.brNo.split('-').pop());
    };

    app.db.model('Matter', matterSchema);
};