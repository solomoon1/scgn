﻿module.exports = function (app, mongoose) {
    app = app || {};
    require('./models')(app, mongoose);
    var controller = require('./controller.js');
    // app.all('/matter*', app.ensureAuthenticated);
    app.get('/matter/list', controller.getMatterList);
    app.get('/matter/listemps',app.ensureMatterCreation, controller.getMatterListWithEmps);
    app.post('/matter', app.ensureMatterCreation, controller.saveMatter);
    app.get('/matter/reports', app.ensureHR, controller.activeMatterReports);
    app.get('/matter/reports/:id', app.ensureHR, controller.activeMatterReports);
    app.post('/matter/close/:id', app.ensurePD, controller.closeMatter);
}