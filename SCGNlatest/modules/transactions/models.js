module.exports = function (app, mongoose) {
    //Transactions
    var orderSchema = new mongoose.Schema({
        bookId: { type: mongoose.Schema.Types.ObjectId, ref: 'Books' },
        price: { type: Number},
        count: { type: Number},
        cost: { type: Number}
    });

    var transactionSchema = new mongoose.Schema({
        tranxId: { type: String },
        status: { type: String, default: 'Pending', enum: ['Approved', 'Pending', 'Cancelled', 'Declined']},
        statusDescription: { type: String},
        amount: { type: Number, default: 0 },
        type: { type: String, default: '', enum: ['book', 'donation','training-event','subscription']},
        customer: { type: String },
        email: { type: String, required: true },
        phone: { type: String },
        invoiceSent: { type: Boolean, default: false },
        items: [orderSchema],
        approvalCode: { type: Number },
        createdOn: { type: Date, default: Date.now },
        scheme: { type: String },
        currency: { type: String },
        description: { type: String },
        memberId: { type: String }
    });

    // bookOrderSchema.set('toJSON', {virtuals: true});
    app.db.model('Transaction', transactionSchema);

};