module.exports = function (app, mongoose) {
    require('./models')(app, mongoose);
    var controller = require('./controller.js');

    app.get('/api/tranx', controller.getTransactions);
    app.get('/api/tranx/:trxId', controller.getTransaction);
    app.get('/api/tranxError/:trxId', controller.getTransactionError);
    app.get('/api/tranx/mbr/:memberId', controller.getMemberTransaction);
    app.post('/api/tranx', controller.createTransaction);
    app.post('/api/tranxResponse', controller.concludeTransaction);
    app.put('/api/tranx', controller.updateTransaction);

    // app.put('/employee/experience', controller.editExperience);
    // app.put('/employee/education', controller.editEducation);
    // app.put('/employee/skill', controller.editSkill);
    // app.put('/employee/dependant', controller.editDependant);
    // app.post('/employee/picture', controller.changePicture);
    // app.post('/weeklyReport/report', controller.postWeeklyReport);
};