'use strict';
module.exports = function (app, mongoose) {
    var outOfOfficeSchema = new mongoose.Schema({
        employee: {type: mongoose.SchemaTypes.ObjectId, ref: 'Employee'},
        employeeId: {type: String, required: true},
        name: {type: String, required: true},
        department: {type: String, required: true},
        // hrcomment: {type: String, required: false},
        designation: {type: String, required: true},
        type: {type: String, required: true},
        date: {type: Date, required: true},
        dateCreated: {type: Date, required: true, default: new Date()},
        resumptionDate: {type: Date, required: true, default: new Date()},
        resumptionTime: {type: Date},
        details: String,
        toApprove: {type: mongoose.SchemaTypes.ObjectId, ref: 'Employee'},
        timeBased: Boolean,
        status: {type: String, required: true, enum: app.constants.outOfficeRequestStates}
    });
    app.db.model('OutOffice', outOfOfficeSchema);
};