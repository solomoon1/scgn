'use strict';
module.exports = function (app, mongoose) {
    require('./models')(app, mongoose);
    var controller = require('./controller.js');

    app.post('/attendance/approve/outoffice',app.ensureAuthenticated, controller.approveOutOfficeRequest);
    app.post('/attendance/decline/outoffice',app.ensureAuthenticated, controller.declineOutOfficeRequest);

    app.post('/attendance/outoffice', app.ensureAuthenticated, controller.saveOutOfOfficeRequest);
};