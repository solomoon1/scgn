
'use strict';

module.exports = function (app, mongoose) {
    var auditSchema = new mongoose.Schema({
        action: { type: String, required: true, enum: ['Update', 'Delete', 'Create', 'View'] },//imp
        description: String,
        timestamp: { type: Date, required: true, default: new Date() },
        employeeId: { type: String, required: true },//imp
        entityType: { type: String, required: true },//imp
        entityId: { type: String, required: true },
        endpoint: String,//imp
        oldValue: String,
        newValue: String
    });


    auditSchema.methods.setOld = function (old) {
        this.oldValue = typeof old == 'string' ? old : JSON.stringify(old);
    };
    auditSchema.index({ entityType: 1});
    auditSchema.index({ employeeId: 1});
    auditSchema.index({ date: 1});
    auditSchema.set('autoIndex', (app.get('env') === 'development'));
    app.db.model('AuditLog', auditSchema);
};