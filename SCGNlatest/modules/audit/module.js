
'use strict';
module.exports = function (app, mongoose) {
    require('./models')(app, mongoose);

    var controller = require('./controller.js');
    app.get('/auditlogs', controller.getAuditLogs);
    app.get('/auditlogs/employee/:id', controller.getAuditLogsForEmployee);
    app.get('/auditlogs/action/:id', controller.getAuditLogsByAction);
    app.get('/auditlogs/entity/:id', controller.getAuditLogsByAction);
};