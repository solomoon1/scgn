var mongoose = require('mongoose'),
    formidable = require('formidable'),
    fs = require('fs'),
    async = require('async'),
    EventEmitter = require('events').EventEmitter,
    emitter = new EventEmitter(),
    path = require('path'),
    pathSeparator = path.sep,
    bookCoverPath = path.join('public', 'images', 'book-cover')
    ;


exports.getBooks = function (req, res) { //return all books
    var errorMessage = '';
    req.app.db.models.Books.find({ }, null, { sort: { isActive: 1, createdOn: -1, category: 1 } }, function (err, books) {
        if (err) {
            errorMessage = 'Failed to fetch book(s). ' + err.message;
            return res.status(500).json({ message: errorMessage });
        }
        // req.app.utility.notify.emit('chidi','Hi', function(err, r){
        //     console.log('Got result: %j', r);
        // });
        res.json({ message: 'Books fetched!', books: books });
    });
};


exports.getPubBooks = function (req, res) { //return all books
    var errorMessage = '';
    req.app.db.models.Books.find({ isActive: true }, null, { sort: { createdOn: -1, category: 1 } }, function (err, books) {
        if (err) {
            errorMessage = 'Failed to fetch book(s). ' + err.message;
            return res.status(500).json({ message: errorMessage });
        }
        // req.app.utility.notify.emit('chidi','Hi', function(err, r){
        //     console.log('Got result: %j', r);
        // });
        res.json({ message: 'Books fetched!', books: books });
    });
};

exports.createBook = function (req, res) {
    //create a new book
    var fileExist = false;
    if (req.myFile.bookCover && req.myFile.bookCover.name) fileExist = true;
    if (!req.isMyFileValid && fileExist) {
        var filePath = req.myFile.bookCover.path;
        res.status(400).json({ message: 'Wrong file format! (.jpeg | .jpg | .png) ' });
        var err = fs.unlinkSync(filePath);
        if (err) console.log('Image at path: ', filePath, 'was not deleted!');
    } else {
        var errorMessage = '';
        req.app.db.models.Books.findOne({ title: req.myFields.title }, { title: 1 }, function (err, book) {
            if (err) return res.status(500).json({ message: 'Cannot access Book Collection' });
            if (book) return res.status(403).json({ message: 'Book Title already exists' });

            if (fileExist) req.myFields.bookCover = req.myFile.bookCover.name;
            var newBook = new req.app.db.models.Books();
            Object.assign(newBook, req.myFields);
            newBook.save(function (err, book) {
                if (err) {
                    errorMessage = 'Book creation failed';
                    return res.status(500).json({ message: errorMessage, err: err.message });
                }
                try {
                    req.app.db.models.Member.find({ isActive: true }, function (err, allMember) {
                        if(err) throw new Error(err)
                        allMember.forEach(function (obj) {
                            var m = {
                                firstName: obj.firstName,
                                lastName: obj.lastName,
                                bookTitle: book.title,
                                author: book.author,
                                price: book.price,
                                description: book.description,
                                scgnUrl: req.app.config.host,
                                mEmail: obj.mEmail
                            }
                            req.app.utility.notify.emit('newPublication', m);

                        })
                    })
                } catch (err) {
                    console.log('Error sending mails to the members');
                } finally {
                    res.json({ message: 'Book Created!' });
                }
            });
        });
    }
};

// exports.addStock = function (req, res) {
//     //create a new book
//     var fileExist = false;
//     if(req.bookCover && req.bookCover.name) fileExist = true;
//     if(!req.isMyFileValid && fileExist) {
//         var filePath = req.myFile.bookCover.path;
//         res.status(400).json({ message: 'Wrong file format! (.jpeg | .jpg | .png) '});
//         var err = fs.unlinkSync(filePath);
//         if(err) console.log('Image at path: ', filePath ,'was not deleted!');
//     }else {
//     var errorMessage = '';
//     req.app.db.models.Books.findOne({ title: req.myFields.title}, { title: 1}, function(err, book){
//         if(err) return res.status(500).json({ message: 'Cannot access Book Collection'});
//         if(book) return res.status(403).json({ message: 'Book Title already exists'});
//
//         if(fileExist) req.myFields.bookCover = req.myFile.bookCover.name;
//         var newBook = new  req.app.db.models.Books();
//         Object.assign(newBook, req.myFields);
//         newBook.save(function(err){
//             if(err){
//                 errorMessage = 'Book creation failed';
//                 return res.status(500).json({ message: errorMessage, err: err.message });
//             }
//             res.json({ message: 'Book Created!'});
//         });
//     });
//     }
// };

exports.updateBook = function (req, res) {
    /*
     update a book
     this is done by a privileged employee
     most likely fields to change are
     - stockCount
     - editedOn
     -
     */
    var fileExist = false;
    if (req.myFile.bookCover && req.myFile.bookCover.name) {
        fileExist = true;
    }
    if (!req.isMyFileValid && fileExist) {
        var filePath = req.myFile.bookCover.path;
        res.status(400).json({ message: 'Wrong file format! (.jpeg | .jpg | .png) ' });
        var err = fs.unlinkSync(filePath);
        if (err) console.log('Image at path: ', filePath, 'was not deleted!');
    } else {
        if (fileExist) req.myFields.bookCover = req.myFile.bookCover.name;
        var query = { _id: req.myFields._id };

        req.app.db.models.Books.findOneAndUpdate(query, req.myFields, { new: false }, function (err, book) {
            if (err) {
                errorMessage = 'Book update failed';
                return res.status(500).json({ message: errorMessage, err: err.message });
            }

            var filePath = bookCoverPath + pathSeparator + book.bookCover;
            if (fileExist && book.bookCover !== 'scgn-book2.png' && book.bookCover !== req.myFields.bookCover) {
                fs.unlink(filePath, function (err) {
                    if (err) console.log('Image at path: ', filePath, 'was not deleted!');
                    if (!err) console.log('Old image removed!');
                });
            }
            res.json({ message: 'Book Updated! (' + book.title + ')' });
        });
    }
    ;
};

/* Book Stock */
exports.getBookStocks = function (req, res) {
    var query = {};
    req.app.db.models.BookStock
        .find({})
        .populate({ path: 'bookId', select: 'title author category stockCount -id' })
        .populate({ path: 'employeeId', select: '-id firstName lastName access' })
        .exec(function (err, bookStocks) {
            if (err) return res.status(500).json({ message: 'Error fetching Book Stock Records' });
            return res.json({ message: 'Book Stock Records fetched!', data: bookStocks });
        });
};
exports.updateBookStock = function (req, res) {
    /* update book stock db and the book db */
    var b = req.body;
    if (b.employeeId && b.bookId && b.quantity) {
        req.app.db.models.Books
            .findOneAndUpdate(
                { _id: b.bookId },
                { $inc: { stockCount: parseInt(b.quantity) } },
                function (err, book) {
                    if (err) return res.status(500).json({
                        message: 'Book Stock Count update failed',
                        "Error Msg": err.message
                    });
                    var newBookStock = new req.app.db.models.BookStock();
                    newBookStock = Object.assign(newBookStock, b);
                    newBookStock.save(function (err) {
                        if (err) return res.status(500).json({
                            message: 'Book and Stock Count failed',
                            "Error Msg": err.message
                        });
                        var bookTitle = book.title.toUpperCase();
                        res.json({ message: 'Book and Stock counts for ' + bookTitle + ' updated!' });
                    })
                });
    } else {
        return res.status(400).json({ message: 'Missing fields!' });
    }
};
exports.editBookStock = function (req, res) {
    /* update book stock db and the book db */
    var b = req.body;
    if (b.bookRetiredOn) return res.status(400).json({ message: 'Cannot retire same book multiple times' });
    if (b.employeeId && b.bookId && b.quantity) {
        req.app.db.models.Books
            .findOneAndUpdate(
                { _id: b.bookId },
                { $inc: { stockCount: parseInt(b.bookRetired) } },
                function (err, book) {
                    if (err) return res.status(500).json({
                        message: 'Book Stock Count update failed',
                        "Error Msg": err.message
                    });
                    req.app.db.models
                        .BookRequest.findOne(
                            { _id: b._id },
                            function (err, bookRequest) {
                                var bR = Object.assign(bookRequest, b);
                                bR.bookRetiredOn = new Date();
                                bR.save(function (err) {
                                    if (err) return res.status(500).json({
                                        message: 'Book Request update failed failed',
                                        "Error Msg": err.message
                                    });
                                    var bookTitle = book.title.toUpperCase();
                                    res.json({ message: 'Book Request, Book and Stock counts for ' + bookTitle + ' updated!' });
                                })
                            });
                });
    } else {
        return res.status(400).json({ message: 'Missing fields!' });
    }

};

/* Book Request */
exports.getBookRequests = function (req, res) {
    //remember to send a requestType property along with the request object
    var type = req.params.requestType || 'All';
    var query = {};
    switch (type) {
        case 'Pending':
            query = { status: 'Pending' };
            break;
        case 'Issued':
            query = { status: 'Issued' };
            break;
        default:
            query = {};
            break;
    }

    if (type === 'All') {
        req.app.db.models.BookRequest
            .aggregate([
                { $group: { _id: '$status', count: { $sum: 1 }, requests: { $push: '$$CURRENT' } } },
                { $project: { status: '$_id', count: 1, requests: 1, _id: -1 } },
                { $sort: { _id: 1 } }
            ], function (err, bookRequests) {
                if (err) return res.status(500).json({ message: 'Error fetching Book Stock Records' });

                async.parallel([
                    function (cb) {
                        req.app.db.models.Books
                            .populate(bookRequests, {
                                path: 'requests.bookId',
                                select: 'title author category stockCount'
                            }, cb);
                    },
                    function (cb) {
                        req.app.db.models.Employee
                            .populate(bookRequests, {
                                path: 'requests.employeeId',
                                select: 'firstName lastName access'
                            }, cb);
                    }],
                    function (err, results) {
                        if (err) return res.status(500).json({ message: 'Error Populating Records via Books model' });
                        return res.json({
                            message: 'Book Stock Records fetched!',
                            bookRequests: results[0]
                        });
                    }
                );


                // req.app.db.models.Books
                //     .populate(bookRequests, {path: 'requests.bookId', select: 'title author category stockCount'},
                //         function (err, r) {
                //             if (err) return res.status(500).json({message: 'Error Populating Records via Books model'});
                //             req.app.db.models.Employee
                //                 .populate(r, {
                //                         path: 'requests.employeeId',
                //                         select: 'firstName lastName access'
                //                     },
                //                     function (err, re) {
                //                         if (err) return res.status(500).json({message: 'Error Populating Records via Books model'});
                //                         return res.json({
                //                             message: 'Book Stock Records fetched!',
                //                             bookRequests: re
                //                         });
                //                     });
                //         });
            }); //end of aggregation query
    } else {
        req.app.db.models.BookRequest
            .find(query)
            .sort({ createdOn: -1 })
            .populate({ path: 'bookId', select: 'title author category stockCount' })
            .populate({ path: 'employeeId', select: 'firstName lastName access' })
            .exec(function (err, bookRequests) {
                if (err) return res.status(500).json({ message: 'Error fetching Book Stock Records' });
                return res.json({ message: 'Book Stock Records fetched!', bookRequests: bookRequests });
            });
    }
};
exports.makeBookRequest = function (req, res) {
    var r = req.body;
    if (r.employeeId && r.bookId && r.quantity) {
        var newRequest = new req.app.db.models.BookRequest();
        newRequest = Object.assign(newRequest, r);
        newRequest.save(function (err) {
            if (err)
                return res.status(500).json({
                    message: 'Book request failed',
                    "Error Msg": err.message
                });
            res.json({ message: 'Request for Book submitted ' });
        })
    } else {
        return res.status(400).json({ message: 'Missing fields!' });
    }
};
exports.updateBookRequest = function (req, res) {
    /*
     this query is successful will
     1. update the bookRequest model.
     2. update the book model
     3. update the bookStock model
     */
    var r = req.body;
    if (r.employeeId._id && r.bookId._id && r.quantity) {
        //find the book request
        var employeeId = r.employeeId._id,
            bookId = r.bookId._id,
            quantity = r.quantity,
            rId = r._id;

        req.app.db.models.BookRequest
            .findOne({ _id: rId }, function (err, bookRequest) {
                if (err) res.status(500).json({ message: 'Error fetching Book Request', errMsg: err.message });
                if (bookRequest._id) {
                    //update book request
                    bookRequest.status = 'Issued';
                    bookRequest.grantedOn = new Date();
                    bookRequest.save(function (err) {
                        if (err) return res.status(500).json({
                            message: 'Failed to update book request.',
                            errMsg: err.message
                        });
                        //update book's stockCount
                        req.app.db.models.Books
                            .findOneAndUpdate(
                                { _id: bookId },
                                { $inc: { stockCount: -(parseInt(quantity)) } },
                                function (err, book) {
                                    if (err) return res.status(500).json({
                                        message: 'Failed to update Book.',
                                        errMsg: err.message
                                    });
                                    //update book stock
                                    var bookStock = new req.app.db.models.BookStock();
                                    bookStock.bookId = bookId;
                                    bookStock.employeeId = employeeId;
                                    bookStock.quantity = quantity;
                                    bookStock.actionType = 'StockOut';
                                    bookStock.save(function (err) {
                                        if (err) return res.status(500).json({
                                            message: 'Failed to update Book.',
                                            errMsg: err.message
                                        });
                                        res.json({ message: 'Book, Stock and Request updated successfully for ' + book.title });
                                    })
                                });
                    });
                } else {
                    return res.status(400).json({ message: 'Book Request with that ID does not exist' });
                }
            });
    } else {
        return res.status(400).json({ message: 'Insufficient Fields sent!' });
    }

    // res.json({ message: 'God request', data: req.body});
};

exports.declineBookRequest = function (req, res) {
    var requestId = req.params.requestId;
    req.app.db.models.BookRequest.remove({ _id: requestId }, function (err) {
        if (err) return res.status(500).json({
            message: 'Failed to decline Book request.',
            errMsg: err.message
        });
        res.json({ message: 'Book request declined successful!' });
    });
};


exports.setIsActive = function (req, res) {
    var bookId = req.body.bookId;
    req.app.db.models.Books.findOne({ _id: bookId }, function (err, b) {
        if (err) return res.status(500).json({
            message: 'Cannot fine book with id: ' + bookId,
            errMsg: err.message
        });
        b.isActive = !b.isActive;
        b.save(function(e, b){
            if(e) return res.status(500).json({
                message: 'Failed to update book\'s isActive property'
            });
            res.json({ message: 'Book with id ' + bookId + ' was updated successful!' });
        })
    });
};

//#endregion
