module.exports = function (app, mongoose) {
    //Books
    var bookSchema = new mongoose.Schema({
        title: {type: String, required: true, unique: true, lowercase: true},
        author: { type: String, required: true},
        category: {type: String, required: true, enum: ['Texts', 'Journals']},
        isbn: {type: String , unique: true, required: false, min: 10, max: 13},
        published: { type: Date, default: Date.now },
        description: { type: String, default: 'It is an interesting book!Get it!'},
        stockCount: { type: Number, default: 0 },
        isActive: { type: Boolean, default: true, enum: [false, true] },
        price: { type: Number, default: 0},
        status: { type: String, default: 'Available', required: true, enum: ['Available', 'OutOfStock']},
        employeeId: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'},
        createdOn: { type: Date, default: Date.now },
        bookCover: { type: String, default: 'scgn-book2.png'}
    });

    //Book Stock In and Out
    var bookStockSchema = new mongoose.Schema({
        bookId: {type: mongoose.Schema.Types.ObjectId, ref: 'Books'},
        employeeId: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'},
        quantity: { type: Number, default: 1},
        actionType: { type: String, required: true, enum: ['StockIn', 'StockOut'], default: 'StockIn'},
        createdOn: { type: Date, default: Date.now }
    });


    //Book Stock
    var bookRequestSchema = new mongoose.Schema({
        bookId: {type: mongoose.Schema.Types.ObjectId, ref: 'Books'},
        employeeId: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'},
        quantity: { type: Number, default: 1},
        status: { type: String, default: 'Pending', required: true, enum: ['Pending', 'Issued']},
        createdOn: { type: Date, default: Date.now },
        grantedOn: { type: Date },
        bookRetired: { type: Number, default: 0 },
        bookRetiredOn: { type: Date }
    });



    // bookSchema.methods.fullName = function () {
    //     return this.firstName + ' ' + this.lastName;
    // };
    // bookSchema.methods.isValidAccess = function () {
    //     return app.constants.access.indexOf(this.access) != -1;
    // };
    // bookSchema.methods.canPlayRoleOf = function (name) {
    //     switch (name.toLowerCase()) {
    //         case 'admin':
    //             return this.access == 'M' || this.access == 'O';
    //         case 'finance':
    //             return this.department && this.department.name == 'Finance';
    //         default:
    //             return false;
    //     }
    // };
    // bookSchema.methods.isPD = function () {
    //     //var l = 'O';
    //     //var result = this.access == l;
    //     //return result;
    //     return this.access == 'O' && this.department.name == 'HR';
    // };
    // bookSchema.methods.isASupervisor = function () {
    //     return this.subordinates.length > 0;
    // };


    // bookSchema.virtual('name').get(function () {
    //    // return this.firstName + ' '+ this.lastName;
    //     return this.fullName();
    // });

    // bookSchema.virtual('extras').get(function () {
    //     var name = this.fullName(),
    //         deptName = (this.department || {}).name || '',
    //         dept = deptName || this.department;
    //     return {
    //         _id: this.id,
    //         name: this.fullName(),
    //         dept: dept,
    //         employeeId:this.employeeId,
    //         active: this.status == 'Active',
    //         desig: this.designation,
    //         concat: String.ConcatWithSpace(name, '(', deptName ? (dept + ' - ') : '', this.designation, ')').trim().replace(/\s+/g, ' ')
    //     }
    // });

    bookSchema.set('toJSON', {virtuals: true});
    app.db.model('Books', bookSchema);
    app.db.model('BookStock', bookStockSchema);
    app.db.model('BookRequest', bookRequestSchema);
    //experience

};