﻿module.exports = function (app, mongoose) {
    require('./models')(app, mongoose);
    var controller = require('./controller.js');

    app.get('/download/kradefs', controller.downloadKraDefinations);
    app.get('/download/kradata', controller.downloadKraData);
    app.get('/download/finance/year/:year', controller.downloadFinanceYearReport);
    app.get('/download/finance/month/:year/:month', controller.downloadFinanceMonthReport);
}