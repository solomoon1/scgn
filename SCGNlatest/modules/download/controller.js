﻿var sample = [{label: 'First Name', prop: 'firstName'},
    {label: 'Last Name', prop: 'lastName'},
    {label: 'Designation', prop: 'designation'},
    {label: 'Department', prop: 'department.name'}
];

exports.downloadKraDefinations = function (req, res) {
    req.app.db.models.KRA.find()
        .populate('department')
        .populate('area')
        .exec(function (e, r) {
            var opts = [{label: 'Area', prop: 'area.name'},
                {label: 'Description', prop: 'description'},
                {label: 'Department', prop: 'department.name'},
                {label: 'Weight', prop: 'weight'},
                {label: 'Designations', prop: 'designations'}
            ];
            req.app.utility.download(res).send(opts, r, 'kras');
        });
};
exports.downloadKraData2 = function (req, res) {
    //var period =
    var download = req.app.utility.download(res);
    req.app.db.models.EmployeeAppraisal.find({'kra.text': {$ne: null}}, 'employee period kra')
        .populate('employee', 'firstName lastName designation department employeeId')
        .populate('period', 'name year')
        .exec(function (e, r) {
            req.app.db.models.Organisation.populate(r, {path: 'department', select: 'name'}, function () {
                var head = [{label: 'Name', prop: 'employee.fullName', isFunction: true},
                    {label: 'Designation', prop: 'employee.designation'},
                    {label: 'Department', prop: 'employee.department.name'},
                    {label: 'Period', prop: 'period.getName', isFunction: true},
                    {label: 'Score', prop: 'kra.score'},
                    {label: 'Rating', prop: 'kra.text'},
                    {label: 'Date', prop: 'kra.date'},
                    {label: 'By', prop: 'kra.by'}
                ];
                var head2 = [{label: 'Area', prop: 'details.area'},
                    {label: 'KRA', prop: 'details.kra'},
                    {label: 'Score', prop: 'scoring.score'},
                    {label: 'Rating', prop: 'scoring.text'},
                    {label: 'Weight', prop: 'details.weight'},
                    {label: 'Designations', prop: 'designations'}
                ];
                var data = [];
                r.forEach(function (i) {
                    data.push(download.toRowArray(i, head));
                    ((i.kra || {}).list || []).forEach(function (p) {
                        data.push(p);
                    });
                    //data.push(i.kra.list);
                });
                download.send(head2, data, 'Appraisals');
            });

        });
};
exports.downloadKraData = function (req, res) {
    //var period =
    var download = req.app.utility.download(res);
    req.app.db.models.EmployeeAppraisal.find({'kra.text': {$ne: null}}, 'employee period kra')
        .populate('employee', 'firstName lastName designation department employeeId')
        .populate('period', 'name year')
        .exec(function (e, r) {
            req.app.db.models.Organisation.populate(r, {path: 'employee.department', select: 'name'}, function () {
                var head = [{label: 'Name', prop: 'employee.fullName', isFunction: true},
                    {label: 'Designation', prop: 'employee.designation'},
                    {label: 'Department', prop: 'employee.department.name'},
                    {label: 'Period', prop: 'period.getName', isFunction: true},
                    {label: 'Score', prop: 'kra.score'},
                    {label: 'Rating', prop: 'kra.text'},
                    {label: 'Date', prop: 'kra.date'}
                ];
                var head2 = [{label: 'Area', prop: 'details.area'},
                    {label: 'KRA', prop: 'details.kra'},
                    {label: 'Score', prop: 'scoring.score'},
                    {label: 'Rating', prop: 'scoring.text'},
                    {label: 'Weight', prop: 'details.weight'},
                    {label: 'Designations', prop: 'designations'}
                ];
                var data = [];
                r.forEach(function (i) {
                    data.push(download.toArray(i, head));
                    //((i.kra || {}).list || []).forEach(function (p) {
                    //    data.push(p);
                    //});
                    //data.push(i.kra.list);
                });
                download.send(head, data, 'Appraisals');
            });

        });
};
exports.downloadEmployeeData = function (req, res) {

};
exports.downloadAssestList = function (req, res) {

};
exports.downloadAttendanceRecords = function () {
    
};
exports.downloadFinanceMonthReport = function (req, res) {
    console.log('Month:\t%d\tYear:\t&d', req.params.month, req.params.year);
    var download = req.app.utility.download(res);
    var year = req.params.year || new Date().getFullYear(),
        month = req.params.month || 0;
    var start = new Date(year, month, 1),
        lastDay = 28,
        isLeap = function (y) { return (y % 4 == 0 && y % 100 != 0) || (y % 100 == 0 && y % 400 == 0); }
    switch (req.params.month) {
        case 3: case 5: case 8: case 10: lastDay = 30; break;
        case 0: case 2: case 4: case 6: case 7: case 9: case 11: lastDay = 31; break;
        case 1: if (isLeap(y)) lastDay = 29; else lastDay = 28; break; default: break;
    }
    var end = new Date(year, month, lastDay),
        months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        monthName = months[month];
    req.app.db.models.PettyCash
        .aggregate([
                {$match: {$or: [{status: 'Cash Released'}, {status: "Retired"}], disbursed: {$gte: start, $lte: end}}},
                {$group: {_id: {$dayOfMonth: "$disbursed"}, total: {$sum: "$amount"}}},
                {$sort: {_id: 1}}]
            , function (err, pc) {
                var head = [{label: 'Date', prop: '_id'},
                        {label: 'Amount', prop: 'total'}
                    ],data = [], total = 0;
                pc.forEach(function (i) {
                    total += i.total;
                    data.push(download.toArray(i, head));
                });
                data.push(download.toArray({_id: 'Total', total: total}, head));
                var body = String.Concat2('Financial Report for', monthName, year, '\r\n',
                    download.toData(head, data));
                download.sendBody(body, String.Concat2(monthName, year, 'finance'));
            });
};
exports.downloadFinanceYearReport = function (req, res) {
    var download = req.app.utility.download(res);
    var year = req.params.year || new Date().getFullYear();
    var start = new Date(year, 0, 1);
    var end = new Date(year+1, 0, 1);
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    req.app.db.models.PettyCash
        .aggregate([
                {$match: {$or: [{status: 'Cash Released'}, {status: "Retired"}], disbursed: {$gte: start, $lt: end}}},
                {$group: {_id: {$month: "$disbursed"}, total: {$sum: "$amount"}}},
                {$sort: {_id: 1}}]
            , function (err, pc) {
                var head = [{label: 'Month', prop: 'month'},
                        {label: 'Amount', prop: 'total'}
                    ],
                    rangeText = String.Concat2(months[pc[0]._id - 1], year, '-', months[pc[pc.length - 1]._id - 1], year),
                    data = [], total = 0;
                pc.forEach(function (i) {
                    i.month = months[i._id - 1];
                    total += i.total;
                    data.push(download.toArray(i, head));
                });
                data.push(download.toArray({month: 'Total', total: total}, head));
                var body = String.Concat('Financial Report for Year ',
                    year, '\r\n',
                    ',', rangeText, '\r\n',
                    download.toData(head, data));
                download.sendBody(body, String.Concat2(year, ' Year Report'));
            });
};
