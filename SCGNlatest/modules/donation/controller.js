var mongoose = require('mongoose');
var async = require('async');
var moment = require('moment');

exports.getAllDonations = function (req, res) { //return all donations
    var start = moment().startOf('quarter').toDate();
    var end = moment().endOf('quarter').toDate();
    req.app.db.models.Donations.find(
        { $and: [  { createdOn: { $gte: start}}, { createdOn: { $lte: end }}]}, null, {sort: {createdOn: -1}}, function (err, donations) {
        if (err) {
            return res.status(500).json({message: 'Failed to fetch donation(s). ' + err.message});
        }
        res.json({message: 'Donations fetched!', donations: donations});
    });
};



exports.getDonations = function (req, res) { //return categorised donations
    async.parallel(
        {
            online: function (cb) {
                req.app.db.models.Donations.find(
                    {mode: 'Online'}, null, {sort: {createOn: -1}},
                    function (err, d) {
                    cb(err, d);
                })
            },
            offline: function (cb) {
                req.app.db.models.Donations.find(
                    {mode: {$ne: 'Online'}}, null, {sort: {createOn: -1}},
                    function (err, d){ cb(err, d); })
            },
            quarterlyDonations: function (cb) {
                var start = moment().startOf('quarter').toDate();
                var end = moment().endOf('quarter').toDate();
                req.app.db.models.Donations.count(
                    {createdOn: {$gte: start, $lte: end}}, function (err, count) {
                    cb(err, count);
                })
            }
        },
        function (err, results) {
            if (err) return res.status(500).json({message: 'There was an error fetching Donations'});
            var quarter = moment().quarter();
            res.json({
                message: 'Donations fetched',
                online: results.online,
                offline: results.offline,
                quarterlyDonations: results.quarterlyDonations,
                quarter: quarter
            });
        }
    )


};

exports.createDonation = function (req, res) {
    //create a new donation
    //req.app.db.models.User.findOne
    var donationToCreate = req.body;
    var newDonation = new req.app.db.models.Donations();
    var errorMessage = '';

    if (req.body) {
        //ensure the title does not exist
        newDonation = Object.assign(newDonation, donationToCreate);
        newDonation.save(function (err, createdDonation) {
            if (err) {
                errorMessage = 'Error Creating Donation: ' + req.body.title + ' ' + err.message;
                return res.status(500).json({message: errorMessage});
            }
            res.json({message: 'Donation Created!', donation: createdDonation});
        })
    } else {
        errorMessage = 'No payload provide';
        res.status(500).json({message: errorMessage});
    }

};

exports.updateDonation = function (req, res) {
    /*
     update a donation
     this is done by a privileged employee
     most likely fields to change are
     - stockCount
     - editedOn
     -
     */

};


//#endregion
