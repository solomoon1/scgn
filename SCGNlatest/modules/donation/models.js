module.exports = function (app, mongoose) {
    //Donations

    var benefactorSchema = new mongoose.Schema({
        name: { type: String, required: true},
        email: { type: String },
        phone: { type: String }
    });

    var donationSchema = new mongoose.Schema({
        mode: {type: String, required: true, enum: ['Offline-Cash', 'Offline-Cheque', 'Online']},
        amount: { type: Number, required: true},
        remark: { type: String, required: true},
        benefactor: {
            name: { type: String, required: true},
            email: { type: String },
            phone: { type: String }
        },
        employeeId: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'},
        createdOn: { type: Date, default: Date.now }
    });

    // donationSchema.methods.fullName = function () {
    //     return this.firstName + ' ' + this.lastName;
    // };
    // donationSchema.methods.isValidAccess = function () {
    //     return app.constants.access.indexOf(this.access) != -1;
    // };
    // donationSchema.methods.canPlayRoleOf = function (name) {
    //     switch (name.toLowerCase()) {
    //         case 'admin':
    //             return this.access == 'M' || this.access == 'O';
    //         case 'finance':
    //             return this.department && this.department.name == 'Finance';
    //         default:
    //             return false;
    //     }
    // };
    // donationSchema.methods.isPD = function () {
    //     //var l = 'O';
    //     //var result = this.access == l;
    //     //return result;
    //     return this.access == 'O' && this.department.name == 'HR';
    // };
    // donationSchema.methods.isASupervisor = function () {
    //     return this.subordinates.length > 0;
    // };


    // donationSchema.virtual('name').get(function () {
    //    // return this.firstName + ' '+ this.lastName;
    //     return this.fullName();
    // });

    // donationSchema.virtual('extras').get(function () {
    //     var name = this.fullName(),
    //         deptName = (this.department || {}).name || '',
    //         dept = deptName || this.department;
    //     return {
    //         _id: this.id,
    //         name: this.fullName(),
    //         dept: dept,
    //         employeeId:this.employeeId,
    //         active: this.status == 'Active',
    //         desig: this.designation,
    //         concat: String.ConcatWithSpace(name, '(', deptName ? (dept + ' - ') : '', this.designation, ')').trim().replace(/\s+/g, ' ')
    //     }
    // });

    donationSchema.set('toJSON', {virtuals: true});
    app.db.model('Donations', donationSchema);
    //experience

};