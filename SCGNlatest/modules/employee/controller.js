var mongoose = require('mongoose');
var async = require('async');

exports.deleteEmployee = function(req, res){
    var employeeId = req.params.employeeId;
    req.app.db.models.Employee
        .findByIdAndRemove(employeeId, function(err, emp){
            if(err) return res.status(500).json({ message: 'Error deleting the employee'});
            var deleteNotice = emp.fullName() + ' is no longer an employee of SCGN';
            req.app.utility.notify.emit('addNotification',{
                    target: 'all',
                    ttl: 7,
                    module: 'employee',
                    type: 'response',
                    message:  deleteNotice
                },
                function(err, r){
                    if(err) console.log(err);
                    else console.log(deleteNotice);
                });
            res.json({ message: 'Employee was successfully deleted'});
        });

};

exports.createEmployee = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);

    workflow.on('validate', function (){
        var employee = req.body;
        ['firstName', 'lastName', 'gender', 'dob', 'hired', 'pPhone', 'location']
            .forEach(function (item) {
                if (!employee[item]) workflow.outcome.errfor[item] = 'required';
            });
        if (workflow.hasErrors()) {
            workflow.outcome.errors.push('Please ensure that all required fields are entered.');
            return workflow.emit('response');
        }
        else {
            workflow.employee = employee;
            workflow.emit('checkDuplicates');
        }
    });

    workflow.on('checkDuplicates', function () {
        workflow.on('checkUsername', function () {
            var ready = false;
            req.app.db.models.User.findOne({username: req.app.scgn.utility.getUsername(workflow.employee, true)}, function (err, user) {
                if (err) {
                    return workflow.emit('exception', err);
                }
                if (user) {
                    req.app.db.models.User.findOne({username: req.app.scgn.utility.getUsername(workflow.employee, false)}, function (err, user) {
                        if (err) {
                            return workflow.emit('exception', err);
                        }
                        if (user) {
                            workflow.outcome.errors.push('That username is already taken.');
                            return workflow.emit('response');
                        } else {
                            workflow.employee.username = req.app.scgn.utility.getUsername(workflow.employee, false);
                            workflow.emit('checkEmail');
                        }
                    });
                } else {
                    workflow.employee.employeeId = req.app.scgn.utility.getUsername(workflow.employee, true);
                    workflow.emit('checkEmail');
                }
            });
        });
        workflow.on('checkEmail', function () {
            workflow.employee.wEmail = workflow.employee.wEmail ? workflow.employee.wEmail.toLowerCase() : workflow.employee.employeeId.toLowerCase() + '@scgn.com';
            req.app.db.models.User.findOne({email: workflow.employee.wEmail}, function (err, user) {
                if (err) {
                    return workflow.emit('exception', err);
                }

                if (user) {
                    workflow.outcome.errors.push('email already registered.');
                    return workflow.emit('response');
                } else workflow.emit('createUser');
            });
        });
        workflow.emit('checkUsername');
    });

    workflow.on('createUser', function () {
        workflow.on('getHashedPassword', function () {
            req.app.db.models.User.encryptPassword(workflow.employee.firstName.toLowerCase(), function (err, hash) {
                if (err) {
                    workflow.emit('exception', err);
                }
                else {
                    workflow.employee.password = hash;
                    workflow.emit('passwordHashed');
                }
            });
        });
        workflow.on('passwordHashed', function () {
            var fieldsToSet = {
                isActive: 'yes',
                username: workflow.employee.employeeId,
                email: (workflow.employee.pEmail || workflow.employee.wEmail).toLowerCase(),
                password: workflow.employee.password
            };
            req.app.db.models.User.create(fieldsToSet, function (err, user) {
                if (err) {
                    return workflow.emit('exception', err);
                }
                workflow.user = user;
                delete workflow.employee.password;
                workflow.outcome.user = {username: user.username, email: user.email}
                workflow.emit('createEmployee');
            });
        });
        workflow.emit('getHashedPassword');
    });

    workflow.on('createEmployee', function () {
        workflow.outcome.item = workflow.employee;
        workflow.employee.status = 'Active';
        req.app.db.models.Employee.create(workflow.employee, function (err, emp) {
            if (err) workflow.emit('reverseCreateUser', err);
            else {
                workflow.employee = emp;
                workflow.emit('employeeCreated');
                var createNotice = emp.fullName() + ' is now an employee of SCGN';
                req.app.utility.notify.emit('addNotification',{
                        target: 'all',
                        ttl: 7,
                        module: 'employee',
                        type: 'response',
                        message:  createNotice
                    },
                    function(err, r){
                        if(err) console.log(err);
                        else console.log(createNotice);
                    });
            }
        });
    });

    workflow.on('reverseCreateUser', function (er) {
        workflow.user.remove(function (err, t) {
            return workflow.emit('exception', er);
        });
    });

    //update user's employee field
    workflow.on('employeeCreated', function () {
        if (workflow.employee) {
            workflow.user.employee = workflow.employee.id;
            workflow.user.save(function () {
                workflow.emit('response');
                new req.app.db.models.AuditLog({
                    action: 'Create',
                    description: 'New User/Employee created.',
                    employeeId: req.user.employee.employeeId,
                    entityType: 'Employee',
                    entityId: workflow.employee.id,
                    endpoint: req.ip,
                    newValue: JSON.stringify(workflow.user.toObject())
                }).save();
            });
        }
        else workflow.emit('response');
    });
    workflow.on('logInUser', function () {
        workflow.outcome.message = 'Employee Successfully created';
        workflow.outcome.success = true;
        req._passport.instance.authenticate('local', function (err, user, info) {
            if (err) {
                return workflow.emit('exception', err);
            }

            if (!user) {
                workflow.outcome.errors.push('Login failed. That is strange.');
                return workflow.emit('response');
            }
            else {
                req.login(user, function (err) {
                    if (err) {
                        return workflow.emit('exception', err);
                    }
                    workflow.outcome.defaultReturnUrl = user.defaultReturnUrl();
                    workflow.emit('response');
                });
            }
        })(req, res);
    });
    workflow.emit('validate');
};

exports.updateEmployee = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    var task = 'emp';
    var log = req.app.utility.audit.prepareLog(req, 'Employee', 'Update');

    function canEdit(emp, cb) {
        if (employee.employeeId != req.user.username && !(req.user.employee.access == 'M' || req.user.employee.access == 'O' || (req.user.employee.tasks || []).indexOf(task) > -1));
    }

    workflow.on('validate', function () {
        var employee = req.body;
        if (employee.employeeId != req.user.username && !(req.user.employee.access == 'M' || req.user.employee.access == 'O' || (req.user.employee.tasks || []).indexOf(task) > -1))
            workflow.outcome.errors.push('You can only edit your own profile');
        ['firstName', 'lastName', 'gender', 'dob', 'hired', 'pPhone', 'location']
            .forEach(function (item) {
                if (!employee[item])
                    workflow.outcome.errfor[item] = 'required';
            });
        if (workflow.hasErrors()) {
            workflow.outcome.errors.push('Please ensure that all required fields are entered.');
            return workflow.emit('response');
        }
        else {
            workflow.employee = employee;
            return (req.user.employee.access == 'M' || req.user.employee.access == 'O' || (req.user.employee.tasks || []).indexOf(task) > -1) ? workflow.emit('updateAsPd') : workflow.emit('update');
        }
    });
    workflow.on('update', function () {
        var opts = "-employeeId -hired -role -department -access -confirmed -designation -experiences -educations -skills -dependants -leaveStatus -leaveLevel -tasks -location";
        req.app.db.models.Employee.findById(workflow.employee._id, opts, function (err, emp) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (emp) {
                log.setOld(emp);
                req.app.scgn.utility.populate(emp, workflow.employee, ['dob']);
                emp.save(function (er, data) {
                    if (er) return workflow.emit('exception', err);
                    else {
                        workflow.outcome.item = data;
                        workflow.outcome.success = true;
                        workflow.outcome.message = 'Record successfully updated';
                        req.app.utility.audit.updateAndSave(log, 'Employee record updated', data);
                        workflow.emit('response');
                    }
                });
            }
        });
    });
    workflow.on('updateAsPd', function () {
        var opts = "-employeeId -experiences -educations -skills -dependants -leaveStatus -leaveLevel -tasks";
        req.app.db.models.Employee.findById(workflow.employee._id, opts, function (err, emp) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (emp) {
                log.setOld(emp);
                workflow.employee.status = workflow.employee.status || 'Active';
                var removeDetails = workflow.employee.status == 'Inactive' && emp.status != 'Inactive;';
                workflow.employee.department = workflow.employee.department._id || workflow.employee.department.id;
                var include = ['dob'];
                if (workflow.employee.confirmed) include.push('confirmed');
                if (!workflow.employee.status) include.push('status');
                req.app.scgn.utility.populate(emp, workflow.employee, include);
                emp.save(function (er, data) {
                    if (er) return workflow.emit('exception', err);
                    workflow.outcome.item = data;
                    workflow.outcome.success = true;
                    workflow.outcome.message = 'Record successfully updated';
                    req.app.utility.audit.updateAndSave(log, 'Employee record updated(Admin)', data);
                    if (removeDetails) workflow.emit('removeAllEmployeeRelations', emp.id, req.app);
                    workflow.emit('response');
                });
            }
        });
    });
    workflow.on('removeAllEmployeeRelations', function (id, app) {
        app.db.models.Employee
            .find({subordinates: id})
            .sort({firstName: 1, lastName: 1})
            .exec(function (e, sups) {
                if (sups && sups.length) {
                    sups.forEach(function (sup) {
                        var idx = sup.subordinates.indexOf(id);
                        if (idx != -1) {
                            sup.subordinates.splice(idx, 1);
                            sup.save();
                        }
                    })
                }
            });
        app.db.models.Employee
            .find({superior: id})
            .sort({firstName: 1, lastName: 1})
            .exec(function (e, subs) {
                subs.forEach(function (sub) {
                    sub.superior = undefined;
                    sub.save();
                })
            });
    });
    workflow.emit('validate');
};
exports.getActiveEmployees = function (req, res, next) {
    var q = req.app.db.models.Employee
        .find({status: {$ne: 'Inactive'}})
        .populate('department')
        .sort({firstName: 1, lastName: 1})
        .exec(function (err, results) {
            if (err) {
                return next(err);
            }
            res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
            res.send({employees: results, byDepartment: require('underscore').groupBy(results, 'department')});
        });
};
exports.getActiveEmployeesForCard = function (req, res, next) {
    req.app.db.models.Employee
        .find({status: {$ne: 'Inactive'}})
        .populate('department')
        .populate('experiences')
        .populate('educations')
        .populate('skills')
        .populate('dependants')
        .sort({firstName: 1, lastName: 1})
        .exec(function (err, results) {
            if (err) {
                return next(err);
            }
            res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
            res.send({employees: results, byDepartment: require('underscore').groupBy(results, 'department')});
        });
};
exports.getExitedEmployees = function (req, res, next) {
    req.app.db.models.Employee
        .find({status: 'Inactive'})
        .populate('department')
        .sort({firstName: 1, lastName: 1})
        .exec(function (err, results) {
            if (err) {
                return next(err);
            }
            res.send({employees: results, byDepartment: require('underscore').groupBy(results, 'department')});
        });
};

//#region Profile Update
exports.editExperience = function (req, res) {

    var log = req.app.utility.audit.prepareLog(req, 'Experience', 'Update');
    var workflow = req.app.utility.workflow(req, res);
    workflow.on('validate', function () {
        var experience = req.body;
        ['employer', 'startDate', 'endDate']
            .forEach(function (item) {
                if (!experience[item])
                    workflow.outcome.errfor[item] = 'required';
            });
        ['startDate', 'endDate']
            .forEach(function (item) {
                if (experience[item] && false)
                    workflow.outcome.errfor[item] = 'invalid date';
            });
        if (workflow.hasErrors()) {
            workflow.outcome.errors.push('Please ensure that all required fields are entered.');
            return workflow.emit('response');
        }
        else {
            workflow.exp = experience;
            if (experience._id) {// update;
                workflow.emit('update');
            } else {//create
                workflow.emit('create');
            }
        }
    });
    workflow.on('update', function () {
        req.app.db.models.Experience.findById(workflow.exp._id, function (err, ex) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (ex) {
                log.setOld(ex);
                req.app.scgn.utility.populate(ex, workflow.exp);
                ex.save(function (er, data) {
                    if (er) return workflow.emit('exception', err);
                    else {
                        workflow.outcome.item = data;
                        req.app.utility.audit.updateAndSave(log, 'Experience updated', data);
                        workflow.outcome.success = true;
                        workflow.outcome.message = 'Experience successfully updated';
                        workflow.emit('response');
                    }
                });
            }
            else {
                delete workflow.exp._id;
                workflow.emit('create');
            }
        });
    });
    workflow.on('create', function () {
        req.app.db.models.Experience.create(workflow.exp, function (err, exp) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (exp) {
                req.app.utility.audit.updateAndSave(log, 'Experience added', exp, 'Create');
                workflow.outcome.item = exp;
                workflow.emit('updateEmployee');
            }
        });
    });
    workflow.on('updateEmployee', function () {
        req.app.db.models.Employee
            .findById(workflow.exp.employee)
            .populate('experiences')
            .sort({firstName: 1, lastName: 1})
            .exec(function (err, emp) {
                if (err) {
                    return workflow.emit('exception', err);
                }
                else if (emp) {
                    emp.experiences.push(workflow.outcome.item.id);
                    emp.save(function (er, data) {
                        if (er) return workflow.emit('exception', err);
                        else {
                            workflow.outcome.success = true;
                            workflow.outcome.message = 'Experience successfully added';
                            workflow.emit('response');
                        }
                    });
                }
                else {
                    workflow.outcome.errors.push('Employee record not found.');
                    workflow.emit('response');
                }
            });
    });

    workflow.emit('validate');
};
exports.editEducation = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    var log = req.app.utility.audit.prepareLog(req, 'Education', 'Update');
    workflow.on('validate', function () {
        var education = req.body;
        ['school', 'major', 'grade', 'startDate', 'endDate']
            .forEach(function (item) {
                if (!education[item])
                    workflow.outcome.errfor[item] = 'required';
            });
        ['startDate', 'endDate', 'dateAwarded']
            .forEach(function (item) {
                if (education[item] && false)
                    workflow.outcome.errfor[item] = 'invalid date';
            });
        ['qualification']
            .forEach(function (item) {
                if (education[item] && req.app.constants.qualifications.indexOf(education[item]) == -1)
                    workflow.outcome.errfor[item] = 'invalid qualification';
            });
        if (workflow.hasErrors()) {
            workflow.outcome.errors.push('Please ensure that all required fields are entered.');
            return workflow.emit('response');
        }
        else {
            workflow.edu = education;
            if (education._id) {// update;
                workflow.emit('update');
            } else {//create
                workflow.emit('create');
            }
        }
    });
    workflow.on('update', function () {
        req.app.db.models.Education.findById(workflow.edu._id, function (err, ed) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (ed) {
                log.setOld(ed);
                req.app.scgn.utility.populate(ed, workflow.edu);
                ed.save(function (er, data) {
                    if (er) return workflow.emit('exception', err);
                    else {
                        workflow.outcome.item = data;
                        workflow.outcome.success = true;
                        req.app.utility.audit.updateAndSave(log, 'Education added', data);
                        workflow.outcome.message = 'Education successfully updated';
                        workflow.emit('response');
                    }
                });
            }
            else {
                delete workflow.exp._id;
                workflow.emit('create');
            }
        });
    });
    workflow.on('create', function () {
        req.app.db.models.Education.create(workflow.edu, function (err, edu) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (edu) {
                workflow.outcome.item = edu;
                req.app.utility.audit.updateAndSave(log, 'Education added', edu, 'Create');
                workflow.emit('updateEmployee');
            }
        });
    });

    workflow.on('updateEmployee', function () {
        req.app.db.models.Employee
            .findById(workflow.edu.employee)
            .populate('educations')
            .sort({firstName: 1, lastName: 1})
            .exec(function (err, emp) {
                if (err) {
                    return workflow.emit('exception', err);
                }
                else if (emp) {
                    emp.educations.push(workflow.outcome.item.id);
                    emp.save(function (er, data) {
                        if (er) return workflow.emit('exception', err);
                        else {
                            workflow.outcome.success = true;
                            workflow.outcome.message = 'Education successfully added';
                            workflow.emit('response');
                        }
                    });
                }
                else {
                    workflow.outcome.errors.push('Employee record not found.');
                    workflow.emit('response');
                }
            });
    });
    workflow.emit('validate');
};
exports.editSkill = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    var log = req.app.utility.audit.prepareLog(req, 'Skill', 'Update');
    workflow.on('validate', function () {
        var skill = req.body;
        ['name', 'date', 'level']
            .forEach(function (item) {
                if (!skill[item])
                    workflow.outcome.errfor[item] = 'required';
            });
        ['date']
            .forEach(function (item) {
                if (skill[item] && false)
                    workflow.outcome.errfor[item] = 'invalid date';
            });
        ['skill']
            .forEach(function (item) {
                if (skill[item] && req.app.constants.skills.indexOf(education[item]) == -1)
                    workflow.outcome.errfor[item] = 'skill level no recognized';
            });
        if (workflow.hasErrors()) {
            workflow.outcome.errors.push('Please ensure that all required fields are entered.');
            return workflow.emit('response');
        }
        else {
            workflow.ski = skill;
            if (skill._id) {// update;
                workflow.emit('update');
            } else {//create
                workflow.emit('create');
            }
        }
    });
    workflow.on('update', function () {
        req.app.db.models.Skill.findById(workflow.ski._id, function (err, sk) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (sk) {
                log.setOld(sk);
                req.app.scgn.utility.populate(sk, workflow.ski);
                sk.save(function (er, data) {
                    if (er) return workflow.emit('exception', err);
                    else {
                        workflow.outcome.item = data;
                        workflow.outcome.success = true;
                        req.app.utility.audit.updateAndSave(log, 'Skill updated', data, 'Update');
                        workflow.outcome.message = 'Skill successfully updated';
                        workflow.emit('response');
                    }
                });
            }
            else {
                delete workflow.ski._id;
                workflow.emit('create');
            }
        });
    });
    workflow.on('create', function () {
        req.app.db.models.Skill.create(workflow.ski, function (err, ski) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (ski) {
                workflow.outcome.item = ski;
                req.app.utility.audit.updateAndSave(log, 'Skill added', ski, 'Create');
                workflow.emit('updateEmployee');
            }
        });
    });

    workflow.on('updateEmployee', function () {
        req.app.db.models.Employee
            .findById(workflow.ski.employee)
            .populate('skills')
            .sort({firstName: 1, lastName: 1})
            .exec(function (err, emp) {
                if (err) {
                    return workflow.emit('exception', err);
                }
                else if (emp) {
                    emp.skills.push(workflow.outcome.item.id);
                    emp.save(function (er, data) {
                        if (er) return workflow.emit('exception', err);
                        else {
                            workflow.outcome.success = true;
                            workflow.outcome.message = 'Skill successfully added';
                            workflow.emit('response');
                        }
                    });
                }
                else {
                    workflow.outcome.errors.push('Employee record not found.');
                    workflow.emit('response');
                }
            });
    });
    workflow.emit('validate');
};
exports.editDependant = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    var log = req.app.utility.audit.prepareLog(req, 'Dependant', 'Update');
    workflow.on('validate', function () {
        var dependant = req.body;
        ['firstName', 'lastName']
            .forEach(function (item) {
                if (!dependant[item])
                    workflow.outcome.errfor[item] = 'required';
            });
        ['effectiveDate', 'EndDate', 'dob']
            .forEach(function (item) {
                if (dependant[item] && false)
                    workflow.outcome.errfor[item] = 'invalid date';
            });
        ['gender']
            .forEach(function (item) {
                if (dependant[item] && req.app.constants.gender.indexOf(dependant[item]) == -1)
                    workflow.outcome.errfor[item] = 'skill level no recognized';
            });
        if (workflow.hasErrors()) {
            workflow.outcome.errors.push('Please ensure that all required fields are entered.');
            return workflow.emit('response');
        }
        else {
            workflow.dep = dependant;
            if (dependant._id) {// update;
                workflow.emit('update');
            } else {//create
                workflow.emit('create');
            }
        }
    });
    workflow.on('update', function () {
        req.app.db.models.Dependant.findById(workflow.dep._id, function (err, dp) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (dp) {
                log.setOld(dp);
                req.app.scgn.utility.populate(dp, workflow.dep);
                dp.save(function (er, data) {
                    if (er) return workflow.emit('exception', err);
                    else {
                        workflow.outcome.item = data;
                        workflow.outcome.success = true;
                        req.app.utility.audit.updateAndSave(log, 'Dependant updated', data);
                        workflow.outcome.message = 'Dependant successfully updated';
                        workflow.emit('response');
                    }
                });
            }
            else {
                delete workflow.dep._id;
                workflow.emit('create');
            }
        });
    });
    workflow.on('create', function () {
        req.app.db.models.Dependant.create(workflow.dep, function (err, dep) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (dep) {
                workflow.outcome.item = dep;
                req.app.utility.audit.updateAndSave(log, 'Dependant added', dep, 'Create');
                workflow.emit('updateEmployee');
            }
        });
    });
    workflow.on('updateEmployee', function () {
        req.app.db.models.Employee
            .findById(workflow.dep.employee)
            .populate('dependants').exec(function (err, emp) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (emp) {
                emp.dependants.push(workflow.outcome.item.id);
                emp.save(function (er, data) {
                    if (er) return workflow.emit('exception', err);
                    else {
                        workflow.outcome.success = true;
                        workflow.outcome.message = 'Dependant successfully updated';
                        workflow.emit('response');
                    }
                });
            }
            else {
                workflow.outcome.errors.push('Employee record not found.');
                workflow.emit('response');
            }
        });
    });
    workflow.emit('validate');
};
exports.changePicture = function (req, res) {
    var fs = require('fs');
    req.files = req.myFile;
    var file = (req.files || {}).file;
    if (file && file.path) {
        var filePath = file.path;
        var dest = 'public/img/' + req.user.username + req.myFileExt;
        var imgPath = '/img/' + req.user.username + req.myFileExt;

        async.parallel(
            {
                empImgUpdate: function (cb) {
                    req.app.db.models.Employee.findByIdAndUpdate(
                        {_id: req.user.employee._id},
                        {$set: {img: imgPath}},
                        {new: true},
                        function (err, emp) {
                            cb(err, emp);
                        }
                    );
                },
                renameImg: function (cb) {
                    fs.unlink(dest, function (err) {
                            if (!err || err.errno.toString() === '-4058'||err.errno.toString() === '-2' || err.errno.toString() === '34')
                                fs.rename(filePath, dest, function (e) {
                                    new req.app.db.models.AuditLog({
                                        action: 'Update',
                                        description: 'Changed profile picture',
                                        employeeId: req.user.employee.employeeId,
                                        entityType: 'Employee',
                                        entityId: req.user.employee.id,
                                        endpoint: req.ip
                                    }).save(function (err) {
                                        cb(err, true);
                                    });
                                });
                            else
                                fs.unlink(filePath, function (err) {
                                    cb(err, false);
                                });
                        });
                }
            }
            ,
            function (err, r) {
                if (err) {
                    return res.status(400).json({success: false, message: 'Could not complete request.'});
                }
                else res.json({
                    success: true,
                    message: 'Successfully updated picture'
                });
            });
    }
    else res.json({success: false, message: "No file was uploaded."})
};

exports.getSupervisorsWeeklyReports = function (req, res) {
    var employeeId = req.params && req.params.employeeId,
        query = employeeId ? {supervisor: mongoose.Types.ObjectId(employeeId)} : {supervisor: null};
    if (employeeId){
        var async = require('async');
        async.parallel([
            function (cb){
                req.app.db.models.WeeklyReports.aggregate(
                    [
                    {$match: {supervisor: mongoose.Types.ObjectId(employeeId)}},
                    {
                        $group: {
                            _id: {day: {$dayOfMonth: "$date"}, month: {$month: "$date"}, year: {$year: "$date"}},
                            count: {$sum: 1},
                            reports: {$push: "$$CURRENT"}
                        }
                    },
                    {
                        $group: {
                            "_id": "$employee",
                            "count": {$sum: 1},
                            item: {
                                $push: {
                                    supervisor: "$$CURRENT.supervisor",
                                    month: {$month: "$$CURRENT.date"},
                                    day: {$dayOfMonth: "$$CURRENT.date"},
                                    year: {$year: "$$CURRENT.date"}
                                }
                            }
                        }
                    },
                    {$sort: {"item.month": -1}}
                    // { $group: {
                    //     _id: "$employee",
                    //     date: { day: { $dayOfMonth:     "$date"}, month: { $month: "$date"}, year: { $year: "$date"} },
                    //     count: { $sum: 1},
                    //     reports: { $push: "$$CURRENT"}
                    // }}
                ]
                        [
                        {$match:{"supervisor": mongoose.Types.ObjectId(employeeId)}},

                            {
                                $group: {
                                    _id: { month: {$month: "$date"}, year: {$year: "$date"}, employee: "$employee"},
                                    count: {$sum: 1},
                                    reports: {$push: "$$CURRENT"}
                                }
                            },
                            {
                                $project: {
                                    month: "$_id.month", year: "$_id.year", employee: "$_id.employee",
                                    count: 1,
                                    _id:0,
                                    reports: 1
                                }
                            },
                            {
                                $group: {
                                    _id: {month: "$month", year:"$year"},
                                    employee: {$last: "$employee"},
                                    count: {$sum: 1},
                                    reports: {$push: "$$CURRENT"}
                                }
                            },

                            {
                                $project: {
                                    month: "$_id.month",
                                    year: "$_id.year",
                                    count: 1,
                                    _id:0,
                                    reports: 1
                                }
                            },

                            {$sort: {"month": -1}}

                        ]
                    , cb);
            },
            function (cb) {
                req.app.db.models.WeeklyReports.aggregate([
                    {$match: {employee: mongoose.Types.ObjectId(employeeId)}},
                    {
                        $group: {
                            _id: {month: {$month: "$date"}, year: {$year: "$date"}},
                            count: {$sum: 1},
                            reports: {$push: "$$CURRENT"}
                        }
                    }
                ], cb)
            }
        ], function (err, result) {
            if (err) return res.status(500).json({message: 'Error fetching reports'});
            var supervisorReports = result[0];
            var ownReports = result[1];
            req.app.db.models.Employee
                .populate(supervisorReports,
                    {path: 'reports.reports.supervisor reports.employee', select: {firstName: 1, lastName: 1}}, function (e, r) {
                        res.json({ownReports: result[1], supervisorReports: r});
                    })
        });
    } else {
        res.status(404).json({message: 'Employee not found.'});
    }
};


exports.getOwnWeeklyReports = function (req, res) {
    var employeeId = req.params && req.params.employeeId,
        query = employeeId ? {employee: mongoose.Types.ObjectId(employeeId)} : {employee: null};
    if (query.employee) {
         req.app.db.models.WeeklyReports.find(query)
             .populate('employee', 'firstName lastName middleName designation department')
             .populate('supervisor', 'firstName lastName middleName designation department')
             .exec(function (err, reports) {
                 if (err) return workflow.emit('exception', err);
                 else res.json({message: 'Own Reports fetched!', reports: reports});
             });

        req.app.db.models.WeeklyReports.aggregate([
            {$match: query},
            {
                $group: {
                    _id: {day: {$dayOfMonth: "$date"}, month: {$month: "$date"}, year: {$year: "$date"}},
                    count: {$sum: 1},
                    reports: {$push: "$$CURRENT"}
                }
            }
        ], function (err, reports) {
            if (err) return res.status(500).json({message: 'Error fetching own reports'});
            res.json({message: 'Own reports fetch success', reports: reports});
        })
    } else {
        res.status(404).json({message: 'Supervisor\'s id not fount'});
    }
};

exports.postWeeklyReport = function (req, res) {
    var report = new req.app.db.models.WeeklyReports();
    Object.assign(report, req.body);
    report.save(function (err, report) {
        if (err) return res.status(500).json({message: err.message, errors: [err.message]});
        var msg = req.body.employeeName + ' submitted a weekly report';
        return req.app.utility.notify.
            emit('addNotification',
            { employeeId: report.employee,
                message: msg,
                viewers: [report.supervisor],
                type: 'request',
                target: 'supervisor',
                ttl: 1,
                module: 'weekly report'
            },
            function(err, r){
                if(err) console.error(err);
                res.json({
                    message: 'Report successfully saved!',
                    data: report, success: true
                });
            }
        );
    });
};

//#endregion
