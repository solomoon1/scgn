/**
 * Created by toluogunremi on 3/12/15.
 */
'use strict';

module.exports = function (config) {
    config.AppOwner = {
        name: 'Society For Corporate Governance',
        abbrev: 'SCGN'
    };
    //config.host = 'localhost:3040';
    //in production change this to the site's name eg. scgn.trusoftng.com or the server's ip
    config.host = 'scgnportal.org';
    config.requireAccountVerification = false;
    config.sendWelcomeEmail = false;
    config.defaultUser = {
        username: 'hr',
        email: 'hr@scgn.com',
        password: 'scgn.234',
        roles: {
            admin: {
                name: {
                    first: 'Administrator',
                    last: 'System',
                    full: 'System Administrator'
                },
                groups: ['root']
            }
        }
    };
    config.tranxParameter = {
        merchantId: 'SCGN',
        secretKey: 'CA24AC34952FB7BFC293F6451A5AAB82737CF9170E49F552',
        initialURL: 'http://196.46.20.80:8085/',
        verificationURL: 'http://196.46.20.80:8085/status/',
        returnUrl: function (req, api) {
            return req.protocol + '://' + config.host + api;
        },
        headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
    }
};