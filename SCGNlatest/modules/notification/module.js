﻿module.exports = function (app, mongoose) {
    require('./models')(app, mongoose);
    var controller = require('./controller.js');
/*
    Get the notifications for the currently logged in user
 */
 app.post('/api/notify', controller.getNotifications);

};