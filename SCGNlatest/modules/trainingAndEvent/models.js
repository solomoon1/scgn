module.exports = function (app, mongoose) {
    //Training And Event
    var tneSchema = new mongoose.Schema({
        employeeId: {type: String, required: true},
        title: {type: String, required: true},
        fee: {type: Number, default: ''},
        venue: {type: String, required: true},
        contactName: String,
        contactPhone: String,
        contactEmail: String,
        isActive: { type: Boolean, default: false, enum: [false, true] },
        start: Date,
        end: Date,
        status: { type: String, default: 'Active', enum: ['Active', 'Postponed', 'Concluded']},
        type: {type: String, default: 'Active', required: true, enum: ['Event', 'Training']},
        createdOn: { type: Date, default: Date.now }
    });
    tneSchema.post('save', function (model, next) {
        if(this.isNew) next();
        var ct = new Date().getTime();
        if( this.status && this.status !== 'Concluded' && ct > this.end.getTime() ) this.status = 'Concluded';
        next();
    });

    // memberSchema.set('toObject',{virtuals: true});
    // tneSchema.set('toJSON', {virtuals: true});
    app.db.model('TrainingAndEvent', tneSchema);

};