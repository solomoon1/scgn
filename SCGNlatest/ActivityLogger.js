var path = require('path'),
    fs = require('fs')
    // mongoose = require('mongoose')
;
/*
    task: logs the activity data for the following
        - books
        - book-orders
        - donations
 argument : Object
    type: Books | BookOrder | Donation | transactions | errors
    status: transaction summary
    createdOn: current datetime
    employeeId: optional
 */
var Logger = {};

Logger.getFilePath = function(filename){
    var rootDir = path.dirname(require.main.filename);
    return path.join( rootDir + '/Logs/', filename);
};

Logger.log = function(msgObj){
    var d = new Date();
    var filename = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + '.txt';
    filename = Logger.getFilePath(filename);
    // mongoose.models.Employee.
    var log = "[" + d.toLocaleTimeString() + "]" + " :: "  + "Type: " + msgObj.type + "; Summary: " + msgObj.status + " by: " + (msgObj.employeeId || 'Optional') + '\r\n';
    fs.appendFile(filename, log, function(err) {
        if (err) console.error(err.message);
    });
};

module.exports = Logger;

