/**
 * Created by toluogunremi on 3/12/15.
 */
'use strict';

exports.port = process.env.PORT || 3050;
exports.mongodb = {
    uri: 'mongodb://127.0.0.1/scgn-hr'
};
exports.companyName = 'Society For Corporate Governance Nigeria';
exports.projectName = 'SCGN - HR System';
exports.systemEmail = 'scgn.enterserv@corpgovnigeria.com';
exports.cryptoKey = 'k3yb0ardj4t';

exports.recaptchaol = {
    key: '6LenVQMTAAAAAOFCK2LGvfRdbmc0GcUFzSw3SuF4',
    secret: '6LenVQMTAAAAADeP8j6DcwatNcXyiBOJ8GH8-BpZ'
};
exports.recaptcha = {
    key: '6LfwxgMTAAAAACgvL07nP_zCmPvBr9DuGQvNyy0U',
    secret: '6LfwxgMTAAAAAP77SQfli2w_ik93f79MZFsMA8Bj-BpZ'
};
exports.loginAttempts = {
    forIp: 50,
    forIpAndUser: 7,
    logExpiration: '20m'
};
exports.smtp = {
    from: {
        name: process.env.SMTP_FROM_NAME || exports.projectName,
        address: process.env.SMTP_FROM_ADDRESS || exports.systemEmail
    },
    credentials: {
        // for scgn yandex mail
        user: process.env.SMTP_USERNAME || 'scgnenterpriseserver@gmail.com',
        password: process.env.SMTP_PASSWORD || 'server2018',
        host: process.env.SMTP_HOST || 'smtp.gmail.com',
        //tls: true,
        ssl: true,
        timeout: 300000
        
    }

    // credentials: {
    //     user: process.env.SMTP_USERNAME || 'trusoftng@gmail.com',
    //     password: process.env.SMTP_PASSWORD || 'cdwtyrcxuoidmpzj',
    //     host: process.env.SMTP_HOST || 'smtp.gmail.com',
    //     ssl: true,
    //     timeout: 300000
    // }

};

exports.employees = [
    {firstName: 'Hilda', lastName: 'Nkor', department: 'HR', designation: 'CEO', access: 'Q'},
    {firstName: 'Chioma', lastName: 'Mordi', department: 'HR', designation: 'COO', access: 'O'},
    {
        firstName: 'Lovelyn',
        lastName: 'Okafor',
        department: 'Business Development & Administration',
        designation: 'Executive Sales & Operational Assistant',
        access: 'N'
    },
    {
        firstName: 'Bamidele',
        lastName: 'Ajamu',
        department: 'Research & Publication',
        designation: 'Research Assistant',
        access: 'M'
    },
    {
        firstName: 'Oladotun',
        lastName: 'Aladelua',
        department: 'Research & Publication',
        designation: 'Research Analyst',
        access: 'M'
    },
    {firstName: 'Israel', lastName: 'Unknown', department: 'Administration', designation: 'Driver I', access: 'K'},
    {firstName: 'Godwin', lastName: 'Adache', department: 'Administration', designation: ' Driver I', access: 'K'},
    {
        firstName: 'Ijeoma',
        lastName: 'Ejim',
        department: 'Administration',
        designation: 'Front Desk Officer',
        access: 'K'
    },

    // {
    //     firstName: 'Christopher',
    //     lastName: 'Abuah',
    //     department: 'Information Technology',
    //     designation: 'IT Manager ',
    //     access: 'N'
    // },

    {firstName: 'Michael', lastName: 'Umoh', department: 'Administration', designation: 'Cleaner', access: 'K'},
    {firstName: 'Samson', lastName: 'Orabiyi', department: 'Administration', designation: 'Cleaner', access: 'K'},
    {
        firstName: 'Andy',
        lastName: 'Adade',
        department: 'Administration',
        designation: 'Cafeteria Assistant',
        access: 'K'
    }
];

/*
    The leave configuration has two levels, Level 1 and Level 2
    Employees in Level 1 have more leave allowance
    The list below species the allocated leave levels for both levels for:
        1. Annual leave 2. Casual leave 3. Sick leave
 */
exports.leaveEntitlementSettings = [
    {
        "name" : "Annual Leave",
        "days" : 10,
        "countType" : "Only Working Days",
        "level" : "Level 2"
    },
    {
        "name" : "Annual Leave",
        "days" : 15,
        "countType" : "Only Working Days",
        "level" : "Level 1"
    },
    {
        "name" : "Casual Leave",
        "days" : 3,
        "countType" : "Only Working Days",
        "level" : "Level 1"
    },
    {
        "name" : "Casual Leave",
        "days" : 3,
        "countType" : "Only Working Days",
        "level" : "Level 2"
    },
    {
        "name" : "Sick Leave",
        "days" : 10,
        "countType" : "Only Working Days",
        "level" : "Level 2"
    },
    {
        "name" : "Sick Leave",
        "days" : 10,
        "countType" : "Only Working Days",
        "level" : "Level 1"
    }
];