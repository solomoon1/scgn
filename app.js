'use strict';

//dependencies
var config = require('./config'),
    express = require('express'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    multer = require('multer'),
    session = require('express-session'),
    mongoStore = require('connect-mongo')(session),
    jade = require('jade'),
    http = require('http'),
    path = require('path'),
    mongoose = require('mongoose'),
    helmet = require('helmet'),
    passport = require('passport');

//create express app
var app = express();
//keep reference to config
app.config = config;
//setup the web server
app.server = http.createServer(app);

//setup mongoose
 mongoose.Promise = global.Promise;
app.db = mongoose.createConnection(config.mongodb.uri);
app.db.on('error', console.error.bind(console, 'mongoose connection error: '));
app.db.once('open', function () {
});

//settings
app.disable('x-powered-by');
//app.set('port', config.port);
app.set('views', path.join(__dirname, 'modules/templates'));
app.set('view engine', 'jade');

//middleware
app.use(require('morgan')('dev'));
app.use(require('compression')());
// app.use(multer({dest: './public/img'}));
app.use('/static', express.static(path.join(__dirname, 'public')))
app.use(require('serve-static')(path.join(__dirname, 'public')));
app.use(require('method-override')());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true, limit: '1000mb' }));
app.use(cookieParser(config.cryptoKey));
app.clearStore = function (req) {
    if ((req.signedCookies && req.signedCookies['connect.sid']) || (req.cookies && req.cookies['connect.sid'])) {
        req.app.db.models.Store.remove({_id: req.signedCookies['connect.sid'] || req.cookies['connect.sid']});
    }
};
app.get('/state', function (req, res) {
    function clear() {
        res.clearCookie('provision');
    }

    req.app.clearStore(req);
    res.cookie('connect.sid', '', {expires: new Date(Date.now() + 1000 * 30), path: '/'});
    function sendFalse() {
        res.json({msg: 'This is NOT an authorized machine or browser for Attendance Tracking.'});
    }

    if (!req.signedCookies.provision) return sendFalse();

    var ref = JSON.parse(req.signedCookies.provision).ref;

    req.app.db.models.AttendanceSystem.findOne({referenceNumber: ref}, function (e, t) {
        if (t && t.status == 'Active') res.json({success: true});
        else sendFalse();
    });

});
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: config.cryptoKey,
    //cookie: {httpOnly: true, maxAge: 30 * 60000},
    //rolling: true,
    store: new mongoStore({url: config.mongodb.uri})
}));

app.use(passport.initialize());
app.use(passport.session(
    //{
    //    cookie: {httpOnly: true, maxAge: 1 * 60 * 1000}
    //    //cookie: {httpOnly: true, maxAge: 600000}
    //}
));
//app.use(csrf({ cookie: { signed: true } }));
helmet(app);

//response locals
app.use(function (req, res, next) {
    //res.cookie('_csrfToken', req.csrfToken());
    res.locals.user = {};
    res.locals.user.defaultReturnUrl = req.user && req.user.defaultReturnUrl();
    res.locals.user.username = req.user && req.user.username;
    next();
});

app.use(function (req, res, next) {
    req.scgnIP = req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;
    next();
});
//global locals
app.locals.projectName = app.config.projectName;
app.locals.copyrightYear = new Date().getFullYear();
app.locals.copyrightName = app.config.companyName;
app.locals.cacheBreaker = 'br34k-01';
app.appVersion = require('./package.json').version;
app.templates = {
    query: jade.compileFile(__dirname + '/modules/templates/scgn/query.jade', {
        filename: 'query',
        cache: true
    }),
    matter: jade.compileFile(__dirname + '/modules/templates/scgn/query.jade', {
        filename: 'query',
        cache: true
    }),
    vacation: jade.compileFile(__dirname + '/modules/templates/scgn/vacation.jade', {
        filename: 'query',
        cache: true
    }),
    attendance: jade.compileFile(__dirname + '/modules/templates/scgn/attendance.jade', {
        filename: 'query',
        cache: true
    }),
    confirmation: jade.compileFile(__dirname + '/modules/templates/scgn/confirmation.jade', {
        filename: 'query',
        cache: true
    }),
    tranxReceipt: jade.compileFile(__dirname + '/modules/templates/tranxReceipt.jade', {
        filename: 'query',
        cache: true
    })
    ,
    newMember: jade.compileFile(__dirname + '/modules/templates/scgn/newMember.jade', {
        filename: 'query',
        cache: true
    })
    ,
    memberResetPassword: jade.compileFile(__dirname + '/modules/templates/scgn/memberResetPassword.jade', {
        filename: 'query',
        cache: true
    })
    ,
    newPublication: jade.compileFile(__dirname + '/modules/templates/scgn/newPublication.jade', {
        filename: 'query',
        cache: true
    })
    ,
    newSCGNEvents: jade.compileFile(__dirname + '/modules/templates/scgn/newEvents.jade', {
        filename: 'query',
        cache: true
    })

};
app.constants = require('./public/javascripts/constants.json');

require('./util/scgn/prototypes');
require('./logger');
//setup modules
require('./modules/system/module')(app, mongoose);
// require('./test')(app, mongoose);
require('./util/migration')(app);

//non service routes
app.get('/img/*', function (req, res) {
    res.sendFile('profile.png', {root: './public/images'});
});
app.get('/static/getExcelFile', function (req, res) {
    res.sendFile('scgn-membership.xlsx', {root: './public/static'});
});
app.get('/javascripts/prototypes*', function (req, res) {
    res.sendFile('prototypes.js', {root: './util/scgn'});
});
app.get('*', function (req, res) {
    var sp = req.path.split('.');
    console.log(req.headers['content-type'], req.headers['accept']);
    if (req.app.scgn.isJSON(req))
     //is JSON request
        return res.status(404).json({success: false, message: 'Resource not found', error: 'Resource not found.'});
    if (sp.length > 1) //Request with an extension
        return res.status(404).end('File not found');
    res.set('Last-Modified', new Date().toUTCString());
    if (req.user && req.user.username && req.user.employee && req.isAuthenticated())
        return res.sendFile('home.html', {root: './views'});
    if (req.user && req.user.username && req.user.member && req.isAuthenticated())
        return res.sendFile('members.html', {root: './views'});
    req.session.destroy(function () {
        req.app.clearStore(req);
        res.cookie('connect.sid', '', {expires: new Date(Date.now() + 1000 * 30), path: '/'});
        res.sendFile('login.html', {root: './views'});
    });
});
//custom (friendly) error handler
var errors = require('./errors');
app.all('*', errors.http404);
app.use(errors.http500);


//setup utilities
app.scgn = require('./util/scgn/utilityfunctions');
app.utility = {
    sendmail: require('./util/sendmail'),
    slugify: require('./util/slugify'),
    workflow: require('./util/workflow'),
    auto: require('./util/scgn/automated')(app),
    attendance: require('./util/scgn/attendance'),
    notify: require('./util/scgn/notification')(app),
    download: require('./util/scgn/downloadfile'),
    appraisal: require('./util/scgn/appraisal')(app),
    audit: require('./util/scgn/auditlog')(app)
};
require('./util/scgn/attTimer')(app);

//listen up
app.server.listen(app.config.port, function () { });
console.log("Port: " + app.config.port);