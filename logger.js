/**
 * Created by Ubiri Uzoezi on 18/02/2017.
 */
var winston = require('winston'),
    pack = require('./package.json'),
    moment = require('moment'),
    fs = require('fs'),
    logger = new (winston.Logger)(),
    first = true,
    log_dir = 'scgn_logs',
    separator = process.env.PUBLIC ? '\\' : '/';

/**
 function pad(n) {
    return n < 10 ? '0' + n.toString(10) : n.toString(10);
}
 function formattedDate() {
    var st = new Date();
    return st.getFullYear() + '-' + pad(st.getMonth() + 1) + '-' + pad(st.getDate());
}
 function getPath() {
    //return './'
    return (process.env.PUBLIC || process.env.HOME) + (process.env.PUBLIC ? '/trusoft_logs/' : '/trusoft_logs/')
}
 function restart() {
    clearTransport();
    //setupLogPath();
    logger
        .add(winston.transports.File, {
            filename: getPath() + formattedDate() + '-' + fileName,
            json: false,
            showLevel: false,
            handleExceptions: true,
            humanReadableUnhandledException: true,
            level: 'debug',
            safe: false,
            name: pack.name + '-info',
            timestamp: function () {
                // return Date.formatTime(new Date());
                return moment(new Date()).format('hh:mm a');
            },
            formatter: function (options) {
                return options.timestamp() + ':\n\t' + (undefined !== options.message ? options.message : '') +
                    (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '' ) + '\t';
            }
        })
        .add(winston.transports.Console, {
            handleExceptions: true,
            json: false,
            humanReadableUnhandledException: true,
            colorize: true
        });
    !first ? logger.log('error', 'Logger Renamed @ %s', new Date().toLocaleString()) : first = !first;
    logger.info('Log Path: ' + getPath() + formattedDate() + '-' + fileName);
    logger.close();

    var e = Date.today();
    e.setHours(24);
    setTimeout(restart, e - Date.now() + 1000);
}

 **/
function clearTransport() {
    [pack.name + '-info', winston.transports.Console].forEach(function (n) {
        try {
            logger.remove(n);
        } catch (e) {
        }
    });
}


function recycleLogger() {
    var d = Date.today();
    setupLogPathSync(d);
    clearTransport();
    setTransport(getFileName());
    !first ? logger.log('error', 'Logger Renamed @ %s', new Date().toLocaleString()) : first = !first;
    logger.info('Log Path: ' + getFileName(d));
    logger.close();

    var e = Date.today();
    e.setHours(24);
    setTimeout(recycleLogger, e - Date.now() + 1000);
    setupLogPath(d, function () {
    });
}
function setTransport(path) {
    logger
        .add(winston.transports.File, {
            filename: path,
            json: false,
            showLevel: false,
            handleExceptions: true,
            humanReadableUnhandledException: true,
            level: 'debug',
            safe: false,
            name: pack.name + '-info',
            timestamp: function () {
                // return Date.formatTime(new Date());
                return moment(new Date()).format('hh:mm a');
            },
            formatter: function (options) {
                return options.timestamp() + ':\n\t' + (undefined !== options.message ? options.message : '') +
                    (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '' ) + '\t';
            }
        })
        .add(winston.transports.Console, {
            handleExceptions: true,
            json: false,
            humanReadableUnhandledException: true,
            colorize: true
        });
}
function getFileName(date) {
    return getLogPath()
        + moment(date).format('YYYY-MM MMMM-').replace(/-/g, separator)
        + (pack.name || '')
        + '-'
        + moment(date).format('YYYY-MM-DD')
        + '.log';
}
function getLogPath() {
    return (process.env.PUBLIC || '/home/trusoftng/' || process.env.HOME) +
        separator + log_dir + separator;
}
function setupLogPath(date, cbx) {
    var count = 0, arr = [
        getLogPath(),
        getLogPath() + date.getFullYear(),
        getLogPath() + date.getFullYear() + separator + moment(date).format('MM MMMM')];

    function done(){
        if (++count == arr.length)
            cbx();
    }

    arr.forEach(function (dir){
        fs.mkdir(dir, done)
    });
}
function setupLogPathSync(date){
    var arr = [
        getLogPath(),
        getLogPath() + date.getFullYear(),
        getLogPath() + date.getFullYear() + separator + moment(date).format('MM MMMM')];

    arr.forEach(function (dir) {
        fs.existsSync(dir) || fs.mkdirSync(dir);
    });
}
recycleLogger();

module.exports.log = logger.info;
module.exports.info = console.log = logger.info;
module.exports.error = console.error = logger.error;
module.exports.restart = recycleLogger;
