var module = angular.module('system.user',
    ['ui.bootstrap',
    'chieffancypants.loadingBar',
        'ngAnimate',
        'ngSanitize',
        'ngRoute'
    ]);

module.run(['$rootScope', '$window', '$location', function ($rootScope, $window, $location) {
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        // if ($location.$$path == '/api/tranxResponse') $window.location.path = '/pub/tranxReceipt';
        $rootScope.pageTitle = (((next || {}).$$route || {}).title) ? (' - ' + next.$$route.title) : $rootScope.pageTitle = '';
    });
    $rootScope.$on("routeChangeError", function (event, current, previous, rejection) {
        //console.log('Could not load requested route', rejection);
    });
}]);
module.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    var ensureAuth = {
        CurrentUser: function ($rootScope, $location, $http, $q, Auth) {
            var d = $q.defer();
            Auth.getUser().then(d.resolve, function () {
                $location.path('/');
                d.reject();
            });
            return d.promise;
        }
    };
    var onlyPublic = {
        CurrentUser: function ($rootScope, $location, $http, $q, Auth) {
            var d = $q.defer();
            Auth.getUser().then(function () {
                $location.path('/membership');
                d.reject();
            }, d.resolve);
            return d.promise;
        }
    };
    var onlyReceipt = {
        receiptModel: function ($rootScope, $location, $http, $q, $window) {
            var d = $q.defer();
            var trxId = $window.sessionStorage.getItem('tranxId');
            $http.get('/api/tranx/' + trxId)
                .success(function (resp) {
                    $location.path('/pub/tranxReceipt');
                    d.resolve(resp);
                })
                .error(function (err) {
                    d.reject(err.message);
                });
            return d.promise;
        }
    };
    $routeProvider
        .when('/', {
            templateUrl: 'pages/new-home.html',
            title: 'Book Sale',
            controller: "bookStoreCtrl",
            resolve: onlyPublic
        })
        .when('/tne', {
            templateUrl: 'pages/tne.html',
            title: 'Training & Events',
            controller: "tAndECtrl",
            resolve: onlyPublic
        })
        .when('/membership', {
            templateUrl: 'pages/membership.html',
            title: 'Membership',
            controller: "PubMembership",
            resolve: ensureAuth
        })
        .when('/bookStore', {
            templateUrl: 'pages/book/bookstore.html',
            title: 'Book Sale',
            controller: "PubBookStoreCtrl",
            resolve: onlyPublic
        })
        .when('/pub/tranxReceipt', {
            templateUrl: 'pages/payment/show-details.html',
            title: 'PaymentReceipt',
            controller: "PaymentCtrl",
            resolve: onlyReceipt
        })
        .when('/pub/tranxError', {
            templateUrl: 'pages/payment/tranxError.html',
            title: 'PaymentResponseError',
            controller: "PaymentErrorCtrl"
        })
    ;
    $routeProvider.otherwise({redirectTo: '/'});
    $locationProvider.html5Mode(true).hashPrefix('!');
}]);
module.factory('Auth', ['$rootScope', '$http', '$q', function (r, h, q) {
    return {
        getUser: function () {
            var d = q.defer();
            if (r.user && r.member)
                d.resolve();
            else if (r.loaded) d.reject();
            else h.get('/account')
                    .then(function (resp) {
                        if ((resp.status === '200' || resp.status === 200) && resp.data && resp.data.username) {
                            r.member = resp.data.member;
                            r.user = {
                                username: resp.data.username,
                                fullName: resp.data.fullName,
                                info: resp.data.info
                            };
                            r.loaded = true;
                            d.resolve()
                        } else d.reject();
                    }, d.reject);
            return d.promise;
        }
    };
}]);
module.factory('Messages', ['$timeout', function ($timeout) {
    var s = {};
    s.messages = [];
    s.modalMessages = [];
    s.showMessage = function (msg, level) {
        s.messages.push({text: msg, type: level});
        $timeout(function () {
            s.closeMessage(0);
        }, 15000);
    };
    s.showModalMessage = function (msg, level) {
        s.modalMessages.push({text: msg, type: level});
        $timeout(function () {
            s.closeModalMessage(0);
        }, 15000);
    };
    s.closeMessage = function (index) {
        s.messages.splice(index, 1);
    };
    s.closeModalMessage = function (index) {
        s.modalMessages.splice(index, 1);
    };
    s.processResult = function (result, pop) {
        s.messages.length = 0;
        if (result.Success || result.success) {
            if (isEmpty(result.Messages)) s.showMessage('Operation succeeded', 'success');
        }
        else if (isEmpty(result.Messages))
            s.showMessage('Operation failed', 'warning');
        angular.forEach(result.Messages, function (value, key) {
            var v = 'muted';
            switch (value) {
                case 0:
                    v = 'muted';
                    break;
                case 1:
                    v = 'primary';
                    break;
                case 2:
                    v = 'success';
                    break;
                case 3:
                    v = 'info';
                    break;
                case 4:
                    v = 'warning';
                    break;
                case 5:
                    v = 'danger';
                    break;
            }
            s.showMessage(key, v);
        });
    };
    return s;
}]);
module.filter('encode', [function () {
    return function (input) {
        if (input) {
            return input.replace(' ', '');
        }
    };
}]);
module.filter('capitalize', [function () {
    return function (input) {
        if (input) {
            input = input.replace(/\w+/g, function (v) {
                return ( v.slice(0, 1).toUpperCase() + v.slice(1) );
            });
        }
        // console.log('system: Input Filter:', input);
        return input;
    };
}]);

module.controller('PaymentErrorCtrl', function ($scope, $location, $http, $window) {
    var trxId = $window.sessionStorage.getItem('tranxId');
    $scope.loading = true;
    trxId && $http.get('/api/tranxError/' + trxId)
        .success(function (r) {
            $scope.details = r.tranxError;
            $scope.displayError = true;
            $scope.displayEmpty = true;
            $scope.loading = false;
        })
        .error(function (err) {
            $scope.loading = false;
            Messages.showMessage('Could not fetch your transaction details. Contact SCGN admin', 'warning');
        })
});
module.controller('PaymentCtrl', function ($scope, Messages, $location, $http, $window, receiptModel) {
    $scope.displayError = false;
    var trxId = $window.sessionStorage.getItem('tranxId');
    // /pub/tranxReceipt/trxId
    // var orderId = $location.path().split('/').pop();
    if (!trxId) {
        $scope.displayEmpty = true;
        console.log('No tranxId in sessionStorage');
    }
    else {
        $scope.loading = true;
        $http.get('/api/tranx/' + trxId)
            .success(function (r) {
                Messages.showMessage('Displaying your transaction receipt', 'success');
                if (r.transaction) {
                    $scope.displayError = false;
                    var t = r.transaction.type || null;
                    r.transaction.type = t.slice(0, 1).toUpperCase() + t.slice(1);
                    $scope.receipt = r.transaction;
                } else {
                    $scope.receipt = [];
                    $scope.displayEmpty = true;
                }
                $scope.loading = false;
            })
            .error(function (err) {
                $scope.loading = false;
                //console.log('Error!', err.message);
                Messages.showMessage('Could not fetch your transaction details. Contact SCGN admin', 'warning');
            })
    }
});
module.controller('msgCtrl',
    ['$scope', 'Messages', function ($scope, Messages) {
        $scope.alerts = Messages.messages;
        $scope.closeAlert = Messages.closeMessage;
        $scope.modalAlerts = Messages.modalMessages;
        $scope.closeModalAlert = Messages.closeModalMessage;
    }]).controller('stateCtrl', ['$scope', '$http', function ($scope, $http) {
    $http.get('/state', {ignoreLoadingBar: true}).success(function (r) {
        $scope.slogan = r.msg;
    });
}]);

module.controller('loginCtrl',
    ['$scope', '$http', 'Messages', '$window', '$modalInstance', '$location', function ($scope, $http, Messages, $window, $modalInstance, $location) {
        $scope.user = null;
        $scope.backText = 'Close';
        $scope.back = function () {
            $modalInstance.dismiss('Close');
        };
        $scope.login = function () {
            $scope.signingIn = true;
            $http.post('/login', $scope.user).success(function (result) {
                $scope.signingIn = false;
                if (result.success) {
                    $window.location.href = '/';
                }
                else {
                    if (result.message) Messages.showMessage(result.message, 'danger');
                    if (result.errors.length > 0) {
                        result.errors.forEach(function (item) {
                            Messages.showMessage(item, 'danger');
                        });
                    }
                    if ('Recaptcha' in window) Recaptcha.reload();
                }
            }).error(function (data, status) {
                $scope.signingIn = false;
                alert('Error: ' + status);
            });
        };
        $scope.login21 = function () {
            if ('Recaptcha' in window && 'recaptcha_response_field' in window && recaptcha_response_field.value) {
                $scope.signingIn = true;
                $scope.companyName = "The Society For Corporate Governance Nigeria";
                $scope.user['recaptcha'] = recaptcha_response_field.value;
                $scope.user['quest'] = recaptcha_challenge_field.value;
                $http.post('/login', $scope.user).success(function (result) {
                    $scope.signingIn = false;
                    if (result.success) $window.location.href = $window.location.href;
                    else {
                        if (result.message) Messages.showMessage(result.message, 'danger');
                        if (result.errors.length > 0) {
                            result.errors.forEach(function (item) {
                                Messages.showMessage(item, 'danger');
                            });
                        }
                        if ('Recaptcha' in window) Recaptcha.reload();
                    }
                }).error(function (data, status) {
                    $scope.signingIn = false;
                    alert('Error: ' + status);
                });
            }
            else if ('Recaptcha' in window && Recaptcha.get_response() && Recaptcha.get_challenge()) {
                $scope.signingIn = true;
                $scope.user['recaptcha'] = Recaptcha.get_response();
                $scope.user['quest'] = Recaptcha.get_challenge();
                $http.post('/login', $scope.user).success(function (result) {
                    $scope.signingIn = false;
                    if (result.success) $window.location.href = $window.location.href;
                    else {
                        if (result.message) Messages.showMessage(result.message, 'danger');
                        if (result.errors.length > 0) {
                            result.errors.forEach(function (item) {
                                Messages.showMessage(item, 'danger');
                            });
                        }
                        if ('Recaptcha' in window) Recaptcha.reload();
                    }
                }).error(function (data, status) {
                    $scope.signingIn = false;
                    Messages.showMessage('Error: ' + ( data || status || 'could not connect to server.'));
                });
            }
            else if ('Recaptcha' in window) {
                Messages.showMessage('Please response to the captcha', 'danger');
            }
            else {
                Messages.showMessage('Please wait while recaptcha loads', 'danger');
            }
        };
    }]);

module.controller('bookStoreCtrl', function ($scope, $http, $q, $modal, Messages, $location) {
    $scope.newBook = {};
    $scope.books = [];
    var getBooks = function () {
        $http.get('/api/getPubBooks').then(function (resp) {
            $scope.books = resp.data.books;
            /* console.log('Books: ', resp.data.books); */
        });
    };
    getBooks();
    $scope.createBook = function (type, book) {
        $modal.open(
            {
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'pages/book-new.html',
                controller: function ($scope, Messages, book, $http, item, employee, $modalInstance, $filter) {
                    if (item == 'new') $scope.newBook = {employeeId: employee.employeeId};
                    if (item == 'edit') $scope.newBook = book;
                    $scope.categories = ['Texts', 'Journals'];
                    var fd = new FormData();
                    $scope.uploadFile = function (files) {
                        //Take the first selected file
                        fd.append("bookCover", files[0]);
                    };
                    $scope.backText = 'Close';
                    $scope.saveBook = function () {
                        $scope.savingNewBook = true;
                        if ($scope.newBookForm.$valid) {
                            for (var p in $scope.newBook) fd.append(p, $scope.newBook[p]);
                            // console.log('Form Data Submited: ')
                            //fd.forEach((x,y)=>console.log(x,y));
                            var headerList = {'Content-Type': undefined};
                            if (type == 'new') {
                                $http.post(
                                    '/api/book'
                                    , fd
                                    , {headers: headerList}
                                )
                                    .success(function (result, status) {
                                        if (result.message) Messages.showMessage(result.message, 'success');
                                        $scope.savingNewBook = false;
                                        $modalInstance.close({});
                                    })
                                    .error(function (err, status) {
                                        if (angular.isString(err.message)) Messages.showMessage(err.message, 'danger');
                                        $scope.savingNewBook = false;
                                        return;
                                    });

                            } else {
                                $http.put(
                                    '/api/book'
                                    , fd
                                    , {headers: headerList}
                                )
                                    .success(function (result, status) {
                                        if (result.message) Messages.showMessage(result.message, 'success');
                                        $scope.savingNewBook = false;
                                        $modalInstance.close({});
                                    })
                                    .error(function (err, status) {
                                        if (angular.isString(err.message)) Messages.showMessage(err.message, 'danger');
                                        $scope.savingNewBook = false;
                                        return;
                                    });
                            }
                        }
                    };
                    // $scope.changeEmp = function (emp) {
                    //     if ($scope.newBook.assigned.indexOf(emp) > -1)
                    //         $scope.newBook.assigned.splice($scope.newBook.assigned.indexOf(emp), 1);
                    //     else $scope.newBook.assigned.push(emp);
                    // };
                    $scope.back = function () {
                        $modalInstance.dismiss('Close');
                    };
                    // $scope.$watch('newBook.type', function (n) {
                    //     if (pass == 0) {
                    //         switch (String((n || {}).shared)) {
                    //             case 'false':
                    //                 angular.forEach($scope.refs.emps, function (g) {
                    //                     if (!angular.isArray($scope.newBook.assigned)) return;
                    //                     if ($scope.newBook.assigned.indexOf(g._id) == 0) $scope.newBook.assigned = g;
                    //                 });
                    //                 break;
                    //             case 'true':
                    //                 break;
                    //             default:
                    //                 $scope.newBook.assigned = '';
                    //         }
                    //     } else if ($scope.newBook.type.shared) $scope.newBook.assigned = [];
                    //     else $scope.newBook.assigned = '';
                    //     ++pass;
                    // })
                },
                resolve: {
                    item: function () {
                        return type == 'new' ? {} : type || {};
                    },
                    book: function () {
                        return book;
                    },
                    employee: function () {
                        return globals.getEmployee();
                    }
                }
            }).result.then(function () {
            console.log('Refresh Books');
            getBooks();
        });
    };
    $scope.viewBook = function (book) {
        // console.log('View Book', book.title);
        $modal.open(
            {
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'pages/book-view.html',
                controller: function ($scope, $modalInstance, $filter, book) {
                    $scope.book = book;
                    $scope.backText = 'Close';
                    $scope.back = function () {
                        $modalInstance.dismiss('Close');
                    };
                },
                resolve: {
                    book: function () {
                        return book;
                    }
                }
            });
    }
    $scope.editBook = function (book) {
        console.log('Edit Book', book.title);
    }

});

module.controller('newHomePageCtrl',
    ['$scope', '$http', 'Messages', '$window', '$modal', '$location', '$timeout', function ($scope, $http, Messages, $window, $modal, $location, $timeout) {
        $scope.user = null;
        $scope.companyName = "Society For Corporate Governance Nigeria";
        $scope.doLogout = function () {
            $http.get('/logout').then(function () {
                $window.location.href = '/';
            });
        };
        $scope.gotoGalery = function () {
            $location.path('/bookStore');
        };
        $scope.gotoTnE = function () {
            $location.path('/tne');
        };
        $scope.displayLoginForm = function () {
            $modal.open(
                {
                    backdrop: 'static',
                    keyboard: false,
                    templateUrl: 'pages/scgn-login.html',
                    controller: 'loginCtrl',
                    resolve: {}
                }).result.then(function () {
            });
        };
        $scope.membershipForm = function () {
            $modal.open(
                {
                    backdrop: 'static',
                    keyboard: false,
                    templateUrl: 'pages/scgn-membership.html',
                    controller: 'loginCtrl',
                    resolve: {}
                }).result.then(function () {
            });
        };
        $scope.donationForm = function () {
            $modal.open(
                {
                    backdrop: 'static',
                    keyboard: false,
                    templateUrl: 'pages/donation/online.html',
                    controller: function ($scope, $modalInstance) {
                        $scope.donation = {type: 'donation'};
                        $scope.donation.mode = 'Online';
                        $scope.scgnRemarks = ['Annual Corporate Governance Conference', 'Executive Breakfast Meeting', 'Research and Publication', 'Others'];
                        $scope.backText = 'Close';
                        $scope.back = function () {
                            $modalInstance.dismiss('Close');
                        };
                        $scope.makeOnlineDonation = function () {
                            //console.log('Make Donation', $scope.donation);
                            $http.post('/api/tranx', $scope.donation)
                                .success(function (resp) {
                                    $modalInstance.close(resp.trxUrl);
                                })
                                .error(function (err) {
                                    if (err) $modalInstance.dismiss(err.message);
                                });
                        }
                    },
                    resolve: {}
                }).result.then(function (t) {
                    Messages.showMessage('Transaction being processed', 'success');
                    var tranxId = t.split('/').pop();
                    $window.sessionStorage.setItem('tranxId', tranxId);
                    $window.location.href = t;
                },
                function (err) {
                    if (err && err.message) Messages.showMessage(err.message, 'danger');
                });
        };

        $scope.RegisterNewMemberForm = function () {
            $modal.open(
                {
                    backdrop: 'static',
                    keyboard: false,
                    templateUrl: 'pages/membership/new-member.html',
                    controller: function ($scope, $modalInstance) {
                        $scope.registerNewMember = {
                            userName : '',
                            password : '',
                            email    : '',
                            website  : '',
                            firstName: '',
                            lastName : ''
                        };

                        $scope.backText = 'Close';
                        $scope.back = function () {
                            $modalInstance.dismiss('Close');
                        };
                        $scope.saveNewRegistration = function () {
                            //console.log('Make Donation', $scope.donation);
                            $http.post('/api/newMember/register', $scope.registerNewMember)
                                .success(function (resp) {
                                    Messages.showModalMessage('Your data has been saved', 'success');
                                    $timeout(function(){
                                        $modalInstance.close(resp.message);
                                    },3000);
                                })
                                .error(function (err) {
                                    Messages.showModalMessage(err.message, 'danger');
                                    $timeout(function(){
                                        //$modalInstance.close(resp.message);
                                        
                                        $modalInstance.dismiss(err.message);
                                    },3000);
                                });
                        }
                    },
                    resolve: {}
                }).result.then(function (t) {
                    Messages.showMessage('Your data has been saved', 'success');
                    // var tranxId = t.split('/').pop();
                    // $window.sessionStorage.setItem('tranxId', tranxId);
                    // $window.location.href = t;
                },
                function (err) {
                    if (err && err.message) Messages.showMessage(err.message, 'danger');
                });
        };


    }]);

module.controller('PubBookStoreCtrl', function ($scope, $http, $q, $modal, Messages, $window, $location) {
    $scope.newBook = {};
    $scope.books = [];
    $scope.cart = {
        items: [],
        count: 0
    };
    var getBooks = function () {
        $http.get('/api/getPubBooks').then(function (resp) {
            $scope.books = resp.data.books;
            /* console.log('Books: ', resp.data.books); */
        });
    };
    getBooks();
    $scope.addToCart = function (book) {
        $modal.open(
            {
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'pages/book/addToCart.html',
                controller: function ($scope, $modalInstance, book, cart) {
                    $scope.backText = 'Close';
                    $scope.back = function () {
                        $modalInstance.dismiss('Close');
                    };
                    $scope.book = book;
                    $scope.item = null;
                    var emptyCart = {
                        id: null,
                        title: null,
                        price: null,
                        count: 0
                    };
                    var index = -1;
                    cart.forEach(function (v, i) {
                        if (v.id == book.id) {
                            $scope.item = v;
                            index = i;
                        }
                    });
                    $scope.item = ($scope.item) ? $scope.item : emptyCart;
                    // console.log('Type: ', Array.isArray(cart));
                    if (index > -1) {
                        // if(cart.length == 1) cart.pop();
                        // else cart.splice(index,1);
                        cart.splice(index, 1);
                    }
                    $scope.add = function (book, count) {
                        $scope.updatingCart = true;
                        $scope.item.id = book.id;
                        $scope.item.title = book.title;
                        $scope.item.price = book.price;
                        $scope.item.count = count;
                        cart.push($scope.item);
                        $modalInstance.close(cart)
                    };
                },
                resolve: {
                    book: function () {
                        return book;
                    },
                    cart: function () {
                        return $scope.cart.items;
                    }
                }
            })
            .result.then(function (cart) {
            $scope.cart.items = cart;
            var getCount = function () {
                var count = 0;
                cart.forEach(function (v) {
                    count += v.count;
                });
                return count;
            };
            $scope.cart.count = getCount();
            // console.log('Current cart items: ', $scope.cart.count)
        });
    };
    $scope.viewBook = function (book) {
        // console.log('View Book', book.title);
        $modal.open(
            {
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'pages/book/book-view.html',
                controller: function ($scope, $modalInstance, $filter, book) {
                    $scope.book = book;
                    $scope.backText = 'Close';
                    $scope.back = function () {
                        $modalInstance.dismiss('Close');
                    };
                },
                resolve: {
                    book: function () {
                        return book;
                    }
                }
            });
    };
    $scope.doCheckout = function (cart) {
        if (cart.count > 0) {
            // console.log('Cart Count: ', cart.count);
            $modal.open(
                {
                    backdrop: 'static',
                    keyboard: false,
                    templateUrl: 'pages/book/checkout-view.html',
                    controller: function ($scope, $modalInstance, cart, $http) {
                        $scope.cart = cart.items;
                        $scope.t = {};
                        var totalCost = function (cartList) {
                            var c = 0;
                            cartList.forEach(function (v) {
                                c += parseInt(v.count) * parseInt(v.price);
                            });
                            return c;
                        };
                        $scope.totalCost = totalCost(cart.items);
                        $scope.totalCount = cart.count;
                        $scope.backText = 'Close';
                        $scope.back = function () {
                            $modalInstance.dismiss('Close');
                        };
                        $scope.makePayment = function () {
                            console.log('Amount to be paid for ', cart.count, 'items is:', totalCost(cart.items));
                            var trx = {
                                items: cart.items,
                                totalCost: totalCost(cart.items),
                                type: 'book',
                                email: $scope.t.email,
                                phone: $scope.t.phone
                            };
                            $http.post('/api/tranx', trx)
                                .success(function (resp) {
                                    $modalInstance.close(resp.trxUrl);
                                })
                                .error(function (err) {
                                    if (err) $modalInstance.dismiss(err.message);
                                });
                        }
                    },
                    resolve: {
                        cart: function () {
                            return cart;
                        }
                    }
                })
                .result
                .then(
                    function (t) {
                        Messages.showMessage('Transaction being processed', 'success');
                        var tranxId = t.split('/').pop();
                        $window.sessionStorage.setItem('tranxId', tranxId);
                        $window.location.href = t;
                    },
                    function (err) {
                        if (err && err.message) Messages.showMessage(err.message, 'danger');
                    });
        } else {
            Messages.showMessage('Cart is empty! Do place an order', 'info')
        }
    };
});
/*
 module.controller('membershipCtrl', function($scope){
 $scope.doLogout = function () {
 $http.get('/logout').then(function () {
 $window.location.href = '/';
 });
 };
 });
 */
module.controller('tAndECtrl', function ($scope, $http, $modal, Messages, $window) {
    var getTnE = function () {
        $http
            .get('/api/pub/tne')
            .success(function (resp) {
                $scope.tne = resp.tne;
                // console.log('TnE fetched!', $scope.tne);
            })
            .error(function (err) {
                console.log(err && err.message);
            });
    };
    getTnE();
    $scope.displayTnEForm = function (tne) {
        if (tne.fee > 0) {
            $modal.open(
                {
                    backdrop: 'static',
                    keyboard: false,
                    templateUrl: 'pages/tne/payment-form.html',
                    controller: function ($scope, $modalInstance, tne, $http) {
                        $scope.tneItem = tne;
                        $scope.t = {};
                        $scope.backText = 'Close';
                        $scope.back = function () {
                            $modalInstance.dismiss('Close');
                        };
                        $scope.makePayment = function () {
                            var trx = {
                                id: tne._id,
                                fee: tne.fee,
                                type: 'training-event',
                                email: $scope.t.email,
                                phone: $scope.t.phone,
                                name: $scope.t.name
                            };
                            $http.post('/api/tranx', trx)
                                .success(function (resp) {
                                    $modalInstance.close(resp.trxUrl);
                                })
                                .error(function (err) {
                                    if (err) $modalInstance.dismiss(err.message);
                                });
                        }
                    },
                    resolve: {
                        tne: function () {
                            return tne;
                        }
                    }
                })
                .result
                .then(
                    function (t) {
                        Messages.showMessage('Transaction being processed', 'success');
                        var tranxId = t.split('/').pop();
                        $window.sessionStorage.setItem('tranxId', tranxId);
                        $window.location.href = t;
                    },
                    function (err) {
                        if (err && err.message) Messages.showMessage(err.message, 'danger');
                    });
        }
        else {
            var msg = 'This ' + tne.type + ' is free';
            Messages.showMessage(msg, 'info');
        }
    };

})
;
module.controller('PubMembership', function ($rootScope, $scope, $http, $q, $modal, Messages, $window, $location) {
    $scope.view = {name: 'profile'};
    $scope.thisUser = {};
    $scope.thisUser.today = new Date();
    $scope.thisUser.username = $rootScope.user.username;
    $scope.thisUser.fee = $rootScope.member.fee;
    $scope.currentMember = $rootScope.member;
    $scope.thisUser._id = $scope.currentMember._id;
    $scope.data = {};
    $scope.tranxHistory = null;
    $scope.hidePaymentView = false;


    $scope.changePassword = function () {
        $modal.open(
            {
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'pages/pub/membership/change-password.html',
                controller: function ($scope, $modalInstance, Messages) {
                    $scope.m = {};
                    $scope.savingPassword = false;
                    $scope.backText = 'Close';
                    $scope.errorData = null;
                    $scope.back = function () {
                        $modalInstance.dismiss('Close');
                    };
                    $scope.doChangePassword = function () {
                        $scope.savingPassword = true;
                        var obj = {
                            password: $scope.m.password,
                            memberId: $rootScope.member.memberId
                        };
                        // console.log('Change Password', obj);
                        var url = '/api/membership/change-password';
                        $http.post(url, obj)
                            .success(function (result) {
                                Messages.showModalMessage(result.message, 'success');
                            }).error(function (err) {
                            Messages.showModalMessage(err.message, 'danger');
                        })
                    }
                },
                resolve: {}
            }).result.then(function () {
        });
    };
    // console.log('Member', $rootScope);
    var getProfile = function () {
        $scope.view.name = 'profile';
        $scope.data = $rootScope.member;
        switch ($scope.data.category) {
            case 'SMEs':
                $scope.thisUser.fee = 150000;
                break;
            case 'Institutions':
                $scope.thisUser.fee = 200000;
                break;
            case 'Large Enterprises':
                $scope.thisUser.fee = 250000;
                break;
        }
        ;

        $http.get('/api/tranx/mbr/' + $rootScope.member._id)
            .success(function (resp) {
                $scope.tranxHistory = resp.memberPayment;
                if ($scope.tranxHistory.status == 'Approved') $scope.hidePaymentView = true;
            })
    };
    var getSummary = function () {
        $scope.view.name = 'summary';
        $scope.data = ''; //$rootScope.member;
    };
    var getSubscription = function () {
        $scope.view.name = 'subscription';
        $scope.loading = true;
        /*
         query to get the user's subscription history
         then update the relevant models
         */

        $scope.data = {};
        $scope.data.status = 'uptodate';
    };
    $scope.getView = function (viewName) {
        switch (viewName) {
            case 'profile':
                getProfile();
                break;
            case 'summary':
                getSummary();
                break;
            case 'subscription':
                getSubscription();
                break;
        }
    };
    $scope.getView('profile');
    $scope.showSubscriptionForm = function () {
        $modal.open(
            {
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'pages/pub/membership/pay-subscription.html',
                controller: function ($scope, $modalInstance, Messages, member) {
                    $scope.m = member;
                    $scope.sendingSubscription = false;
                    $scope.backText = 'Close';
                    $scope.errorData = null;
                    $scope.back = function () {
                        $modalInstance.dismiss('Close');
                    };
                    $scope.paySubscription = function () {
                        $scope.sendingSubscription = true;
                        var trx = {};
                        console.log('Make payment for user with id= ', member._id);
                        trx._id = member._id;
                        trx.type = 'subscription';
                        $http.post('/api/tranx', trx)
                            .success(function (resp) {
                                $scope.sendingSubscription = false;
                                $modalInstance.close(resp.trxUrl);
                            })
                            .error(function (err) {
                                $scope.sendingSubscription = false;
                                if (err) $modalInstance.dismiss(err.message);
                            });
                        // console.log('Change Password', obj);
                        // var url = '/api/membership/change-password';
                        // $http.post(url, obj)
                        //     .success(function (result) {
                        //         Messages.showModalMessage(result.message, 'success');
                        //     }).error(function (err) {
                        //     Messages.showModalMessage(err.message, 'danger');
                        // })
                    }
                },
                resolve: {
                    member: function () {
                        return $scope.thisUser;
                    }
                }
            }).result.then(
            function (t) {
                var tranxId = t.split('/').pop();
                $window.sessionStorage.setItem('tranxId', tranxId);
                // $window.localStorage.setItem('tranxId', tranxId);
                Messages.showMessage('Transaction being processed', 'success');
                $window.location.href = t;
                // sessionStorage.setItem('tranxId', localStorage.getItem('tranxId'));
                // console.log($window.localStorage.getItem('tranxId'));
                // sessionStorage.setItem('tranxId', tranxId)
                // localStorage.removeItem('tranxId');
            },
            function (err) {
                if (err && err.message) Messages.showMessage(err.message, 'danger');
            });
    };
});