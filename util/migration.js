/**
 * Created by toluogunremi on 3/19/15.
 */
var async = require('async');
module.exports = function (app) {


    var employees = app.config.employees;

    var migrated = [], errors = [], duplicates = [];
    var getUsername = function (model, whc) {
        return whc ?
            model.firstName.substring(0, 1).toLowerCase() + model.lastName.toLowerCase() :
            model.lastName.substring(0, 1).toLowerCase() + model.firstName.toLowerCase()
            ;
    };

    function createDepartments() {
        function createSingle(name) {
            app.db.models.Organisation.findOne({name: name}, function (err, dept) {
                if (err) {
                    console.log(err);
                }
                else if (!dept) {
                    app.db.models.Organisation.create({name: name, orgType: 'Department'},
                        function (err, dept) {
                            if (err) {
                                console.log(err);
                            }//
                            else {
                                console.log('department created');
                            }
                        });
                }
            });
        }

        var depts = [];
        employees.forEach(function (item) {
            if (depts.indexOf(item.department) == -1) {
                depts.push(item.department);
                createSingle(item.department);
            }
        });
        if (app.db.models.Employee.find(function (err, emps) {
                if (err) console.log('data base error: ', err);
                if (emps.length == 0) createEmployees();
            }));
    };
    function createEmployees() {
        employees.forEach(function (item) {
            var user = {}, employee = item;

            function checkDuplicates() {
                function checkUsername() {
                    app.db.models.User.findOne({username: getUsername(employee, true)}, function (err, user) {
                        if (err) {
                            //handle error
                        }
                        if (user) {
                            app.db.models.User.findOne({username: getUsername(employee, false)}, function (err, user) {
                                if (err) {
                                    errors.push(item);
                                }
                                if (user) {
                                    duplicates.push(item);
                                } else {
                                    employee.employeeId = getUsername(item, false);
                                    checkEmail();
                                }
                            });
                        }
                        else {
                            employee.employeeId = getUsername(item, true);
                            checkEmail();
                        }
                    });
                };
                function checkEmail() {
                    employee.wEmail = employee.employeeId.toLowerCase() + '@scgn.com';
                    app.db.models.User.findOne({email: employee.wEmail}, function (err, user) {
                        if (err) {
                            errors.push(item);
                        }
                        if (user) {
                            duplicates.push(item);
                        }
                        else createUser();
                    });
                };
                checkUsername();
            }

            function createUser() {
                function getHashedPassword() {
                    app.db.models.User.encryptPassword(employee.firstName.toLowerCase(), function (err, hash) {
                        if (err) { /*  handle error */
                        }
                        else {
                            user.password = hash;
                            user.isActive = 'yes';
                            user.email = employee.wEmail;
                            user.username = employee.employeeId;
                            passwordHashed();
                        }
                    });
                };
                function passwordHashed() {
                    app.db.models.User.create(user, function (err, us) {
                        if (err) { /*  handle error */
                        }
                        else {
                            user = us;
                            checkDepartment();
                        }
                    });
                };
                getHashedPassword();
            }

            function checkDepartment() {
                app.db.models.Organisation.findOne({name: employee.department}, function (err, dept) {
                    if (err) {
                        console.log(err);
                    }//
                    else if (dept) {
                        employee.department = dept.id;
                        createEmployee();
                    }
                    else {
                        app.db.models.Organisation.create({name: employee.department, orgType: 'Department'},
                            function (err, dept) {
                                if (err) {
                                    console.log(err);
                                }//
                                else {
                                    employee.department = dept.id;
                                    createEmployee();
                                }
                            });
                    }
                });
            }

            function createEmployee() {
                app.db.models.Employee.create(employee, function (err, emp) {
                    if (err) {
                        console.log(err);
                    }//
                    else {
                        employee = emp;
                        employeeCreated();
                    }
                });
            }

            function employeeCreated() {
                if (employee) {
                    user.employee = employee.id;
                    user.save(function () {
                        migrated.push({item: item, emp: employee, user: user});
                    });
                }
                else {
                    migrated.push({item: item, emp: employee, user: user});
                }
            }

            checkDuplicates();
        });
    };
    app.db.models.Employee.count(function (e, c) {
        if (c == 0) createDepartments();
    });

    function createDefaultLeaveEntitlements(c) {
        console.log(c + ' entitlements found');
        var defaultLeaveEntitlements = app.config.leaveEntitlementSettings;
        async.each(defaultLeaveEntitlements,
            function (item, cb) {
                var m = new app.db.models.LeaveEntitlement();
                m = Object.assign(m, item);
                m.save(item, function (err) {
                    cb(err);
                });
            }, function (err) {
                if (err) console.log('Failed to populate leave entitlements model');
                else {
                    console.log('Done populating the leave entitlements model ('+ defaultLeaveEntitlements.length + ')')
                }
            });
    };
    app.db.models.LeaveEntitlement.count(function (e, c) {
        if (c == 0) createDefaultLeaveEntitlements(c);
        else console.log(c, 'leave entitlements found');
    });
};
