﻿module.exports = function (app, mongoose) {
    /*
     The notification system should fetch all docs whose viewed field is false
     for the just logged in user.
     For a general notification, the employeeId is null and views is via the user's viewers right
     The duration for a notification
     - for employeeId it is immediately after he logs in
     - for viewers it is a day
     - for time based events it should be the end datetime of the event
     */
    var scgnNotificationSchema = new mongoose.Schema({
        createdOn: { type: Date, default: Date.now },
        employeeId: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'},
        employeeName: { type: String},
        message: { type: String, required: true},
        nType: { type: String, enum: ['request', 'response', 'others'], required: true},
        target: { type: String, enum: ['all', 'employee', 'supervisor', 'manager'], required: true},
        viewed: { type: Boolean, default: false},
        viewers: [{type: mongoose.Schema.Types.ObjectId, ref: 'Employee'}],
        ttl: { type: Number, default: 1},
        endDate: { type: Date },
        module: { type: String, required: true }
    });

    scgnNotificationSchema.pre('save', function(next){
        var ttl = this.ttl;
        this.endDate = require('moment')().add(ttl, 'days').toDate();
        next();
    });
    app.db.model('SCGNNotification', scgnNotificationSchema);



};