﻿
exports.getNotifications = function (req, res){
    var body = req.body;
    var employeeId = body.employeeId || null;
    req.app.utility.notify.emit('fetchNotification', employeeId, req.user
        , function(err, r){// r.eNotification && r.vNotification
            if(err) return res.status(500).json({ message: 'Error fetching notifications'});
            res.json(
                { message: 'Notifications fetched'
                , data: {
                    empNotification: r.eNotification,
                    viewersNotification: r.vNotification
                 }
                }
            );
        })
};
