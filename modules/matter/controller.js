﻿const teamSize = 3;

exports.saveMatter = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    var matter = req.body;
    var p = [];
    var async = require('async');

    workflow.on('createMatter', function (m) {
        req.app.db.models.Matter.getNextSerial(function (e, code) {
            if (e) {
                workflow.outcome.errors.push('An error occurred while generating serial no.');
                return workflow.emit('response');
            }
            matter.brNo = code;
            matter.members = m;
            matter.by = req.user.employee;
            new req.app.db.models.Matter(matter).save(function (e, p) {
                if (e) {
                    workflow.outcome.errors.push('An error occurred while saving the matter record.');
                    workflow.emit('response');
                } else {
                    req.app.utility.notify.emit('matterInvitation', p);
                    workflow.outcome.success = true;
                    workflow.outcome.message = 'Successfully created the matter';
                    workflow.emit('response');
                }
            });
        });
    });
    workflow.on("updateMatter", function (members) {
        if(!req.user.employee.isAdmin && !matter.nextHearing && !matter.verdict) {
            workflow.outcome.errors.push('Either Next Hearing or Verdict is required.');
            return workflow.emit('response');
        }
        matter.members = members;
        req.app.db.models.Matter.findById(matter._id)
            .exec(function (e, p) {
                if (e || !p) {
                    workflow.outcome.errors.push(e ? 'An error occurred retrieving the record.' : 'The requested record was not found.');
                    return workflow.emit('response');
                }
                var history =
                {
                    date: new Date(),
                    by: req.user.employee.fullName()
                };
                if (matter.lastHearingDate && matter.nextHearing) {
                    history.type = 'nextHearing';
                    history.value = matter.lastHearingDate;
                    p.verdict && (history.verdict = p.verdict);
                }
                if (!req.user.employee.isAdmin) {
                    delete matter.brNo;
                    delete matter.title;
                    delete matter.client;
                    delete matter.practiceArea;
                    delete matter.lead;
                    delete matter.members;
                }
                req.app.scgn.utility.populate(p, matter);

                p.save(function (e) {
                    if (e) {
                        workflow.outcome.errors.push('An error occurred while updating the matter record.');
                        workflow.emit('response');
                    } else {
                        workflow.outcome.success = true;
                        workflow.outcome.message = 'Successfully updated the matter record';
                        workflow.emit('response');
                    }
                })
            });
    });
    async.parallel([
        function (cb) {
            [{prop: 'title', text: 'title'}, {prop: 'members', text: 'members'}, {
                prop: 'lead',
                text: 'Matter Lead'
            }].forEach(function (i) {
                if (!matter[i.prop]) p.push(req.app.scgn.utility.capitalize(i.text));
            });
            if (p.length > 0) cb('The following field' + (p.length == 1 ? ' is' : 's are') + ' required "' + p + '".');
            else if (Array.isArray(matter.members) && matter.members.length > 0 && matter.members.length < 4) {
                cb();
            } else cb('Selected members must be between one and three.');
        }
    ], function (e) {
        if (e) {
            workflow.outcome.errors.push(e);
            workflow.emit('response');
        } else {
            var members = matter.members.map(function (p) {
                return p._id || p;
            });
            matter._id ? workflow.emit('updateMatter', members) : workflow.emit('createMatter', members);
        }
    });
};
exports.closeMatter = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    var date = req.body.date, id = req.params.id;
    var async = require('async');
    async.parallel([
        function (cb) {
            req.app.db.models.Matter.findById(id)
                .exec(function (e, t) {
                    if (e) return cb('Could not retrieve the selected matter data');
                    else if (!t) cb('The selected matter was not found.');
                    //else if(t.leader != req.user.employee.id) cb('You can only close a matter in which you are the team leader.');
                    else if (t && t.members.length != teamSize) cb('The selected matter is incomplete. Only completed matters can be closed.')
                    else {
                        t.closeDate = date;
                        t.closed = true;
                        workflow.matter = t;
                        cb();
                    }
                });
        }
    ], function (e) {
        if (e) {
            workflow.outcome.errors.push(e);
            workflow.emit('response');
        } else {
            workflow.matter.save(function () {
                workflow.outcome.success = true;
                workflow.outcome.message = 'Successfully marked the matter as closed';
                workflow.emit('response');
            });
        }
    });
};
exports.activeMatterReports = function (req, res) {
    var query = {closed: {"$ne": true}}, allowed = 'closed date initiator leader location members title';
    if (req.params.id == 'closed')
        query.closed = true; //return closedMatterReports(req, res);
    req.app.db.models.Matter.find(query, allowed)
        .populate('initiator leader members', 'firstName lastName designation')
        .exec(function (e, cs) {
            var us = require('underscore');
            var temp = (cs || []).map(function (p) {
                return p.initiator;
            });
            var initiators = [];
            var ids = [];
            temp.forEach(function (p) {
                if ((p || {}).id && ids.indexOf(p.id.toString()) == -1) {
                    ids.push(p.id.toString());
                    initiators.push(p.toObject());
                }
            });
            initiators.forEach(function (i) {
                if (!i) return;
                i.matters = (cs || []).filter(function (j) {
                    return j.initiator.id.toString() == i._id;
                });
                i.name = i.firstName + ' ' + i.lastName;
            });

            req.app.db.models.Employee.find({
                designation: {$in: req.app.seniorCounsels},
                location: 'Abuja',
                status: {$ne: 'Inactive'}
            }, 'firstName lastName designation', function (e, f) {
                var async = require('async');
                async.each((f || []), function (s, cb) {
                    req.app.db.models.Matter.find({closed: query.closed, members: s.id}, allowed)
                        .populate('initiator leader members', 'firstName lastName designation')
                        .exec(function (e, cs) {
                            if (s.id && ids.indexOf(s.id.toString()) == -1 && cs.length > 0) {
                                ids.push(s.id.toString());
                                var p = s.toObject();
                                p.matters = cs;
                                p.name = p.firstName + ' ' + p.lastName;
                                initiators.push(p);
                            }
                            cb();
                        });
                }, function () {
                    res.json(initiators);
                });
            });
        });
};

exports.getHearingCalendar = function (req, res) {

};

exports.getMatterList = function (req, res) {
    req.app.db.models.Matter.find()
        .populate([{
            path: 'members'
        }, {
            path: 'lead'
        }])
        .exec(function (e, m) {
            res.json(m);
        });
};
exports.getMatterListWithEmps = function (req, res) {
    var async = require('async');
    async.parallel([
        function (cb) {
            async.waterfall([
                function (cbx) {
                    req.app.db.models.Organisation
                        .findOne({name: "Counsel"})
                        .select("_id")
                        .exec(function (e, m) {
                            if (e) return cbx(e);
                            cbx(null, (m || {}).id);
                        });
                },
                function (dept) {
                    req.app.db.models.Employee
                        .find({status: {$ne: 'Inactive'}, department: dept})
                        .select("firstName middleName lastName designation")
                        .exec(function (e, m) {
                            cb(e, (m || []).map(function (t) {
                                return t.extras;
                            }))
                        });
                }
            ]);
        },
        function (cb) {
            if (!req.user || !req.user.employee || (!req.user.employee.rights.isAdmin && !req.user.employee.rights.isCounsel && !req.user.employee.rights.isIT))return cb();
            var query = req.user.employee.rights.isCounsel ?
            {'$or': [{lead: req.user.employee.id}, {members: req.user.employee.id}]} :
            {};
            req.app.db.models.Matter.find(query)
                .populate([{
                    path: 'members', select: "firstName middleName lastName designation"
                }, {
                    path: 'lead', select: "firstName middleName lastName designation"
                }])
                .exec(cb);
        }
    ], function (e, m) {
        res.json({
            matters: m[1],
            emps: m[0]
        });
    });
};
exports.getVerdictReport = function (req, res) {
    req.app.db.models.Matter.find({verdict: null})
        .populate([{
            path: 'members'
        }, {
            path: 'lead'
        }])
        .exec(function (e, m) {
            res.json(m);
        });
};

exports.counselOnBench = function (req, res) {

};
