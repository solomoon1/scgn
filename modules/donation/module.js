module.exports = function (app, mongoose) {
    require('./models')(app, mongoose);
    var controller = require('./controller.js');
    app.get('/api/donations', controller.getDonations);
    app.get('/api/all-donations', controller.getAllDonations);
    app.post('/api/donation', controller.createDonation);
    app.put('/api/donation', controller.updateDonation);

};