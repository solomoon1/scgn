module.exports = function (app, mongoose) {
    require('./models')(app, mongoose);
    var controller = require('./controller.js');
    var formidable = require('formidable');

    /*
     returns with req obj modified with
     - req.myFile
     - req.myFields
     - req.isMyFileValid
     */
    var uploaderHelper = function(uploadDir, req, res, next){
        var form = new formidable.IncomingForm();
        var fileValid = false;
        form.encoding = 'utf-8';
        form.uploadDir= uploadDir;
        form.on('fileBegin', function(name, file) {
            var fPath = file.path.split("\\");
            fPath.pop();
            fPath.push(file.name);
            file.path = fPath.join('\\');
            var fExt = file.type.split('/')[1];
            req.myFileExt = '.' + fExt;
            fileValid = 'jpeg jpg png'.split(' ').indexOf(fExt) > -1;
        });
        form.parse(req, function(err, fields, files) {
            req.myFile = files;
            req.myFields = fields;
            req.isMyFileValid = fileValid;
            next();
        });

    };
    var myImgUploader = function(req, res, next){
        uploaderHelper('./public/img/', req, res, next);
    };

    //delete an exited employee
    app.post('/employee/exited/:employeeId', controller.deleteEmployee);

    app.post('/employee', app.ensureEmpRecord, controller.createEmployee);
    app.get('/employee', app.ensureEmpRecord, controller.getActiveEmployees);
    app.get('/supervisorReports/:employeeId', controller.getSupervisorsWeeklyReports);
    app.get('/ownReports/:employeeId', controller.getOwnWeeklyReports);
    app.get('/employee/card', app.ensureEmpRecord, controller.getActiveEmployeesForCard);
    app.get('/employee/exited', app.ensureEmpRecord, controller.getExitedEmployees);
    app.put('/employee', controller.updateEmployee);

    app.put('/employee/experience', controller.editExperience);
    app.put('/employee/education', controller.editEducation);
    app.put('/employee/skill', controller.editSkill);
    app.put('/employee/dependant', controller.editDependant);
    app.post('/employee/picture', myImgUploader, controller.changePicture);
    app.post('/weeklyReport/report', controller.postWeeklyReport);
};