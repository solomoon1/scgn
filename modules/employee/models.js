module.exports = function (app, mongoose) {
    //employee
    var employeeSchema = new mongoose.Schema({
        employeeId: {type: String, required: true, unique: true, lowercase: true},
        firstName: {type: String, required: true},
        middleName: {type: String, default: ''},
        lastName: {type: String, required: true},
        dob: Date,
        gender: {type: String, default: ''},
        location: {type: String, default: ''},
        maritalStatus: {type: String, default: ''},
        street: {type: String, default: ''},
        city: {type: String, default: ''},
        state: {type: String, default: ''},
        zipCode: {type: String, default: ''},
        pEmail: {type: String, default: '', lowercase: true},
        pPhone: {type: String, default: ''},
        wEmail: {type: String, default: '', lowercase: true},
        wPhone: {type: String, default: ''},
        hired: {type: Date, required: true, default: Date.now},
        confirmed: Date,
        designation: {type: String/*, enum:app.constants.designations*/},
        department: {type: mongoose.Schema.Types.ObjectId, ref: 'Organisation', required: true},
        access: {type: String, enum: app.constants.accessLevels},
        nokName: {type: String, default: ''},
        nokRelationship: {type: String, default: ''},
        nokPhone: {type: String, default: ''},
        nokEmail: {type: String, default: '', lowercase: true},
        nokAddress: {type: String, default: ''},
        experiences: [{type: mongoose.Schema.Types.ObjectId, ref: 'Experience'}],
        educations: [{type: mongoose.Schema.Types.ObjectId, ref: 'Education'}],
        skills: [{type: mongoose.Schema.Types.ObjectId, ref: 'Skill'}],
        dependants: [{type: mongoose.Schema.Types.ObjectId, ref: 'Dependant'}],
        leaveLevel: {type: String, default: 'Level 1', enum: app.constants.levels},
        leaveStatus: {
            casual: {
                entitlement: Number, taken: {type: Number, default: 0}
            },
            annual: {entitlement: Number, taken: {type: Number, default: 0}},
            parenting: {entitlement: {type: Number, default: 0}, taken: {type: Number, default: 0}},
            sick: {
                entitlement: {type: Number, default: 0},
                taken: {type: Number, default: 0},
                occassions: {type: Number, default: 0}
            }
        },
        supervisor: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'},
        subordinates: [{type: mongoose.Schema.Types.ObjectId, ref: 'Employee'}],
        tasks: [String],
        status: {type: String, default: 'Active', required: true, enum: ['Active', 'Inactive']},
        img: { type: String, default: '/img/profile.png'}
    });
    employeeSchema.methods.fullName = function () {
        return this.firstName + ' ' + this.lastName;
    };
    employeeSchema.methods.isValidAccess = function (){
        return app.constants.access.indexOf(this.access) != -1;
    };
    employeeSchema.methods.canPlayRoleOf = function (name) {
        switch (name.toLowerCase()) {
            case 'admin':
                return this.access == 'M' || this.access == 'O';
            case 'finance':
                return this.department && this.department.name == 'Finance';
            default:
                return false;
        }
    };
    employeeSchema.methods.isPD = function () {
        //var l = 'O';
        //var result = this.access == l;
        //return result;
        return this.access == 'O' && this.department.name == 'HR';
    };
    employeeSchema.methods.isASupervisor = function () {
        return this.subordinates.length > 0;
    };
    // employeeSchema.virtual('name').get(function () {
    //    // return this.firstName + ' '+ this.lastName;
    //     return this.fullName();
    // });
    employeeSchema.virtual('extras').get(function () {
        var name = this.fullName(),
            deptName = (this.department || {}).name || '',
            dept = deptName || this.department;
        return {
            _id: this.id,
            name: this.fullName(),
            dept: dept,
            employeeId:this.employeeId,
            active: this.status == 'Active',
            desig: this.designation,
            img: this.img,
            concat: String.ConcatWithSpace(name, '(', deptName ? (dept + ' - ') : '', this.designation, ')').trim().replace(/\s+/g, ' ')
        }
    });
    employeeSchema.virtual('rights').get(function () {
        var info = {};
        if (this.access == 'O' || this.access == 'Q') info.isExec = true;
        if ((this.subordinates || []).length > 0) info.isSuper = true;
        if (!this.department) return info;
        if (this.department.name == 'Finance') info.isFinance = true;
        if (this.department.name == 'Information Technology') info.isIT = true;
        if (this.department.name == 'HR') info.isHR = true;
        if (this.department.name == 'Research & Publication' || 'Business Development & Administration') info.isRnP = true;
        if (this.department.name == 'Administration') info.isAdmin = true;
        // if (this.id == (app.pd ||{}).id) info.isPD= true;
        // if (this.department.name == 'Counsel') info.isCounsel = true;
        // if (this.department.name == 'Counsel' && app.constants.sCounsels.indexOf(this.designation) > -1) info.issCounsel = true;
        return info;
    });
    employeeSchema.virtual('delegation').get(function () {
        var delegated = {};
        if (!this.tasks) return {};
        if (this.tasks.indexOf('trn') > -1) delegated.canTrn = true;
        if (this.tasks.indexOf('fac') > -1) delegated.canFac = true;
        if (this.tasks.indexOf('emp') > -1) delegated.canEmp = true;
        if (this.tasks.indexOf('att') > -1) delegated.canAtt = true;
        if (this.tasks.indexOf('cas') > -1) delegated.canCas = true;
        if (this.tasks.indexOf('lev') > -1) delegated.canLev = true;
        if (this.tasks.indexOf('quo') > -1) delegated.canQuo = true;
        if (this.tasks.indexOf('bkMgr') > -1) delegated.canMgBk = true;
        if (this.tasks.indexOf('donatMgr') > -1) delegated.canMgDonat = true;
        if (this.tasks.indexOf('membMgr') > -1) delegated.canMgMbrship = true;
        if (this.tasks.indexOf('tne') > -1) delegated.canMgTnE = true;
        if (this.tasks.indexOf('fin') > -1) delegated.canMgFund = true;

        //this is the list of mac addresses of the system from which employees can log in from
        if (["57a0a007cd9bf6bf165d0ae0",
                "5581a126aa0de72c0980ff0c",
                "5581a126aa0de72c0980fef8",
                "5581a126aa0de72c0980ff00",
                "5581a126aa0de72c0980fef4",
                "5581a126aa0de72c0980ff16"].indexOf(this._id) >= 0) delegated.canMatter = true;
        return delegated;
    });

    // employeeSchema.virtual('concats').get(function () {
    //     if((this.department || {}).name) return String.ConcatWithSpace(this.name ,'(' ,this.department.name,' - ',this.designation ,')').trim().replace(/\s+/g, ' ');
    //     return String.ConcatWithSpace(this.name ,'(' ,this.designation ,')').trim().replace(/\s+/g, ' ');
    // });

    // employeeSchema.set('toObject',{virtuals: true});
    employeeSchema.set('toJSON', {virtuals: true});
    app.db.model('Employee', employeeSchema);
    //experience
    var experienceSchema = new mongoose.Schema({
        employer: {type: String, required: true},
        designation: String,
        startDate: {type: Date, required: true},
        endDate: {type: Date, required: true},
        employee: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'}
    });
    app.db.model('Experience', experienceSchema);
    //education
    var educationSchema = new mongoose.Schema({
        school: {type: String, required: true},
        major: {type: String, required: true},
        qualification: {type: String, enum: app.constants.qualifications},
        grade: {type: String, required: true},
        startDate: {type: Date, required: true},
        endDate: {type: Date, required: true},
        dateAwarded: Date,
        graduated: Boolean,
        employee: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'}
    });
    app.db.model('Education', educationSchema);
    //skill
    var skillSchema = new mongoose.Schema({
        name: {type: String, required: true},
        type: {type: String, default: ''},
        date: {type: Date, required: true},
        level: {type: String, required: true, enum: app.constants.skills},
        employee: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'}
    });
    app.db.model('Skill', skillSchema);
    //dependant
    var dependantSchema = new mongoose.Schema({
        firstName: {type: String, required: true},
        middleName: {type: String, default: ''},
        lastName: {type: String, required: true},
        dob: Date,
        gender: {type: String, default: ''},
        relationship: {type: String, default: ''},
        street: {type: String, default: ''},
        city: {type: String, default: ''},
        state: {type: String, default: ''},
        zipCode: {type: String, default: ''},
        policyNumber: {type: String, default: ''},
        effectiveDate: Date,
        endDate: Date,
        employee: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'}
    });
    app.db.model('Dependant', dependantSchema);
    var weeklyReportSchema = new mongoose.Schema({
        date: { type: Date, default: Date.now },
        employee: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'},
        supervisor: {type: mongoose.Schema.Types.ObjectId, ref: 'Employee'},
        report: { type: String, default: ''},
        title: { type: String, default: ''}
    });
    app.db.model('WeeklyReports', weeklyReportSchema);

};