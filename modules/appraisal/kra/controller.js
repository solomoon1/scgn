// Performance History as Chart(kra)
exports.getPerformanceHistory = function (req, res) {
    req.app.db.models.EmployeeAppraisal
        .aggregate([
                {$match: {"kra.text": {"$ne": null}}},
                {
                    $project: {
                        list: "$kra.list",
                        score: "$kra.score",
                        text: "$kra.text",
                        date: "$kra.date",
                        leads: "$kra.leads",
                        period: "$period",
                        employee: "$employee"
                    }
                },
                {
                    $group: {
                        _id: {period: "$period", id: "$employee", text: "$text", score: "$score", date: "$date"},
                        data: {$push: "$$ROOT"}
                    }
                },
                {
                    $project: {
                        period: "$_id.period",
                        _id: 0,
                        id: "$_id.id",
                        score: "$_id.score", text: "$_id.text", date: "$_id.date",
                        data: 1, score: 1, text: 1, date: 1, more: 1
                    }
                },
                {
                    $group: {
                        _id: "$id",
                        employee: {$last: "$id"},
                        periods: {$push: "$$CURRENT"}//,
                        // scoring: { text: "$$CURRENT.text", score: "$$CURRENT.score", date: "$$CURRENT.date", more: "$$CURRENT.more" }
                    }
                },
                {$sort: {_id: 1}}
            ],
            function (err, pc) {
                if (err) res.json([]);
                else {
                    require('async').parallel([function (cb) {
                        req.app.db.models.Employee.populate(pc, [{
                            path: 'employee',
                            select: 'firstName lastName designation department employeeId'
                        }], function () {
                            cb();
                        });
                    }, function (cb) {
                        req.app.db.models.AppraisalPeriod.populate(pc, {
                            path: 'periods.period',
                            select: 'year name'
                        }, function () {
                            cb();
                        });
                    }, function (cb) {
                        req.app.db.models.AppraisalPeriod.populate(pc, {
                            path: 'periods.period',
                            select: 'year name'
                        }, function () {
                            cb();
                        });
                    }], function () {
                        req.app.db.models.Organisation.populate(pc, {
                            path: 'employee.department',
                            select: 'name'
                        }, function () {
                            res.json(pc);
                        });
                    })

                }
            });
};

exports.getAllEmployeesPerformanceHistory = function (req, res) {
    var d = new Date();
    require('async').waterfall([
        function (cb) {
            req.app.db.models.AppraisalPeriod
                .find({end: {$lte: d}}, 'name year')
                .sort({date: -1})
                .limit(4)
                .exec(function (err, periods) {
                    cb(err, periods);
                });
        },
        function (periods, cb) {
            req.app.db.models.AppraisalPeriod
                .aggregate([
                    {$match: {end: {$lte: d}}},
                    {$group: {_id: null, ids: {$push: "$_id"}}},
                    {$project: {ids: "$ids"}}
                ], function (err, pp) {
                    var ids = [];
                    periods.forEach(function (i) {
                        ids.push(i.id.toString());
                    });
                    cb(err, periods, ids);
                });
        },
        function (periods, ids, cb) {
            req.app.db.models.EmployeeAppraisal
                .aggregate([
                    // {$match: {"$and": [{"kra.text": {"$ne": null},
                    //     "period": "57758c8f7ad105d61260047a"}]}},
                    // {
                    //     $match: {
                    //         "kra.text": {"$ne": null},
                    //         // "period": "560eb084814f1f622a2efddf"
                    //     }
                    // },
                    {
                        $match: {
                            "kra.text": {"$ne": null},
                            // "period": {"$in": []}
                        }
                    },
                    {
                        $project: {
                            //list: "$kra.list",
                            score: "$kra.score",
                            period: "$period",
                            employee: "$employee"
                        }
                    },
                    {
                        $group: {
                            _id: {period: "$period", employee: "$employee", score: "$score"},
                            data: {$push: "$$ROOT"}
                        }
                    },
                    {
                        $project: {
                            period: "$_id.period",
                            _id: 0,
                            id: "$_id.employee",
                            employee: "$_id.employee",
                            score: "$_id.score"
                        }
                    },
                    {
                        $group: {
                            _id: "$id",
                            employee: {$first: "$employee"},
                            data: {$push: "$$ROOT"}
                        }
                    },
                    {
                        $project: {
                            employee: 1,
                            data: 1
                        }
                    }
                ], function (e, results) {
                    // res.json({results: results, periods: periods, ids: ids});
                    // cb('Truncated');
                    console.log("Results: %d, Periods: %d", results.length, periods.length);
                    cb(e, results, periods);
                })
        },
        function (results, periods, cb) {
            console.log("Results: %d, Periods: %d", results.length, periods.length);
            req.app.db.models.Employee.populate(results, [{
                path: 'employee',
                select: 'firstName lastName designation employeeId department -_id isActive'
            }], function () {
                cb(null, results, periods);
            });
        },
        function (results, periods, cb) {
            req.app.db.models.Organisation.populate(results, [{
                path: 'employee.department',
                select: 'name -_id'
            }], function () {
                cb(null, results, periods);
            });
        },
        function (results, periods, cb) {
            console.log("Results: %d, Periods: %d", results.length, periods.length);

            var names = {};
            periods.forEach(function (i) {
                names[i.id] = {
                    name: i.name,
                    year: i.year
                };
            });
            results = results.filter(function (emp) {
                return emp.isActive;
            });
            results.forEach(function (emp, index) {
                emp.name = emp.employee.fullName();
                emp.department = emp.employee.department.name;
                emp.designation = emp.employee.designation;
                delete emp.employee;

                emp.data.forEach(function (i, ii) {

                    var name = names[i.period];
                    if (name) {
                        // var name = getPeriodName(periods, i.period);
                        emp[(name || {}).name + ' ' + (name || {}).year] = i.score;
                    }
                });
                delete emp.data;
            });
            cb(null, results);
        }
    ], function (err, result) {
        if (err) {
            console.error(err);
            res.status(500).end(err);
        } else {
            console.log('Result of aggregate ', result.length);
            res.json(result);
        }
    });
};


exports.getKraAsAdMin = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    workflow.on('getData', function () {
        req.app.db.models.KraArea
            .find()
            .populate('kras')
            .exec(function (err, pers) {
                req.app.db.models.Organisation
                    .populate(pers,
                        {
                            path: 'kras.department'//,select: 'name'
                        },
                        function () {
                            res.send(pers);
                        })
            });
    });
    workflow.emit('getData');
};

//KRA Appraisal
exports.addKraArea = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    var kraArea = req.body;
    if (!kraArea.name) {
        workflow.outcome.errors.push('The area name is required.');
        return workflow.emit('response');
    }
    else {
        req.app.db.models.KraArea.create({name: kraArea.name}, function (err, exp) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else {
                workflow.outcome.item = exp;
                workflow.outcome.success = true;
                workflow.outcome.message = 'KRA Area added';
                workflow.emit('response');
            }
        });
    }
};
exports.deleteKraArea = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    var kraAreaId = req.params.id;
    req.app.db.models.KraArea.findByIdAndRemove(kraAreaId, function (err, exp) {
        if (err) {
            return workflow.emit('exception', err);
        }
        else {
            workflow.outcome.item = exp;
            workflow.outcome.success = true;
            workflow.outcome.message = 'KRA Area deleted';
            workflow.emit('response');
        }
    });
};
exports.saveKRA = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    workflow.on('validate', function () {
        var kra = req.body;
        delete kra.chosen;
        if (kra.removed) delete kra.removed;
        ['area', 'description', 'designations', 'department']
            .forEach(function (item) {
                if (!kra[item]) workflow.outcome.errfor[item] = 'required';
            });
        workflow.kra = kra;
        if (workflow.hasErrors()) {
            workflow.outcome.errors.push('Please ensure that all required fields are entered.');
            return workflow.emit('response');
        }
        else workflow.emit('getArea');
    });
    workflow.on('getArea', function () {
        req.app.db.models.KraArea.findById(workflow.kra.area._id, function (err, exp) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (!exp) {
                workflow.outcome.errors.push('Selected KRA Area was not found.');
                return workflow.emit('response');
            }
            else {
                workflow.kraArea = exp;
                workflow.kra.area = exp.id;
                workflow.isNew = !workflow.kra._id;
                workflow.kra.department = workflow.kra.department._id;
                if (workflow.isNew) workflow.emit('create');
                else workflow.emit('update');
            }
        });
    });
    workflow.on('updateArea', function () {
        if (workflow.kraArea.kras.indexOf(workflow.kra.id) == -1) {
            workflow.kraArea.kras.push(workflow.kra.id);
            workflow.kraArea.save(function (er, area) {
                if (er) workflow.emit('sendResponse', true);
                else {
                    workflow.kraArea = area;
                    workflow.emit('sendResponse');
                }
            });
        } else workflow.emit('sendResponse');
    });
    workflow.on('sendResponse', function (er) {
        workflow.outcome.item = {area: workflow.kraArea, kra: workflow.kra};
        workflow.outcome.success = true;
        var r = workflow.isNew;
        workflow.outcome.message = r ? 'KRA Description added' : 'KRA Description updated';
        workflow.outcome.message += er ? ', could not update area' : '';
        workflow.emit('response');
    });
    workflow.on('update', function () {
        req.app.db.models.KRA.findById(workflow.kra._id, function (err, exp) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else if (!exp) {
                return workflow.emit('create');
            }
            else {
                workflow.emit('removeFromOthers', exp.id, function () {
                    req.app.scgn.utility.populate(exp, workflow.kra, ['department']);
                    exp.save(function (err, kra) {
                        if (err) {
                            return workflow.emit('exception', err);
                        }
                        else {
                            workflow.kra = kra;
                            workflow.emit('updateArea');
                        }
                    });
                });
            }
        });
    });
    workflow.on('removeFromOthers', function (kra, cb) {
        var length = 0;
        var count = 0;

        function done() {
            if (++count >= length) cb();
        }

        req.app.db.models.KraArea.find({kras: kra}, function (err, exp) {
            if (err || exp.length == 0) done();
            else {
                length = exp.length;
                exp.forEach(function (item) {
                    if (item.id != workflow.kraArea.id) {
                        var i = item.kras.indexOf(kra);
                        if (i >= 0) item.kras.splice(i, 1);
                        item.save(function () {
                            done();
                        });
                    }
                    else done();
                });
            }
        });
    });
    workflow.on('create', function () {
        req.app.db.models.KRA.create(workflow.kra, function (err, kra) {
            if (err) {
                return workflow.emit('exception', err);
            }
            else {
                workflow.kra = kra;
                workflow.emit('updateArea');
            }
        });
    });
    workflow.emit('validate');
};

exports.getKraReports = function (req, res) {
    req.app.scgn.utility.getLatestPeriod(req.app.db.models.AppraisalPeriod, function (e, per) {
        if (e) res.status(500).send(e);
        else
            req.app.db.models.EmployeeAppraisal.find(
                {period: per.period.id, "kra.text": {"$ne": null}}
                , 'period employee kra')
                .populate('employee', 'firstName lastName designation department employeeId')
                .populate('period', 'name year')
                .populate('kra.leads')
                .exec(function (err, pc) {
                    req.app.db.models.Organisation.populate(pc, {
                        path: 'employee.department',
                        select: 'name'
                    }, function () {
                        res.json(pc || []);
                    });
                });
    });
};
exports.getKraReportsForPeriod = function (req, res) {
    req.app.db.models.AppraisalPeriod.findById(req.params.id)
        .exec(function (e, per) {
        if (e) res.status(500).send(e);
        else
            req.app.db.models.EmployeeAppraisal.find(
                {period: per.id, "kra.text": {"$ne": null}}
                , 'period employee kra')
                .populate('employee', 'firstName lastName designation department employeeId')
                .populate('period', 'name year')
                .populate('kra.leads')
                .exec(function (err, pc) {
                    req.app.db.models.Organisation.populate(pc, {
                        path: 'employee.department',
                        select: 'name'
                    }, function () {
                        res.json(pc || []);
                    });
                });
    });
};

// Adding KRA Achievements
exports.getKraAppraisal = function (req, res) {
    var data = {
        isOpen: false, appraised: false, kras: [], areas: [], period: {}
    };
    var async = require('async');

    //console.log(req.user);
    function getLatestPeriod() {
        req.app.db.models.AppraisalPeriod.find(function (err, pers) {
            if (err) res.status(500).send('Could not retrieve the appraisal period status.');
            else if (pers.length == 0) res.status(500).send('no Appraisal period found.');
            else {
                var per = pers[pers.length - 1];
                var today = new Date(new Date().toDateString());
                data.isOpen = today >= per.start && today <= per.end;
                data.period = per;
                getKraAppraisals();
            }
        });
    }

    function getKraAppraisals() {
        var us = require('underscore');
        async.parallel([
            function (cbx) {
                req.app.db.models.KraAppraisal
                    .find({employee: req.user.employee.id, period: data.period.id},
                        '-details.catW -period -employee')
                    .exec(function (er, kas) {
                        if (er) cbx({code: 500, text: 'An error occurred while retrieving kra appraisal data.'});
                        else if (kas.length > 0) {
                            var item = kas[0].scoring || {};
                            data.kras = us.groupBy(kas, function (o) {
                                return o.details.area;
                            });
                            data.appraised = !!(item.score && item.by && item.date, item.text);
                            if (data.appraised) {
                                data.date = item.date;
                                data.by = item.by;
                            }
                            cbx();
                        }
                        else {
                            cbx({code: 404, text: 'No KRA Appraisal data found.'});
                        }
                    });
            },
            function (cbx) {
                req.app.db.models.Lead
                    .findOne({employee: req.user.employee, period: data.period},
                        function (e, r) {
                            if (r) data.leads = r;
                            cbx();
                        });
            },
            function (cbx) {
                req.app.db.models.EmployeeAppraisal
                    .findOne({employee: req.user.employee.id, period: data.period.id}, 'kra',
                        function (e, r) {
                            if (e || !r) cbx();
                            else {
                                data.text = r.kra.text;
                                cbx();
                            }
                        });
            }
        ], function (er) {
            if (er) res.status(er.code).send(er.text);
            else res.json(data);
        });

    }

    getLatestPeriod();
};

exports.saveLeads = function (req, res) {
    var workflow = req.app.utility.workflow(req, res),
        async = require('async');
    async.waterfall([
        function (cb) {// ensure employee department is Counsel
            if (req.user.employee.extras.dept !== 'Counsel') return cb("Only counsels are appraised on this.");
            cb();
        }, function (cb) {// Get Current Appraisal Period
            req.app.scgn.utility.getCurrentPeriod(
                req.app.db.models.AppraisalPeriod, cb);
        }, function (period, cb) { // Validate user input
            var leads = req.body, error = '';
            if (!leads.count) error = 'No of leads generated is required.';
            else if (!(leads.count === '0' || leads.count === 0) && !leads.explanation) error = 'Leads explanation/list is required.';
            if (!error) return cb(null, period);
            cb(error);
        }, function (period, cb) { // Get Counsel Leads
            req.app.db.models.Lead
                .findOne({period: period, employee: req.user.employee})
                .exec(function (er, item) {
                    if (er) return cb(er);
                    if (item) return cb(null, item);
                    cb(null, new req.app.db.models.Lead({
                        employee: req.user.employee.id,
                        period: period.id
                    }));
                });
        }, function (leads, cb) { //Update leads generated
            leads.count = req.body.count;
            if(!(leads.count === '0' || leads.count === 0)) leads.explanation = req.body.explanation;
            leads.save(cb);
        }
    ], function (msg, result) {
        if (msg)
            workflow.outcome.errors.push(msg);
        else{
            workflow.outcome.leads = result;
            workflow.outcome.message = "Successfully saved leads";
        }
        workflow.emit('response');
    });
};

exports.performAddKraAchievement = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    workflow.on('returnError', function (msg) {
        workflow.outcome.errors.push(msg);
        workflow.emit('response');
    });
    workflow.on('validate', function () {
        var appr = req.body;
        if (!appr.achievement) return workflow.emit('returnError', 'Please ensure achievement is inputted.');
        else if (!(appr.details || {}).kra) return workflow.emit('returnError', 'Invalid kra.');
        else {
            workflow.appr = appr;
            workflow.emit('getKRAAppraisal', appr);
        }
    });
    workflow.on('getKRAAppraisal', function (apprx) {
        req.app.db.models.KraAppraisal
            .findById(apprx._id)
            .exec(function (er, apr) {
                if (apr) {
                    if (apr.employee != req.user.employee.id) return workflow.emit('returnError', 'Appraisal belongs to another employee.');
                    else if (apr.scoring.score || apr.scoring.date || apr.scoring.by) return workflow.emit('returnError', 'Cannot add achievements for an already scored kra.');
                    else {
                        apr.achievement = apprx.achievement;
                        apr.date = new Date(new Date().toDateString());
                        apr.save(function (e, a) {
                            if (er) return workflow.emit('returnError', 'Error occurred while modifying achievements.');
                            else {
                                workflow.outcome.success = true;
                                workflow.outcome.message = 'Achievements successfully modified';
                                workflow.emit('response');
                            }
                        });
                    }
                } else return workflow.emit('returnError', er ? 'An error occured while retrieving appraisal' : 'KRA Appraisal was not found.');
            });
    });
    workflow.on('isAppraisalOpen', function () {
        req.app.scgn.utility.getCurrentPeriod(
            req.app.db.models.AppraisalPeriod,
            function (er, p) {
                if (er) {
                    workflow.outcome.errors.push(er);
                    return workflow.emit('response');
                }
                else {
                    workflow.period = p;
                    workflow.emit('validate');
                }
            });
    });
    workflow.emit('isAppraisalOpen');
};

exports.performKraAppraisal = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    workflow.kras = [];
    workflow.completed = [];
    workflow.sum = 0;
    workflow.unit = 0;
    var today = new Date(new Date().toDateString());
    var async = require('async');
    workflow.on('validate', function () {
        var data = req.body;
        if (!data.emp || !data.emp.code)
            workflow.outcome.errors.push('Employee data is required.');
        else if (req.user.employee.subordinates.indexOf(data.emp.code) == -1) {
            workflow.outcome.errors.push('Selected employee is not a subordinate.');
            return workflow.emit('response');
        }
        else workflow.empId = data.emp.code;
        if (!data.kras)
            workflow.outcome.errors.push('KRA data is required');
        else {
            var msg = '';
            for (var c in data.kras)
                if (data.kras[c] && Array.isArray(data.kras[c]))
                    data.kras[c].forEach(function (item) {
                        if (item._id && item.details && item.details.kra && item.scoring && item.scoring.text)
                            workflow.kras.push(item);
                        else if (item._id && item.details && item.details.kra && !msg)
                            msg = 'All kras must scored before submitting';
                    });
            if (data.kras.length == 0 || msg)
                workflow.outcome.errors.push(msg || 'No KRA was submitted');
        }
        if (!data.comment) workflow.outcome.errors.push('Please add your comment to continue.');
        if (workflow.hasErrors()) return workflow.emit('response'); else workflow.emit('checkSize');
    });
    workflow.on('checkSize', function () {
        req.app.db.models.KraAppraisal
            .count({employee: workflow.empId, period: workflow.period.id})
            .exec(function (er, count) {
                if (count && count == workflow.kras.length) workflow.emit('startAppraisal');
                else res
                    .status(count ? 400 : 500)
                    .send(count ? 'The submitted kras do not match. please refresh and try again.'
                        : 'An error occurred while retrieving kra appraisal data.');
            });
    });
    workflow.on('startAppraisal', function () {
        var done = [];
        var m = '';
        var name = req.user.employee.fullName();
        async.each(workflow.kras, function (i, cb) {
            req.app.db.models.KraAppraisal
                .findOne({employee: workflow.empId, period: workflow.period.id, _id: i._id})
                .exec(function (er, kas) {
                    if (!kas) cb(er ? 'An error occurred while retrieving kra appraisal data.' : 'Not found');
                    else if (kas.scoring && (kas.scoring.text || kas.scoring.score)) {
                        m = 'Kra has already been scored.';
                        cb(m);
                    }
                    else {
                        kas.scoring = {
                            score: req.app.scgn.defaultLevels[i.scoring.text.toLowerCase()],
                            text: i.scoring.text,
                            by: name,
                            date: today
                        };
                        workflow.sum += (kas.details.catW * kas.scoring.score);
                        workflow.unit += kas.details.catW;
                        kas.save(function (e, r) {
                            done.push(r || kas);
                            cb(e);
                        });
                    }
                });
        }, function (err) {
            if (err) {
                done.forEach(function (itm) {
                    itm.save({scoring: {}});
                });
                res.json({message: err ? err : "Subordinate appraisal could not be completed.", success: false});
            } else {
                workflow.completed = done;
                workflow.emit('getCounselLeads');
            }
        });
    });
    workflow.on('getCounselLeads', function () {
        req.app.db.models.Lead
            .findOne({period: workflow.period, employee: workflow.empId})
            .exec(function (er, item) {
                workflow.leads = item;
                workflow.emit('updateSummary');
            });
    });
    workflow.on('updateSummary', function () {
        var value = req.app.scgn.utility.calculateScore(workflow.sum, workflow.unit);
        console.log("Workflow Leads", workflow.leads);
        req.app.db.models.EmployeeAppraisal
            .findOneAndUpdate({employee: workflow.empId, period: workflow.period.id},
                {
                    kra: {
                        list: workflow.completed,
                        score: value.score,
                        text: value.text,
                        date: new Date(),
                        comment: req.body.comment,
                        leads: workflow.leads
                    }
                },
                function () {
                    workflow.outcome.success = true;
                    workflow.outcome.message = "Subordinate appraisal was successfully processed.";
                    workflow.emit('response');
                });
    });
    workflow.on('isAppraisalOpen', function () {
        req.app.scgn.utility.getCurrentPeriod(
            req.app.db.models.AppraisalPeriod,
            function (er, p) {
                if (er) {
                    workflow.outcome.errors.push(er);
                    return workflow.emit('response');
                }
                else {
                    workflow.period = p;
                    workflow.emit('validate');
                }
            });
    });
    workflow.emit('isAppraisalOpen');
};
// Performing Subordinate KRA Appraisal
exports.getSubAppraisals = function (req, res) {
    var data = {isOpen: false, period: {}, subs: []};// sub :{appraised, kras:[]}
    var async = require('async');
    var us = require('underscore');

    function getData() {
        req.app.db.models.AppraisalPeriod.find(function (err, pers) {
            if (err) res.status(500).send('Could not retrieve the appraisal period status.');
            else if (pers.length == 0) res.status(404).send('no Appraisal period found.');
            else {
                var per = pers[pers.length - 1];
                var today = new Date(new Date().toDateString());
                data.isOpen = today >= per.start && today <= per.end;
                data.period = per;

                async.forEach(req.user.employee.subordinates, function (sub, cb) {
                    var subData = {kras: []};
                    async.parallel([
                            function (cbx) {
                                req.app.db.models.Employee.findById(sub, 'firstName lastName designation department employeeId')
                                    .populate('department')
                                    .exec(function (er, emp) {
                                        if (er) {
                                            cbx();
                                        }
                                        else {
                                            subData.emp = {
                                                code: emp.id,
                                                name: emp.fullName(),
                                                desig: emp.designation,
                                                employeeId: emp.employeeId,
                                                dept: emp.department.name
                                            };
                                            cbx();
                                        }
                                    })
                            }, function (cbx) {
                                req.app.db.models.EmployeeAppraisal
                                    .findOne({employee: sub, period: data.period.id}, 'kra',
                                        function (e, r) {
                                            if (r) subData.text = r.kra.text;
                                            cbx();
                                        });
                            }, function (cbx) {
                                req.app.db.models.Lead
                                    .findOne({employee: sub, period: data.period.id}, 'count explanation',
                                        function (e, r) {
                                            if (r) subData.leads = r;
                                            cbx();
                                        });
                            },
                            function (cbx) {
                                req.app.db.models.KraAppraisal
                                    .find({employee: sub, period: data.period.id},
                                        '-details.catW -period -employee')
                                    .exec(function (er, kas) {
                                        if (er) cbx('An error occurred while retrieving kra appraisal data.');
                                        else if (kas.length > 0) {
                                            var item = kas[0].scoring;
                                            subData.kras = us.groupBy(kas, function (o) {
                                                return o.details.area;
                                            });
                                            subData.appraised = !!(item.score && item.by && item.date, item.text);
                                            if (subData.appraised) {
                                                subData.date = item.date;
                                                subData.by = item.by;
                                            }
                                            cbx();
                                        }
                                        else {
                                            subData.kras = [];
                                            subData.appraised = false;
                                            cbx();
                                        }
                                    });
                            }
                        ], function () {
                            data.subs.push(subData);
                            cb();
                        }
                    );
                }, function () {
                    res.json(data);
                });
            }
        });
    }

    if (req.user.employee.isASupervisor()) getData();
    else res.status(403).send('Employee is not a supervisor.');
};
// KRA Appraisal Summary
exports.getKraSummary = function (req, res) {
    res.json([]);
};

