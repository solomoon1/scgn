module.exports = function (app, mongoose) {
    require('./assessment/module.js')(app, mongoose);
    require('./competence/module')(app, mongoose);
    require('./confirmation/module')(app, mongoose);
    require('./kra/module')(app, mongoose);
    require('./evaluation/module')(app, mongoose);

    require('./models')(app, mongoose);
    var controller = require('./controller.js');

    app.get('/appraisal/current', controller.currentPeriod);
    app.post('/appraisal/initiate', app.ensureHR, controller.initiateAppraisalPeriod);
    app.post('/appraisal/extend', app.ensureHR, controller.extendAppraisalPeriod);

    app.post('/appraisalx/resetemp', controller.resetEmpApp);


    /*Update 2016/12/05
     * Support for viewing of historical appraisal records.
     */
    app.get('/appraisal/periods', app.ensureHR, controller.getAllPeriodsExceptCurrent);


    app.get('/appraisal/krapreview/:id', app.ensureHR, controller.previewEmployeeKra);
};