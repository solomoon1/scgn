﻿var mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var mS = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
var isLeap = function (y) { return (y % 4 == 0 && y % 100 != 0) || (y % 100 == 0 && y % 400 == 0); }
function getLast(n, y) {
    var lastDay;
    switch (n) {
        case 3: case 5: case 8: case 10: lastDay = 30; break;
        case 0: case 2: case 4: case 6: case 7: case 9: case 11: lastDay = 31; break;
        case 1: if (isLeap(y)) lastDay = 29; else lastDay = 28; break; default: break;
    }
    return lastDay;
}
function getMonth(index) { return mS[index]; }
var lastWorkDay = function (time) {
    var isWeekend = time.getDay() == 0 || time.getDay() == 6;
    if (isWeekend) {
        return new Date(time.getFullYear(), time.getMonth(), time.getDate() - (time.getDay() == 0 ? 2 : 1));
    }
    return time;
};


exports.approveOutOfficeRequest = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    workflow.on('checkrequestStatus', function () {
        req.app.db.models.OutOffice
            .findById(req.body._id).populate('employee')
            .exec(function (err, pc) {
                if (err) {
                    workflow.outcome.errors.push('Could not retrieve the request details. Try again later');
                    return workflow.emit('response');
                } else if (pc) {
                    var t;
                    switch (pc.status) {
                        case 'Pre-Approval': case 'Disapproved': break;
                        case 'Approved': t = 'Request already approved.'; break;
                        default: break;
                    }
                    if (t) {
                        workflow.outcome.errors.push(t);
                        return workflow.emit('response');
                    }
                    else if (req.user.employee.access < pc.employee.access && pc.toApprove == req.user.employee.id) {
                       workflow.cashR = pc; workflow.emit('preapprove');
                    }  else if (pc.toApprove == req.user.employee.id) {
                       workflow.cashR = pc; workflow.emit('preapprove');
                       console.log(String.Concat('Allowing approval of ',pc.employee.fullName(), "'s request by ",
                           req.user.employee.fullName(), '. Even when the access levels are ', pc.employee.access, ' && ', req.user.employee.access ));
                    } else {
                        workflow.outcome.errors.push('You cannot pre-approve a request of' + pc.requester);
                        return workflow.emit('response');
                    }
                }
                else {
                    workflow.outcome.errors.push('Request was not found.');
                    return workflow.emit('response')
                }
            });
    });
    workflow.on('preapprove', function () {
        workflow.cashR.status = 'Approved';
        workflow.cashR.save(function (err, p) {
            if (err) {
                workflow.outcome.errors.push('An error occurred while approving the request. Try again later');
                return workflow.emit('response');
            }
            else {
                workflow.outcome.success = true;
                workflow.outcome.message = 'Out of Office Request was successfully approved';
                req.app.utility.notify.emit('outOfficeRequestStatusChanged', p, req.user);
                workflow.emit('response');
            }
        });
    });
    workflow.emit('checkrequestStatus');
};
exports.declineOutOfficeRequest = function (req, res) {
    var workflow = req.app.utility.workflow(req, res);
    workflow.on('checkrequestStatus', function () {
        req.app.db.models.OutOffice
            .findById(req.body._id)
            .populate('employee')
            .exec(function (err, pc) {
                if (err) {
                    workflow.outcome.errors.push('Could not retrieve the request details. Try again later');
                    return workflow.emit('response');
                } else if (pc) {
                    var t;
                    switch (pc.status) {
                        case 'Pre-Approval': case 'Disapproved': break;
                        case 'Approved': t = 'Request already approved.'; break;
                        default: break;
                    }
                    if (t) {
                        workflow.outcome.errors.push(t);
                        return workflow.emit('response');
                    }
                    else if (req.user.employee.access < pc.employee.access && pc.toApprove == req.user.employee.id) {
                        workflow.cashR = pc; workflow.emit('decline');
                    }
                    else if (pc.toApprove == req.user.employee.id) {
                        workflow.cashR = pc; workflow.emit('decline');
                        console.log(String.Concat('Allowing declining of ',pc.employee.fullName(), "'s request by ",
                            req.user.employee.fullName(), '. Even when the access levels are ', pc.employee.access, ' && ', req.user.employee.access ));
                    } else {
                        workflow.outcome.errors.push('You cannot pre-approve a request of' + pc.employee.fullName());
                        return workflow.emit('response');
                    }
                }
                else {
                    workflow.outcome.errors.push('Request was not found.');
                    return workflow.emit('response');
                }
            });
    });
    workflow.on('decline', function () {
        workflow.cashR.status = 'Disapproved';
        workflow.cashR.save(function (err, s) {
            if (err) {
                workflow.outcome.errors.push('An error occurred while approving the request. Try again later');
                return workflow.emit('response');
            }
            else {
                workflow.outcome.success = true;
                workflow.outcome.message = 'Out of Office Request has been declined';
                req.app.utility.notify.emit('outOfficeRequestStatusChanged', s, req.user);
                workflow.emit('response');
            }
        });
    });
    workflow.emit('checkrequestStatus');
};
exports.saveOutOfOfficeRequest = function(req, res){
    var workflow = req.app.utility.workflow(req, res);

    workflow.on('validate', function(){
        var required = [];
        [
            {field: 'requestType', label: 'Request type'},
            {field:'date', label: 'Date'}].forEach(function(i){
            !req.body[i.field] && required.push(i.label);
        });
        if(required.length){
            workflow.outcome.errors.push('The ' + required.join(',') + ' field' +(required.length > 1? 's are ': ' is') + required);
            return workflow.emit('response');
        }

        workflow.emit('saveRequest');
    });

    workflow.on('checkRequestRange', function () {

    });

    workflow.on('saveRequest', function(){
        var request = {

            employee: req.user.employee.id,
            hrcomment: req.body.hrcomment,
            employeeId: req.user.employee.employeeId,
            department: req.user.employee.department.name,
            name: req.user.employee.fullName(),
            designation: req.user.employee.designation,
            type: req.body.requestType.name,
            date: req.body.date,
            status: 'Pre-Approval',
            timeBased: req.body.requestType.timeBased,
            toApprove: req.user.employee.supervisor
        };
        if(request.timeBased) {
            var d = new Date(req.body.resumptionTime), p = new Date(req.body.date);
            // d.setDate(p.getDate()); d.setYear(d.getFullYear()); d.setMonth(d.getMonth());
            p.setHours(d.getHours()); p.setMinutes(d.getMinutes());
            request.resumptionTime = p;
            request.resumptionDate = p.toDate()
        }else request.resumptionDate = new Date(req.body.resumptionDate).toDate();

        if(request.type == 'Others')
            request.details = req.body.details;

        workflow.item = new req.app.db.models.OutOffice(request);

        workflow.item.save(function(e){
            if(e) return workflow.emit('exception',e);
            workflow.outcome.message = "Successfully submitted request";
            workflow.emit('response');
        });
    });

    workflow.emit('validate');
};
