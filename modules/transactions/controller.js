var mongoose = require('mongoose');
var request = require('request');
var async = require('async');
var moment = require('moment');

/*
 Approved transactions would have
 - ApprovalCode
 - status
 - tranxid === OrderId
 'http://localhost:3040/api/tranxResponse'
 '/api/tranxResponse'
 */


exports.getTransactions = function (req, res) { //return all Orders
    req.app.db.models.Transaction.find({}, null, { sort: { createdOn: -1 } }, function (err, transactions) {
        if (err) {
            return res.status(500).json({ message: 'Failed to fetch Transaction(s). ' + err.message });
        }
        res.json({ message: 'Transactions fetched!', transactions: transactions });
    });
};

exports.getTransaction = function (req, res) { //return all Orders
    var trxId = req.params.trxId;
    req.app.db.models.Transaction.find({ tranxId: trxId }, {
        _id: 0,
        tranxId: 0,
        __v: 0,
        memberId: 0
    }, function (err, transaction) {
        if (err) {
            return res.status(500).json({ message: 'Failed to fetch Transaction(s). ' + err.message });
        }
        res.json({ message: 'Transaction Detail fetched', transaction: transaction[0] });
    });
};

exports.getMemberTransaction = function (req, res) { //return all Orders
    var memberId = req.params.memberId;
    var start = moment().startOf('year').toDate();
    var end = moment().endOf('year').toDate();


    req.app.db.models.Transaction.find(
        {
            $and: [{ memberId: memberId }, { createdOn: { $gte: start } }, { createdOn: { $lte: end } }, { type: 'subscription' }]
        },
        { _id: 0, tranxId: 0, __v: 0, customer: 0, items: 0 },
        { sort: { createdOn: -1 } },
        function (err, transaction) {
            if (err) {
                return res.status(500).json({ message: 'Failed to fetch Transaction(s). ' + err.message });
            }
            res.json({ message: 'Transaction Detail fetched', memberPayment: transaction[0] });
        });
};

exports.getTransactionError = function (req, res) { //return all Orders
    var trxId = req.params.trxId;
    request({
        method: 'GET',
        url: req.app.config.tranxParameter.initialURL + 'status/' + trxId,
        headers: req.app.config.tranxParameter.headers
    }, function (err, resp) {
        if (err) return res.status(400).json({ message: 'Failed to fetch status.' });
        res.json({ tranxError: JSON.parse(resp.body) });
    });
};

exports.createTransaction = function (req, res) {
    //create a new Order
    //req.app.db.models.User.findOne
    //['book', 'donation','event-training','subscription']
    var tranx = req.body;
    if (tranx && tranx.type) {
        //ensure the title does not exist
        // return res.json({message: 'Got order', data: tranx});

        //for book payment
        if (tranx.type == 'book') {
            async.waterfall(
                [
                    function (cb) {
                        //make request to get tranxid
                        var payLoad = {
                            amount: 0,
                            currency: 566,
                            description: '',
                            returnUrl: req.app.config.tranxParameter.returnUrl(req, '/api/tranxResponse'),
                            secretKey: req.app.config.tranxParameter.secretKey,
                            fee: 0
                        };
                        var getBooksCount = function (books) {
                            var sum = 0;
                            books.forEach(function (v) {
                                sum += v.count;
                            });
                            return sum;
                        };
                        payLoad.amount = tranx.totalCost;
                        var difBooks = getBooksCount(tranx.items);
                        payLoad.description = 'Payment for ' + difBooks;
                        payLoad.description += (difBooks > 1) ? ' copies of ' : ' copy of ';
                        payLoad.description += tranx.items.length;
                        payLoad.description += (tranx.items.length > 1) ? ' different books' : ' book';
                        request({
                            method: 'POST',
                            url: req.app.config.tranxParameter.initialURL + req.app.config.tranxParameter.merchantId,
                            json: true,
                            body: payLoad,
                            headers: req.app.config.tranxParameter.headers
                        }, function (err, resp) {
                            cb(err, resp.body, payLoad.description);
                        })
                    },
                    function (trxId, trxDescription, cb) {
                        //make payment request and save trxId
                        var newTrx = new req.app.db.models.Transaction();
                        var bookItems = tranx.items;
                        bookItems.forEach(function (v, i) {
                            var objx = {};
                            objx.bookId = v.id;
                            objx.price = v.price;
                            objx.count = v.count;
                            objx.cost = (v.price * v.count);
                            newTrx.items.push(objx);
                        });
                        var trxObj = {
                            tranxId: trxId,
                            amount: tranx.totalCost,
                            type: tranx.type,
                            email: tranx.email,
                            phone: tranx.phone ? tranx.phone : '',
                            description: trxDescription
                        };
                        newTrx = Object.assign(newTrx, trxObj);
                        newTrx.save(function (err, trx) {
                            if (err) cb(err, null);
                            cb(null, req.app.config.tranxParameter.initialURL + trx.tranxId);
                        })
                    }
                ],
                function (err, result) {
                    if (err) {
                        return res.status(500).json({ message: 'Error Creating Transaction', errMsg: err.message });
                    }
                    res.json({ message: 'Order Created!', trxUrl: result });
                })
        }
        //for subscription payment
        // tranx = { _id, type }
        if (tranx.type == 'subscription') {
            async.waterfall(
                [
                    function (cb) {
                        req.app.db.models.Member.find({ _id: tranx._id }, function (err, m) {
                            cb(err, m[0]);
                        })
                    },
                    function (member, cb) {
                        //make request to get tranxid
                        var payLoad = {
                            amount: 0,
                            currency: 566,
                            description: '',
                            returnUrl: req.app.config.tranxParameter.returnUrl(req, '/api/tranxResponse'),
                            secretKey: req.app.config.tranxParameter.secretKey,
                            fee: 0
                        };
                        var getPayLoad = function (m, p) {
                            var amount = { SMEs: 150000, Institutions: 200000, LargeEnterprises: 250000 };
                            switch (m.category) {
                                case 'SMEs':
                                    p.amount = amount.SMEs;
                                    break;
                                case 'Institutions':
                                    p.amount = amount.Institutions;
                                    break;
                                case 'Large Enterprises':
                                    p.amount = amount.LargeEnterprises;
                                    break;
                            }

                            var today = (new Date()).getFullYear();
                            var memeberName = m.lastName.toUpperCase() + ', ' + m.firstName[0].toUpperCase() + m.firstName.slice(1);
                            p.description = 'Subscription for ' + memeberName + ' for ' + m.category + ' ' + today;
                        };
                        getPayLoad(member, payLoad);
                        //remember to comment this out
                        // payLoad.amount = payLoad.amount / 1000;

                        request({
                            method: 'POST',
                            url: req.app.config.tranxParameter.initialURL + req.app.config.tranxParameter.merchantId,
                            json: true,
                            body: payLoad,
                            headers: req.app.config.tranxParameter.headers
                        }, function (err, resp) {
                            cb(err, resp.body, {
                                id: tranx._id,
                                amount: payLoad.amount,
                                email: member.mEmail,
                                phone: member.mPhone,
                                description: payLoad.description
                            });
                        })
                    },
                    function (trxId, memberObj, cb) {
                        //make payment request and save trxId
                        var newTrx = new req.app.db.models.Transaction();
                        var trxObj = {
                            tranxId: trxId,
                            amount: memberObj.amount,
                            type: tranx.type,
                            email: memberObj.email,
                            phone: memberObj.phone ? tranx.phone : '',
                            description: memberObj.description,
                            memberId: memberObj.id
                        };
                        newTrx = Object.assign(newTrx, trxObj);
                        newTrx.save(function (err, trx) {
                            if (err) cb(err, null);
                            cb(null, req.app.config.tranxParameter.initialURL + trx.tranxId);
                        })
                    }
                ],
                function (err, result) {
                    if (err) {
                        return res.status(500).json({ message: 'Error Creating Transaction', errMsg: err.message });
                    }
                    res.json({ message: 'Order Created!', trxUrl: result });
                })
        }

        if (tranx.type == 'donation') {
            async.waterfall(
                [
                    function (cb) { //post payment initiation to the processor; to obtain tranxid
                        //make request to get tranxid
                        var payLoad = {
                            amount: 0,
                            currency: 566,
                            description: '',
                            returnUrl: req.app.config.tranxParameter.returnUrl(req, '/api/tranxResponse'),
                            secretKey: req.app.config.tranxParameter.secretKey,
                            fee: 0
                        };

                        payLoad.amount = tranx.amount;
                        payLoad.description = tranx.benefactor.name + ' donated ' + tranx.amount + ' for ' + tranx.benefactor.remark;
                        request({
                            method: 'POST',
                            url: req.app.config.tranxParameter.initialURL + req.app.config.tranxParameter.merchantId,
                            json: true,
                            body: payLoad
                            ,
                            headers: req.app.config.tranxParameter.headers
                        }, function (err, resp, body) {
                            if (body) JSON.parse(body);
                            cb(err, resp.body, payLoad.description);
                        })
                    },
                    function (trxId, trxDescription, cb) { //create this transaction document
                        //make payment request and save trxId
                        var newTrx = new req.app.db.models.Transaction();
                        var trxObj = {
                            tranxId: trxId,
                            amount: tranx.amount,
                            type: tranx.type,
                            email: tranx.benefactor.email,
                            phone: tranx.benefactor.phone ? tranx.benefactor.phone : '',
                            description: trxDescription,
                            customer: tranx.benefactor.name
                        };
                        newTrx = Object.assign(newTrx, trxObj);
                        newTrx.save(function (err, trx) {
                            if (err) cb(err, null);
                            cb(null, req.app.config.tranxParameter.initialURL + trx.tranxId);
                        })
                    }
                ],
                function (err, result) {
                    if (err) {
                        return res.status(500).json({ message: 'Error Creating Transaction', errMsg: err.message });
                    }
                    res.json({ message: 'Order Created!', trxUrl: result });
                })
        }

        if (tranx.type == 'training-event') {
            async.waterfall(
                [
                    function (cb) {
                        //get the event
                        req.app.db.models.TrainingAndEvent.find({ _id: tranx.id }, function (err, tnE) {
                            cb(err, tnE[0]);
                        })
                    },
                    function (tnE, cb) { //post payment initiation to the processor; to obtain tranxid
                        //make request to get tranxid
                        var payLoad = {
                            amount: 0,
                            currency: 566,
                            description: '',
                            returnUrl: req.app.config.tranxParameter.returnUrl(req, '/api/tranxResponse'),
                            secretKey: req.app.config.tranxParameter.secretKey,
                            fee: 0
                        };

                        payLoad.amount = tranx.fee;
                        payLoad.description = 'Payment of ' + tnE.fee + ' for ' + tnE.title + ' ' + tnE.type + ' by ' + tranx.name.toUpperCase();
                        request({
                            method: 'POST',
                            url: req.app.config.tranxParameter.initialURL + req.app.config.tranxParameter.merchantId,
                            json: true,
                            body: payLoad
                            ,
                            headers: req.app.config.tranxParameter.headers
                        }, function (err, resp, body) {
                            JSON.parse(body);
                            cb(err, resp.body, payLoad.description);
                        })
                    },
                    function (trxId, trxDescription, cb) { //create this transaction document
                        //make payment request and save trxId
                        var newTrx = new req.app.db.models.Transaction();
                        var trxObj = {
                            tranxId: trxId,
                            amount: tranx.fee,
                            type: tranx.type,
                            email: tranx.email,
                            phone: tranx.phone,
                            description: trxDescription
                        };
                        newTrx = Object.assign(newTrx, trxObj);
                        newTrx.save(function (err, trx) {
                            if (err) cb(err, null);
                            cb(null, req.app.config.tranxParameter.initialURL + trx.tranxId);
                        })
                    }
                ],
                function (err, result) {
                    if (err) {
                        return res.status(500).json({ message: 'Error Creating Transaction', errMsg: err.message });
                    }
                    res.json({ message: 'Order Created!', trxUrl: result });
                })
        }

    } else {
        errorMessage = 'No payload provide';
        res.status(500).json({ message: errorMessage });
    }

};

exports.concludeTransaction = function (req, res) {
    /*
     This is the controller action that receives the payment processor's final response
     The processor posts data to this route
     Check for approval code and status
     Also get status from the processor's api via http://processorsapi/status/tranxId
     compare the data from this query and the req.body data or just use the query response

     */
    var p = req.body;
    var orderId = p.trxId;
    if (p && p.status.toLowerCase() == 'approved' && orderId) {
        async.waterfall([
            function (cb) {
                /*query the processor's endpoint to get transaction status*/
                request({
                    method: 'GET',
                    url: req.app.config.tranxParameter.initialURL + 'status/' + orderId
                }, function (err, trxStatus) {
                    cb(err, trxStatus.body);
                })
            },
            function (trxStatus, cb) {
                /*Use the received OrderId to query your transaction model*/
                trxStatus = JSON.parse(trxStatus);
                if (trxStatus && trxStatus.Status.toLowerCase() == 'approved') {
                    req.app.db.models.Transaction
                        .find({ tranxId: orderId }, function (err, trx) {
                            if (err) cb(err.message, null);
                            else {
                                trx = trx[0];
                                var trxOperation = function (t, ts, cb) {
                                    t.customer = ts['Card Holder'];
                                    t.status = 'Approved';
                                    t.scheme = ts['Scheme'];
                                    t.currency = ts['Currency'];
                                    t.statusDescription = ts['StatusDescription']
                                    //t.approvalCode = ts['Approval Code']

                                    t.save(function (err, trxR) {
                                        cb(err, trxR);
                                    });
                                };
                                if (trx.type == 'book') { //for book payment
                                    var cartBooks = [];
                                    trx.items.forEach(function (v) {
                                        cartBooks.push({ count: v.count, _id: v.bookId });
                                    });
                                    async.forEach(cartBooks, function (v, cbs) {
                                        req.app.db.models.Books.update(
                                            { _id: v._id },
                                            { $inc: { stockCount: -(parseInt(v.count)) } },
                                            { new: true },
                                            function (err, d) {
                                                cbs(err, d);
                                            }
                                        );
                                    }, function (err) {
                                        if (!err) console.log('Book model update succeeded');
                                        if (err) {
                                            console.log('Book Update Error!' + err && err.message);
                                            cb(err, null);
                                        }
                                        else trxOperation(trx, trxStatus, cb);
                                    });
                                }
                                if (trx.type == 'subscription') {
                                    trxOperation(trx, trxStatus, cb);
                                }
                                if (trx.type == 'donation') {
                                    var getCustomerName = function (trxDesc) {
                                        var i = trxDesc.indexOf('donated');
                                        var name = trxDesc.slice(0, i - 1);
                                        return name;
                                    };
                                    var donationBenefactor = {
                                        name: getCustomerName(trx.description),
                                        email: trx.email,
                                        phone: trx.phone
                                    };
                                    var newDonation = req.app.db.models.Donations();
                                    newDonation = Object.assign(newDonation, {
                                        mode: 'Online',
                                        amount: trx.amount,
                                        remark: trx.description,
                                        benefactor: donationBenefactor
                                    });
                                    newDonation.save(function (err, donation) {
                                        trxOperation(trx, trxStatus, cb);
                                    });
                                }
                                if (trx.type == 'training-event') {
                                    trxOperation(trx, trxStatus, cb);
                                }
                            }
                        });
                }
                else {
                    cb(new Error('The transaction failed'), null);
                }
            }
        ],
            function (err, result) {
                if (err) {
                    req.app.utility.notify.emit('tranxResponseEvent', orderId);
                    return res.redirect('/pub/tranxError')
                };
                req.app.utility.notify.emit('tranxResponseEvent', orderId);
                res.redirect('/pub/tranxReceipt');
            });
    }
    else {
        //update the transaction record, change status to declined or whatever is sent back
        req.app.db.models.Transaction
            .find({ tranxId: orderId }, function (err, trx) {
                if (err) {
                    //cb(err.message, null);
                    if (err) console.log(err);
                    else console.log('Failed Transaction Response: ', trxResp);
                    res.redirect('/pub/tranxError');
                }
                else {
                    trx = trx[0];
                    var trxOperation = function (t, ts, cb) {
                        t.status = 'Declined';
                        t.statusDescription = p.status;

                        t.save(function (err, trxR) {
                            cb(err, trxR);
                        });
                    };
                    trxOperation(trx, p, function (err, trxResp) {
                        if (err) console.log(err);
                        else console.log('Failed Transaction Response: ', trxResp);
                        req.app.utility.notify.emit('tranxResponseEvent', orderId);
                        res.redirect('/pub/tranxError');
                    })
                }
            });
    }
};


exports.updateTransaction = function (req, res) {
    /*
     update a Order
     this is done by a privileged employee
     most likely fields to change are
     - stockCount
     - editedOn
     -
     */

};


//#endregion
