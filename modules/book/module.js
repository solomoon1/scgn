module.exports = function (app, mongoose) {
    require('./models')(app, mongoose);
    var controller = require('./controller.js');
    var formidable = require('formidable');
    var path = require('path');
    var pathSeparator = path.sep;

    /*
        returns with req obj modified with
            - req.myFile
            - req.myFields
            - req.isMyFileValid
     */
    var uploaderHelper = function(uploadDir, req, res, next){
        var form = new formidable.IncomingForm();
        var fileValid = false;
        form.encoding = 'utf-8';
        form.uploadDir= uploadDir;
        form.on('fileBegin', function(name, file) {
            var fPath = file.path.split(pathSeparator);
            fPath.pop();
            fPath.push(file.name);
            file.path = fPath.join(pathSeparator);
            //var fExt = file.type.split(pathSeparator)[1];
            var fExt = path.extname(file.path).substring(1);
            fileValid = ('jpeg jpg png'.split(' ').indexOf(fExt) > -1) ? true : false;
        });
        form.parse(req, function(err, fields, files) {
            req.myFile = files;
            req.myFields = fields;
            req.isMyFileValid = fileValid;
            next();
        });

    };
    var myUploader = function(req, res, next){
        var bookCoverPath = path.join('public', 'images', 'book-cover');
        uploaderHelper(bookCoverPath, req, res, next);
    };

    app.get('/api/books', controller.getBooks);
    app.get('/api/getPubBooks', controller.getPubBooks);
    app.post('/api/book',
        myUploader,
        controller.createBook);
    app.put('/api/book',
        myUploader,
        controller.updateBook);

    /* Book Stocking */
    app.get('/api/bookStock', controller.getBookStocks);
    app.post('/api/bookStock', controller.updateBookStock);
    app.put('/api/bookStock', controller.editBookStock); //for book retirement

    /* Book Request and Retirement */
    app.get('/api/bookRequests/:requestType', controller.getBookRequests);
    app.post('/api/bookRequest', controller.makeBookRequest);
    app.put('/api/bookRequest', controller.updateBookRequest);
    app.delete('/api/bookRequest/:requestId', controller.declineBookRequest);

     app.post('/api/setIsActive', controller.setIsActive);
    // app.put('/employee/education', controller.editEducation);
    // app.put('/employee/skill', controller.editSkill);
    // app.put('/employee/dependant', controller.editDependant);
    // app.post('/employee/picture', controller.changePicture);
    // app.post('/weeklyReport/report', controller.postWeeklyReport);
};