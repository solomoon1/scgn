module.exports = function (app, mongoose) {
    require('./models')(app, mongoose);
    var controller = require('./controller.js');


    app.get('/api/pub/tne', controller.getPubTnE);
    app.get('/api/tne'
        // , app.ensureEmpRecord
        , controller.getTnE);

    app.post('/api/tne/setIsActive', controller.setIsActive);
    app.post('/api/tne', app.ensureHR || app.ensureTrainingAndEvent, controller.createTnE);
    app.put('/api/tne', app.ensureHR || app.ensureTrainingAndEvent, controller.updateTnE);
};