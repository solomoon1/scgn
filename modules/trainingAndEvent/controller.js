var
    mongoose = require('mongoose'),
    async = require('async');

exports.getTnE = function (req, res) {
    var today = new Date();
    return req.app.db.models.TrainingAndEvent.find({ $and : [ {status: { $ne: 'Concluded'}}, { end : { $gte: today}} ] },null,{ sort: { createdOn: -1}}, function(err, tne){
        if(err)  return res.status(500).json({ message: 'Failed to fetch Training/Event', err: err.message });
        res.json({ message: 'Training/Events fetched!', tne: tne });
    })
};
exports.getPubTnE = function (req, res) {
    var today = new Date();
    return req.app.db.models.TrainingAndEvent.find(
        { $and : [ {status: { $ne: 'Concluded'}}, { end : { $gte: today}}, { isActive: true } ]}
        ,{ employeeId: 0}
        ,{ sort: { createdOn: -1}}
        , function(err, tne){
        if(err)  return res.status(500).json({ message: 'Failed to fetch Training/Event', err: err.message });
        res.json({ message: 'Training/Events fetched!', tne: tne });
    })
};
exports.createTnE = function (req, res) {
    var b = req.body;
    // var errorMsg = null;

    var newTnE = req.app.db.models.TrainingAndEvent();
    newTnE = Object.assign(newTnE, b);
    newTnE.save(function(err, tne){
        if(err) return res.status(500).json({ message: 'Error creating Training/Event', errMsg: err.message });
        try {
            req.app.db.models.Member.find({ isActive: true }, function (err, allMember) {
                if(err) throw new Error(err)
                allMember.forEach(function (obj) {
                    var m = {
                        firstName: obj.firstName,
                        lastName: obj.lastName,
                        eventTitle: tne.title,
                        start: tne.start,
                        end: tne.end,
                        venue: tne.venue,
                        fee: tne.fee,
                        contactName: tne.contactName,
                        contactPhone: tne.contactPhone,
                        type: tne.type,
                        scgnUrl: req.app.config.host,
                        mEmail: obj.mEmail
                    }
                    req.app.utility.notify.emit('newSCGNEvents', m);

                })
            })
        } catch (err) {
            console.log('Error sending mails to the members');
        } finally {
            res.json({ message: 'Training and Events fetched!', tne: tne });
        }

    });
};

exports.updateTnE = function (req, res) {
    var b = req.body;
    // var errorMsg = null;
    if( b && b._id) {
        var query = { _id: b._id};
        req.app.db.models.TrainingAndEvent.findOne(query, function(err, tne){
            if(err) return res.status(500).json({ message: 'Error fetching Training/Event', errMsg: err.message });
            tne = Object.assign(tne, b);
            tne.save(function (err, tne) {
                if (err) return res.status(500).json({message: 'Error updating Training/Event', errMsg: err.message});
                res.json({message: 'The Training and Events has been updated!', tne: tne});
            });

        });
    }else{
        res.status(400).json({ message: 'Incomplete request sent to the server'})
    }

};

exports.setIsActive = function (req, res) {
    var tneId = req.body.tneId;
    req.app.db.models.TrainingAndEvent.findOne({ _id: tneId }, function (err, t) {
        if (err) return res.status(500).json({
            message: 'Cannot fine book with id: ' + tneId,
            errMsg: err.message
        });
        t.isActive = !t.isActive;
        t.save(function(e, tn){
            if(e) return res.status(500).json({
                message: 'Failed to update Training and Event\'s isActive property'
            });
            res.json({ message: 'Training/Event with id ' + tneId + ' was updated successful!' });
        })
    });
};