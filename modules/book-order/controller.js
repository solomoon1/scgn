var mongoose = require('mongoose');


exports.getOrders = function (req, res) { //return all Orders
    var errorMessage = '';
    req.app.db.models.Orders.find({}, function(err, bookOrders){
        if(err) {
            errorMessage = 'Failed to fetch Book-Order(s). ' + err.message;
            return res.status(500).json({message: errorMessage});
        }
        res.json({ message: 'Book Orders fetched!', bookOrders: bookOrders});
    });
};

exports.createOrder = function (req, res) {
    //create a new Order
    //req.app.db.models.User.findOne
    var orderToCreate = req.body;
    var newOrder = new req.app.db.models.Orders();
    var errorMessage = '';

    if(req.body){
        //ensure the title does not exist
        newOrder = Object.assign(newOrder, orderToCreate);
        newOrder.save(function(err, createdOrder){
            if(err) {
                errorMessage = 'Error Creating Order: ' + query.title + ' ' + err.message;
                return res.status(500).json({message: errorMessage});
            }
            res.json({ message: 'Order Created!', order: createdOrder});
        })

    }else{
        errorMessage = 'No payload provide';
        res.status(500).json({ message: errorMessage});
    }

};

exports.updateOrder = function (req, res) {
    /*
    update a Order
    this is done by a privileged employee
    most likely fields to change are
        - stockCount
        - editedOn
        -
     */

};


//#endregion
