module.exports = function (app, mongoose) {
    require('./models')(app, mongoose);
    var controller = require('./controller.js');

    app.get('/orders', controller.getOrders);
    app.post('/order', controller.createOrder);
    app.put('/order', controller.updateOrder);

    // app.put('/employee/experience', controller.editExperience);
    // app.put('/employee/education', controller.editEducation);
    // app.put('/employee/skill', controller.editSkill);
    // app.put('/employee/dependant', controller.editDependant);
    // app.post('/employee/picture', controller.changePicture);
    // app.post('/weeklyReport/report', controller.postWeeklyReport);
};