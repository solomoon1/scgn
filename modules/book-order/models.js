module.exports = function (app, mongoose) {
    //Orders
    var customerSchema = new mongoose.Schema({
        name: { type: String, required: true},
        email: { type: String },
        phone: { type: String },
        address: { type: String }
    });

    var bookOrderSchema = new mongoose.Schema({
        bookId: { type: mongoose.Schema.Types.ObjectId, ref: 'Books' },
        customer: {
            name: { type: String, required: true},
            email: { type: String },
            phone: { type: String },
            address: { type: String }
        },
        createdOn: { type: Date, default: Date.now },
        status: { type: String, default: 'paid', required: true, enum: ['Paid', 'Delivered']},
        invoiceSent: {type: Boolean, default: false}
    });


    bookOrderSchema.set('toJSON', {virtuals: true});
    app.db.model('Orders', bookOrderSchema);
    //experience

};