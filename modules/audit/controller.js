﻿var getDateQuery = function (req) {
    var query = {
        $and: [
            {timestamp: {$gte: new Date().toDate()}},
            {timestamp: {$lte: new Date().toEnd()}}
        ]
    };
    req.query.start && (query.$and[0].timestamp.$gte = new Date(req.query.start).toDate());
    req.query.end && (query.$and[1].timestamp.$lte = new Date(req.query.end).toEnd());
    return query;
};
exports.getAuditLogs = function (req, res) {
    var query = getDateQuery(req);
    req.app.db.models.AuditLog.find(query)
        .exec(function (e, logs) {
            if (logs)
                return res.json({logs: logs, success: true, message: 'Successfully loaded records'});
            else res.json({
                success: false,
                message: e ? 'An error occurred while fetching logs' : 'No audit found for selected date range.'
            });
        });
};
exports.getAuditLogsForEmployee = function (req, res) {
    var query = getDateQuery(req);
    if (req.params.id) query.employeeId = req.params.id;
    req.app.db.models.AuditLog.find(query)
        .exec(function (e, logs) {
            if (logs)
                return res.json({logs: logs, success: true, message: 'Successfully loaded records'})
            else res.json({
                success: false,
                message: e ? 'An error occurred while fetching logs' : 'No audit found for selected date range.'
            })
        });
};
exports.getAuditLogsByAction = function (req, res) {
    var query = getDateQuery(req);
    if (req.params.id) query.action = req.params.id;
    req.app.db.models.AuditLog.find(query)
        .exec(function (e, logs) {
            if (logs)
                return res.json({logs: logs, success: true, message: 'Successfully loaded records'})
            else res.json({
                success: false,
                message: e ? 'An error occurred while fetching logs' : 'No audit found for selected date range.'
            })
        });
};
exports.getAuditLogsByEntity = function (req, res) {
    var query = getDateQuery(req);
    if (req.params.id) query.entityType = req.params.id;
    req.app.db.models.AuditLog.find(query)
        .exec(function (e, logs) {
            if (logs)
                return res.json({logs: logs, success: true, message: 'Successfully loaded records'})
            else res.json({
                success: false,
                message: e ? 'An error occurred while fetching logs' : 'No audit found for selected date range.'
            })
        });
};

exports.saveAuditLog = function () {

};