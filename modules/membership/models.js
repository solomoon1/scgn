module.exports = function (app, mongoose) {
    //membership
    var memberSchema = new mongoose.Schema({
        memberId: { type: String, required: true, unique: true }, //member's first name
        firstName: { type: String, required: true, lowercase: true },
        middleName: { type: String, default: '', lowercase: true },
        lastName: { type: String, required: true, lowercase: true },
        mEmail: { type: String, default: '', lowercase: true },
        mPhone: { type: Number, default: '' },
        contact: { type: String, default: '' },
        regNumber: { type: Number, default: '', required: false },
        dateRegistered: { type: Date, default: Date.now },
        dateConfirmed: { type: Date, default: '' },
        isActive: { type: Boolean, default: false, enum: [false, true] },
        status: { type: String, default: 'Active', required: true, enum: ['Registered', 'Declined', 'Approved', 'Active', 'Inactive'] },
        category: { type: String, required: true, enum: ['SMEs', 'Individual', 'Institutions', 'Large Enterprises'] }
    });
    memberSchema.methods.fullName = function () {
        return this.firstName + ' ' + this.lastName;
    };


    // memberSchema.virtual('name').get(function () {
    //    // return this.firstName + ' '+ this.lastName;
    //     return this.fullName();
    // });

    memberSchema.virtual('extras').get(function () {
        var name = this.fullName();
        return {
            _id: this.id,
            name: this.fullName(),
            memberId: this.memberId,
            active: this.status == 'Active',
            category: this.category
        }
    });

    // memberSchema.set('toObject',{virtuals: true});
    memberSchema.set('toJSON', { virtuals: true });

    //model for new online member registration
    var newMemberSchema = new mongoose.Schema({
        userName: { type: String, required: true, trim: true },
        email: { type: String, required: true, trim: true, unique: true },
        website: { type: String, trim: true, unique: true },
        firstName: { type: String, required: true, trim: true },
        lastName: { type: String, required: true, trim: true },
        status: { type: String, default: 'pending', enum: ['Registered', 'Declined', 'Approved', 'Active'] },
        mPhone: { type: String, default: '' },
        contact: { type: String, default: '' },
        regNumber: { type: String, default: 'Nil', required: false },
        createdOn: { type: Date, default: Date.now }

    });

    //model for new online member registration
    var MembershipFeeSchema = new mongoose.Schema({
        type: { type: String, default: 'Individual', enum: ['Individual', 'SMEs', 'Institutions', 'Large Enterprises'] },
        amount: { type: Number, required: true },
        dateCreated: { type: Date, default: Date.now },
        dateUpdated: { type: Date, default: Date.now, require: true },
        employeeId: { type: mongoose.Schema.Types.ObjectId, ref: 'Employee', required: true }

    });




    app.db.model('Member', memberSchema);

    app.db.model('NewMember', newMemberSchema);

    app.db.model('MembershipFee', MembershipFeeSchema);


};