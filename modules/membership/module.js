


module.exports = function (app, mongoose) {
    var uploaderHelper;
    require('./models')(app, mongoose);
    var path = require('path');
    var controller = require('./controller.js');
    var formidable = require('formidable');
    var multer = require('multer');

    var fileUploader = multer({
       dest: '../fileUploads/',
       fileFilter: function(req, file, cb){
           cb(null, true);
        }
    });
    /*
     returns with req obj modified with
     - req.myFile
     - req.myFields
     - req.isMyFileValid
     */
    uploaderHelper = function (uploadDir, req, res, next) {
        var form = new formidable.IncomingForm();
        var fileValid = false;
        form.encoding = 'utf-8';
        form.uploadDir = path.join(process.cwd(),uploadDir);
        form.on('fileBegin', function (name, file) {
            //obtain extension from sent file and rename file to the form file name
            var ext = file.name.split('.')[1];
            file.name = name + '.' + ext;
            //modify the file path for the upload directory and filename
            var fPath = file.path.split("\\");
            fPath.pop();
            fPath.push(file.name);
            file.path = fPath.join('\\');
            //test for file validity
            fileValid = ( file.type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') ? true : false;
        });
        form.parse(req, function (err, fields, files) {
            //populate these fields ( viewable in your controller)
            req.myFile = files.membersFile;
            req.myFields = fields;
            req.isMyFileValid = fileValid;
            next();
        });

    };



    var myUploader = function(req, res, next){
        uploaderHelper('/fileUploads/', req, res, next);
    };

    app.get('/api/membership'
        // , app.ensureEmpRecord
        , controller.getMembers);
    app.post('/api/uploadMembers'
        , myUploader
        // ,fileUploader.single('uploadMembers')
        , controller.uploadMembers);
    app.post('/api/membership', 
    app.ensureHR || app.ensureMembership, 
    controller.createMember);
    app.post('/api/membership/change-password', controller.memberChangePassword);
    app.post('/api/admin-change-member-password', app.ensureHR, controller.adminMemberPasswordReset);

            /* new member module 
    //get all new member list
    app.get('/api/newMember/created',
        app.ensureHR || app.ensureMembership,
        controller.getCreatedNewMemberList);
        */
    //get all pending new member list

     app.get('/api/membership/active',
       //app.ensureHR || app.ensureMembership,
        controller.getActiveMembers);


    //get approved member with given id in req.params.id
    app.get('/api/getMember/:id', controller.getApprovedMember);

        //get specified Member - expects req.body.status = Registered | Approved | Declined | Active | Inactive
    app.get('/api/newMember/getMembers', controller.getSpecifiedMember)

    //privileged user approves or decline an online member's membership request
    app.post('/api/newMember/update',
        app.ensureCanApproveMembership,
    controller.updateNewMemberStatus);
    
    app.post('/api/newMember/postMembers', controller.getSpecifiedMember)

    //member register on the site
    app.post('/api/newMember/register', controller.onlineMemberRegister)
    //add new member data
    app.post('/api/newMember/add',
        app.ensureHR || app.ensureMembership,
        controller.addNewMemberList);
    //update member status
    app.post('/api/newMember/updateStatus',
        app.ensureHR || app.ensureMembership,
        controller.updateStatusNewMemberList);

    app.get('/api/getMembershipExcel', controller.getMembershipExcel );
    app.post('/api/getMembership/setIsActive', controller.setIsActive);

    //Section to handle Membership Fees
    app.get('/api/getMembershipFees', controller.getMembershipFees );
    app.post('/api/setMembership/updateFee', controller.updateFee);



};