var
    mongoose = require('mongoose'),
    async = require('async')
    , XLSX = require('xlsx')
    , path = require('path')
    , fs = require('fs')
    ;

var removeFile = function (path, cb) {
    path && fs.unlink(path, function (err) {
        if (err) console.log('File at "', path, '" was not deleted!');
        else console.log('File at "', path, '" was deleted!');
        cb && cb();
    })
};

var getErrorMsg = function (data, reason) {
    errorList.push({ member: data, reason: reason });
};


exports.getMembers = function (req, res) {
    return req.app.db.models.Member.find({}, function (err, members) {
        if (err) return res.status(500).json({ message: 'Failed to fetch Members', err: err.message });
        res.json({ message: 'Members fetched!', members: members });
    })
};

exports.uploadMembers = function (req, res) {
    //expecting an excel file names members having the members data
    /*
     req.myFile
     - req.myFields
     - req.isMyFileValid
     */

    /*
     1. import an excel reader module
     2. get the data
     3. verify that the correct headers are sent
     4. use the data to write to the members model
     5. return the status of the write process
     */

    if (req.isMyFileValid) {
        //read data, write to model and return status

        async.waterfall(
            [
                function (cb) {
                    var file = path.join(process.cwd(), '/fileUploads/membersFile.xlsx');
                    var workbook = XLSX.readFile(file);
                    var worksheet = workbook.Sheets["members"];
                    if (!worksheet) {
                        var sheet = workbook.SheetNames[0];
                        worksheet = workbook.Sheets[sheet];
                    }
                    var roa = XLSX.utils.sheet_to_row_object_array(worksheet);
                    removeFile(file, function () {
                        cb(null, roa);
                    })
                },
                function (data, cb) {//test for already saved member
                    /*
                     pick the first data record sent
                     check for the required headers
                     if not found call cb with an error
                     else proceed with the processing
                     -- required headers
                     1. first name
                     2. last name
                     3. status (Active|Inactive)
                     4. category (SMEs | Institutions | Large Enterprises )
                     });
                     */
                    var getErrorResult = function (v, msg) {
                        var m = msg ? (new Error(msg)) : null;
                        return {
                            valid: v,
                            message: m
                        }
                    };
                    var headers = ['FIRST NAME', 'LAST NAME', 'CATEGORY', 'STATUS', 'DATE REGISTERED', 'CONTACT ADDRESS', 'REGISTRATION NUMBER'];
                    var verifyFileHeaders = function (data, hList) {
                        var isValid = false;
                        //check for empty data set
                        if (!data) return getErrorResult(true, 'No Headers!');
                        //check for required headers
                        var boolList = [];
                        hList.forEach(function (h) {
                            Object.keys(data).indexOf(h) === -1 ? boolList.push(true) : boolList.push(false);
                        });
                        isValid = (boolList.indexOf(true) > -1) ? true : false;
                        if (isValid) return getErrorResult(isValid, 'One or more required HEADER(S) is missing');
                        else return getErrorResult(false);
                    };
                    var headerTest = verifyFileHeaders(data[0], headers);
                    if (headerTest.valid) {
                        cb(headerTest.message);
                    }
                    else {
                        async.each(data, function (d, cb) {
                            var query = {
                                firstName: d['FIRST NAME'].toLowerCase().trim(),
                                lastName: d['LAST NAME'].toLowerCase().trim(),
                                employeeId: d['FIRST NAME'].toLowerCase().trim()
                            };
                            req.app.db.models.Member.find(query, function (err, m) {
                                if (err) {
                                    getErrorMsg(query, 'Was not created');
                                    cb(new Error(query.firstName + ' was not created'));
                                }
                                else if (m.length) {
                                    getErrorMsg(query, 'User with same credentials exist');
                                    cb(new Error(query.firstName + ' already exist'));
                                }
                                else cb();

                            })
                        }, function (err) {
                            if (err) {
                                console.log('A member data has an issue', err.message);
                                cb(err);
                            }
                            else {
                                console.log('All members have not been previously addeed');
                                cb(null, data);
                            }
                        })
                    }
                },
                function (data, cb) {
                    /*
                     FIRST NAME, LAST NAME, EMAIL, PHONE, DATE REGISTERED, STATUS, CATEGORY,
                     DATE CONFIRMED, CONTACT ADDRESS
                     */
                    var errorList = [];
                    // var getErrorMsg = function (data, reason) {
                    //     errorList.push({ member: data, reason: reason });
                    // };
                    var models = [];
                    var mailList = [];
                    async.each(data, function (d, cb) {
                        var query = {
                            firstName: d['FIRST NAME'].toLowerCase().trim(),
                            lastName: d['LAST NAME'].toLowerCase().trim(),
                            employeeId: d['FIRST NAME'].toLowerCase().trim()
                        };
                        var updateData = {
                            firstName: d['FIRST NAME'].toLowerCase().trim(),
                            lastName: d['LAST NAME'].toLowerCase().trim(),
                            mEmail: d['EMAIL'].trim(),
                            mPhone: '0' + d['PHONE'].trim(),
                            contact: d['CONTACT ADDRESS'].trim(),
                            regNumber: d['REGISTRATION NUMBER'],

                            //status: d['STATUS'].trim(),
                            status: 'Active',
                            category: d['CATEGORY'].trim(),
                            dateRegistered: d['DATE REGISTERED'],
                            dateConfirmed: d['DATE CONFIRMED'],
                            memberId: d['FIRST NAME'][0].toLowerCase().trim() + d['LAST NAME'].toLowerCase().trim()
                        };
                        var newMember = req.app.db.models.Member();
                        newMember = Object.assign(newMember, updateData);
                        newMember.save(function (err, newM) {
                            if (err) {
                                var newErr = (err.code && err.code === 11000) ? new Error(query.firstName + ' already exists') : err;
                                cb(newErr);
                            }
                            else {
                                models.push(newM._doc);
                                cb(null, newM);
                            }
                        })
                    },
                        function (err) {
                            if (err) cb(err);
                            else cb(null, models);
                        });
                },
                function (models, cb) {//append hashed passsword to each user model
                    var hashedModels = [];
                    var mailList = [];
                    async.eachOf(models, function (m, index, cb) {
                        var memberPassword = m.firstName.trim() + '123';
                        mailList.push({
                            firstName: m.firstName,
                            lastName: m.lastName,
                            userName: m.memberId,
                            password: memberPassword,
                            mEmail: m.mEmail
                        });

                        req.app.db.models.User.encryptPassword(memberPassword, function (err, hash) {
                            m.password = hash;
                            hashedModels.push(m);
                            cb(err);
                        })

                    }, function (err) {
                        if (err) cb(err);
                        else cb(null, hashedModels, mailList);
                    })

                },
                function (models, mailList, cb) {//create member user and generate passwords
                    // var mailList = []
                    //each models object should have a hashed password field
                    async.each(models, function (m, cb) {
                        var user = req.app.db.models.User();

                        user = Object.assign(user, {
                            isActive: true,
                            username: m.firstName[0] + m.lastName,
                            email: m.mEmail,
                            member: m._id,
                            password: m.password
                        });

                        //push {m.firstName, m.lastName, m.mEmail, m.username, m.password, m.category, m.dateRegistered } to a mailList
                        user.save(function (err) {
                            if (err) cb(new Error('Unable to save user with email ' + user.email));
                            cb()
                        })

                    }, function (err) {
                        if (err) cb(err);
                        else cb(null, 'All Member/User was created!', mailList); //pass mailList here
                    });
                }
            ],
            function (err, outcome, mailListObj) {//add another arg for mailList
                console.log(outcome, mailListObj);
                if (err) return res.status(400).json({ error: err.message, message: err.message });
                //send mail to each of the newly created members
                mailListObj.forEach(function (mailObj) {
                    req.app.utility.notify.emit('newMember', mailObj);
                });

                res.json({ message: 'Excel file uploaded and ALL members have been created!:' });
            }
        );
    }
    else {
        removeFile(req.myFile.path);
        res.status(400).json({
            message: 'Wrong file format uploaded. Provide a .xlsx file'
        });
    }

    // res.json({ message: 'Got file uploaded', data: { file: req.file, fields: req.body }});

}
    ;

/*
 User.username === Member.memberId
 */
exports.createMember = function (req, res) {
    //body should contain
    // -firstName, lastName, middleName, mEmail, mPhone, contact, dateRegistered,
    //      dateConfirmed, status, category
    var b = req.body;
    b.memberId = memberName = b.firstName.toLowerCase()[0].toLowerCase() + b.lastName.toLowerCase();
    var memberName = null;
    var memberPassword = null;
    var errorMsg = null;
    var singleMailObj = {}

    var s = req.body.status;
    var approvedMemberId = req.body.approvedMemberId;

    async.waterfall([
        function (cb) { //update the registered approved member's status
            if (approvedMemberId) {
                req.app.db.models.NewMember.findById(approvedMemberId, function (err, m) {
                    m.status = s;
                    m.save(function (e, updatedMember) {
                        if (!e) console.log(updatedMember.firstName, ' with id: ', updatedMember.id, ' was updated to ', s);
                        cb(e, null)
                    })
                })
            } else {

                var newCreateMember = req.app.db.models.Member();
                newCreateMember = Object.assign(newCreateMember, {
                    memberId: b.memberId,
                    firstName: b.firstName,
                    lastName: b.lastName,
                    middleName: b.middleName,
                    mEmail: b.mEmail,
                    mPhone: b.mPhone,
                    regNumber: b.regNumber,
                    contact: b.contact,
                    status: b.status,
                    category: b.category,
                    dateRegistered: b.dateRegistered
                });

                newCreateMember.save(function (err, newMemb) {
                    if (err) {
                        console.log('Error Creating New Member: %s %s %j', b.firstName, b.lastName, err.message);
                    } else {
                        console.log('New Member : %s %s with Category %s was created!', b.firstName, b.lastName, b.category);
                    }
                    cb(err, newMemb);
                })
            }
        }
        ,
        function (newM, cb) {
            if (approvedMemberId) {
                var newMember = req.app.db.models.Member();
                b.memberId = memberName = b.firstName.toLowerCase()[0].toLowerCase() + b.lastName.toLowerCase();
                // memberName = b.firstName[0].toLowerCase() + b.lastName.toLowerCase();
                // b.memberId = memberName;

                newMember = Object.assign(newMember, b);
                newMember.save(function (err, m) {
                    cb(err, m);
                })
            } else {
                cb(null, newM);
            }
        },
        function (member, cb) {
            memberPassword = member.firstName + '123';
            //populate mailObj here
            singleMailObj = {
                firstName: member.firstName,
                lastName: member.lastName,
                userName: member.memberId,
                password: memberPassword,
                mEmail: member.mEmail
            };
            req.app.db.models.User.encryptPassword(memberPassword, function (err, hash) {
                cb(err, member, hash);
            })
        }
        ,
        function (member, hash, cb) {
            var newUser = req.app.db.models.User();
            newUser.member = member._id;
            newUser.email = member.mEmail;
            newUser.username = member.firstName[0].toLowerCase() + member.lastName.toLowerCase();
            newUser.password = hash;
            newUser.isActive = true;
            newUser.save(function (err, user) {
                if (err) req.app.db.models.Member.remove({ _id: member._id }, function (e) {
                    errorMsg = 'Member added was removed cos user creation failed';
                    cb(e, user);
                });
                //
                else cb(err, user);
            })
        }
    ], function (err, result) {

        if (err) return res.status(500).json({
            message: 'User creation failed!' + errorMsg ? errorMsg : '',
            err: err.message || ''
        });
        //send mail to member
        req.app.utility.notify.emit('newMember', singleMailObj);
        res.json({ message: 'User with UserName = ' + result.memberId + '  ;Password = ' + memberPassword + ' was created' })
    })
};
exports.memberChangePassword = function (req, res) {
    var b = req.body;
    if (b.password || b.memberId) {
        async.waterfall([
            function (cb) {
                req.app.db.models.User.encryptPassword(b.password, function (err, hash) {
                    cb(err, hash);
                })
            },
            function (password, cb) {
                req.app.db.models.User.update({ username: b.memberId }, { $set: { password: password } }, { new: true }, function (err, user) {
                    cb(err, user);
                });
            }
        ], function (err, result) {
            if (err) return res.status(500).json({ message: 'Password change failed', errMsg: err.message });
            res.json({ message: 'Password change succeeded', user: result })
        });
    } else {
        res.status(400).json({ message: 'Bad Request received' });
    }
};
exports.adminMemberPasswordReset = function (req, res) {
    var b = req.body;
    if (b._id) {
        async.waterfall([
            function (cb) {//fetch user by id Member._id === User.member
                req.app.db.models.User.findOne({ member: b._id }, function (err, user) {
                    if (err) cb(new Error({ message: 'Error fetching user' }));
                    else if (!user) cb(new Error({ message: 'User with that id does not exist' }));
                    else cb(null, user);
                })
            },
            function (user, cb) { //the last name from username by striping the first letter. password = lastname + 123
                var password = user.username.slice(0) + '123';
                req.app.db.models.User.encryptPassword(password, function (err, hash) {
                    cb(err, hash);
                })
            },
            function (password, cb) {
                req.app.db.models.User.update({ member: b._id }, { $set: { password: password } }, { new: true }, function (err, user) {
                    cb(err, user);
                });
            }
        ], function (err, result) {
            if (err) return res.status(500).json({ message: 'Password change failed! ' + err.message });
            res.json({ message: 'Password change succeeded! New password is (USERNAME123)' });
        });
    } else {
        res.status(400).json({ message: 'Bad Request received' });
    }
};

/* new member list controller actions */
//NewMember
exports.getActiveMembers = function (req, res) {
    return req.app.db.models.Member.find({ status: 'Active' }).sort('-dateRegistered').exec(function (err, activeMembers) {
        if (err) return res.status(500).json({ message: 'Failed to fetch active Members', err: err.message });
        res.json({ message: 'Active Members fetched!', activeMembers: activeMembers });
    });
};
/**/
exports.getPendingNewMemberList = function (req, res) {
    return req.app.db.models.Member.find({ status: 'Registered' }).sort('-dateRegistered').exec(function (err, pendingMembers) {
        if (err) return res.status(500).json({ message: 'Failed to fetch Pending New Members', err: err.message });
        res.json({ message: 'Registered New Members fetched!', newMembers: pendingMembers });
    });
};


//online membership registration controller
exports.onlineMemberRegister = function (req, res) {
    var data = req.body;
    var newM = req.app.db.models.NewMember();
    newM.status = 'Registered';
    newM.mPhone = data.phoneNumber;
    newM = Object.assign(newM, data);
    //console.log("----------- Data: %j", data);
    newM.save(function (err, m) {
        if (err) return res.status(500).json({ message: 'Failed to Add New Members', err: err.message });
        res.json({ message: 'New Member Added!', newMembers: m });
    });
};



exports.updateNewMemberStatus = function (req, res) {
    var s = req.body.status;
    var id = req.body.id;

    req.app.db.models.NewMember.findById(id, function (err, m) {
        m.status = s;
        m.save(function (e, updatedMember) {
            if (err) return res.status(500).json({ message: "Failed to UPDATE New Member's status", err: err, status: 500 });
            res.json({ message: m.firstName + ' ' + m.lastName + ' Membership Request ' + s, newMembers: updatedMember, status: s });
        })
    })
};

//fetch members based on passed req.body.status = Registered | Approved | Declined | Active 
exports.getSpecifiedMember = function (req, res) {
    var status = req.body.status || 'Registered';

    return req.app.db.models.NewMember.find({ status: status }).sort('-createdOn').exec(function (err, members) {
        if (err) return res.status(500).json({ message: 'Failed to fetch Members', err: err.message });
        res.json({ message: status + ' Members fetched!', newMembers: members });
    });
};

exports.getApprovedMember = function (req, res) {
    var id = req.params.id;
    if (id) {
        req.app.db.models.NewMember.findById(id, function (e, m) {
            if (e) return res.status(500).json({ message: 'Error fetching user with id: ', id });
            res.json({ message: 'Approved member fetched!', approvedMember: m });
        })
    } else {
        return res.status(500).json({ message: 'Please provide the approved member id' });
    }
}

exports.addNewMemberList = function (req, res) {
    var data = req.body;
    var newM = req.app.db.models.Member();
    newM = Object.assign(newM, data);
    //console.log("----------- Data: %j", data);
    newM.save(function (err, m) {
        if (err) return res.status(500).json({ message: 'Failed to Add New Members', err: err.message });
        res.json({ message: 'New Member Added!', newMembers: m });
    });
};
exports.updateStatusNewMemberList = function (req, res) {
    var id = req.body.id;
    if (id) {
        req.app.db.models.Member.findById(id, function (err, m) {
            if (err) return res.status(500).json({ message: 'Id does not exist', err: err.message });
            m.status = 'created';
            m.save(function (err, mm) {
                if (err) return res.status(500).json({ message: 'Failed to update new member status', err: err.message });
                res.json({ message: 'New Member status updated!', newMembers: mm });
            })
        });
    } else {
        res.status(500).json({ message: 'Status update failed! No id found' });
    }
};

exports.getMembershipExcel = function (req, res) {
    res.sendFile('scgn-membership.xlsx', { root: './public/static/' });
}

exports.setIsActive = function (req, res) {
    var memberId = req.body.memberId;
    req.app.db.models.Member.findOne({ _id: memberId }, function (err, m) {
        if (err) return res.status(500).json({
            message: 'Cannot find member with id: ' + memberId,
            errMsg: err.message
        });
        m.status = m.isActive ? 'Inactive' : 'Active';
        m.isActive = !m.isActive;
        m.save(function (e, me) {
            if (e) return res.status(500).json({
                message: 'Failed to update member\'s isActive property'
            });
            res.json({ message: 'Book with id ' + memberId + ' was updated successful!' });
        })
    });
};


//Section to handle Membership Fees

exports.getMembershipFees = function (req, res) {

    async.waterfall(
        [
            function(cb){
                //if there are no fees, set default fees
                req.app.db.models.MembershipFee.find({}, function (err, mf) {
                    if(err) cb(err);
                    if(mf.length == 0){
                        var membershipFeesObj = req.app.constants.membership;
                        var employeeId = req.user.employee.id;
                        var feeObj = {};
                        Object.keys(membershipFeesObj).map(function(k, i){
                            feeObj = {
                                type: k,
                                amount: membershipFeesObj[k],
                                dateCreated: new Date(),
                                dateUpdated: new Date(),
                                employeeId: employeeId
                            };

                            var feeToInsert = req.app.db.models.MembershipFee();
                            feeToInsert = Object.assign(feeToInsert, feeObj );
                            feeToInsert.save();
                        
                        });
                        cb();
                    }else{
                        cb();
                    }
                });
            },
            function(cb){
                req.app.db.models.MembershipFee.find({})
                    .populate('employeeId', 'firstName lastName')
                    .exec( function (err, mf) {
                    if (err) cb(err);
                    cb(null, mf);
                });
            }
        ],
        function(err, membershipFees){
            if (err) return res.status(500).json({
                message: 'Error fetching Membership fee: ',
                errMsg: err.message
            });
            res.json({ message: 'All Membership fees fetched!', membershipFees: membershipFees });
        }
    );
};

exports.updateFee = function (req, res) {
    var feeId = req.body.feeId;
    var employeeId = req.body.employeeId;
    var dateUpdated = new Date();
    var amount = req.body.amount;
    
    if ( feeId && employeeId && amount  ) {
        req.app.db.models.MembershipFee.findOne({ _id: feeId }, function (err, mf) {
            if (err) return res.status(500).json({
                message: 'Cannot find  Membership Fee with id: ' + feeId,
                errMsg: err.message
            });
            mf.amount = amount;
            mf.employeeId = employeeId;
            mf.dateUpdated = dateUpdated;
            mf.save(function (e, f) {
                if (e) return res.status(500).json({
                    message: 'Failed to update membership Fee'
                });
                res.json({ message: f.type + ' feeType was successfully updated!' });
            })
        });
    } else {
        res.status(400).json({ message: 'feeId, employeeId & amount are required', errMsg: 'feeId | employeeId | amount are required' });
    }
};


